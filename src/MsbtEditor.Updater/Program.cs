﻿using System.Diagnostics;
using System.Reflection;

namespace MsbtEditor.Updater;

internal static class Program
{
    private static void Main()
    {
        var rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!;
        var updateFolder = Path.Combine(rootPath, "update");
        var contentFolder = Path.Combine(updateFolder, "content");

        if (!Directory.Exists(contentFolder))
        {
            Console.WriteLine("Update folder not found. Starting MSBT Editor...");
            Process.Start(new ProcessStartInfo("MsbtEditor.exe"));
            Environment.Exit(0);
            return;
        }

        Console.WriteLine("Waiting for MSBT Editor to exit...");
        while (true)
        {
            var running = false;
            foreach (var process in Process.GetProcesses())
            {
                if (process.ProcessName.Equals("MsbtEditor", StringComparison.OrdinalIgnoreCase))
                {
                    running = false;
                    break;
                }
            }
            if (!running) break;
            Thread.Sleep(3000);
        }

        Console.WriteLine("Copying update files...");
        Parallel.ForEach(Directory.GetDirectories(contentFolder, "*", SearchOption.AllDirectories), directory =>
        {
            var relativePath = Path.GetRelativePath(contentFolder, directory);
            var targetPath = Path.Combine(rootPath, relativePath);
            Directory.CreateDirectory(targetPath);
        });
        Parallel.ForEach(Directory.GetFiles(contentFolder, "*", SearchOption.AllDirectories), file =>
        {
            if (Path.GetFileName(file).Equals("MsbtEditor.Updater.exe")) return;
            var relativePath = Path.GetRelativePath(contentFolder, file);
            var targetPath = Path.Combine(rootPath, relativePath);
            File.Copy(file, targetPath, true);
        });

        Console.WriteLine("Cleanup...");
        Directory.Delete(updateFolder, true);

        Console.WriteLine("Starting MSBT Editor...");
        Process.Start(new ProcessStartInfo("MsbtEditor.exe"));
        Environment.Exit(0);
    }
}