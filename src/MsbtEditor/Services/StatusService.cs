﻿namespace MsbtEditor;

internal sealed class StatusService
{
    #region public properties
    public string Message { get; private set; } = string.Empty;
    #endregion

    #region public events
    public event EventHandler? StatusChanged;
    #endregion

    #region public methods
    public void Set(string message)
    {
        Message = message;
        StatusChanged?.Invoke(this, EventArgs.Empty);
    }
    #endregion
}