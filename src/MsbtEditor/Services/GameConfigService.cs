﻿using System.Diagnostics.CodeAnalysis;
using MsbtEditor.Components.Dialogs;
using NintendoTools.FileFormats.Msbt;
using YamlConverter;

namespace MsbtEditor;

internal sealed class GameConfigService
{
    #region private members
    private readonly EditorService _editorService;
    private readonly DialogService _dialogService;
    private readonly IWritableOptions<GameConfigSettings> _settings;
    private List<GameConfigFileData> _configs = [];
    #endregion

    #region constructor
    public GameConfigService(EditorService editorService, DialogService dialogService, IWritableOptions<GameConfigSettings> settings)
    {
        _editorService = editorService;
        _dialogService = dialogService;
        _settings = settings;

        Init();
    }
    #endregion

    #region public properties
    public GameConfigFileData CurrentFile { get; private set; } = null!;

    public GameConfig CurrentConfig { get; private set; } = null!;

    public IReadOnlyList<GameConfigFileData> Configs => _configs;
    #endregion

    #region public events
    public event EventHandler? CurrentConfigChanged;

    public event EventHandler? ConfigListChanged;
    #endregion

    #region public methods
    public async Task Reload()
    {
        // ReSharper disable once ConditionalAccessQualifierIsNonNullableAccordingToAPIContract
        var oldName = CurrentFile?.Name ?? _settings.CurrentValue.LastConfig;

        Directory.CreateDirectory(CommonPaths.ConfigFolder);
        var configs = new List<GameConfigFileData>();

        //load all configs found in the config directory
        GameConfigFileData? currentFile = null;
        GameConfig? currentConfig = null;
        foreach (var filePath in Directory.GetFiles(CommonPaths.ConfigFolder))
        {
            if (!filePath.EndsWith(".gcf", StringComparison.OrdinalIgnoreCase)) continue;
            var fileName = Path.GetFileName(filePath);

            //check if we already know that file
            GameConfigFileData? file = null;
            foreach (var file1 in _configs)
            {
                if (!file1.Name.Equals(fileName)) continue;
                file = file1;
                break;
            }

            //load the new config
            if (file is null)
            {
                try
                {
                    file = new GameConfigFileData(new FileStorageProvider(filePath, false));
                }
                catch
                {
                    //ignored
                    continue;
                }
            }

            //load the file as current config
            if (fileName.Equals(oldName))
            {
                try
                {
                    currentConfig = await file.LoadModel();
                    currentFile = file;
                }
                catch
                {
                    //ignored
                }
            }

            foreach (var tab in _editorService.GetAllUniqueTabs(file)) await tab.Reload();
            configs.Add(file);
        }

        //find any valid config
        if (currentConfig is null) (currentFile, currentConfig) = await FindFirstValidConfig(configs);

        //dispose old configs
        foreach (var file in _configs)
        {
            if (configs.Contains(file)) continue;
            await _editorService.CloseFile(file);
            file.Dispose();
        }

        _configs = configs;
        if (CurrentFile is not null) CurrentFile.Modified -= OnConfigModified;
        CurrentFile = currentFile!;
        CurrentFile.Modified += OnConfigModified;
        CurrentConfig = currentConfig;

        if (string.IsNullOrEmpty(_settings.CurrentValue.LastConfig)) _settings.Update(s => s.LastConfig = CurrentFile.Name);

        ConfigListChanged?.Invoke(this, EventArgs.Empty);
        CurrentConfigChanged?.Invoke(this, EventArgs.Empty);
    }

    public async Task Select(GameConfigFileData config)
    {
        if (CurrentFile == config || !_configs.Contains(config)) return;

        CurrentConfig = await config.LoadModel();
        CurrentFile.Modified -= OnConfigModified;
        CurrentFile = config;
        CurrentFile.Modified += OnConfigModified;

        _settings.Update(s => s.LastConfig = config.Name);

        CurrentConfigChanged?.Invoke(this, EventArgs.Empty);
    }

    public bool TryGet(string name, [MaybeNullWhen(false)] out GameConfigFileData config)
    {
        name = Path.GetFileNameWithoutExtension(name);
        config = _configs.Find(m => Path.GetFileNameWithoutExtension(m.Name).Equals(name));
        return config is not null;
    }

    public bool Contains(string name) => TryGet(name, out _);

    public async Task<GameConfigFileData> Load(string filePath, bool overwrite = false)
    {
        var name = Path.GetFileNameWithoutExtension(filePath);
        var content = await File.ReadAllTextAsync(filePath);

        try
        {
            var provider = new FileStorageProvider(filePath, false);
            using var file = new GameConfigFileData(provider);
            await file.LoadModel();
        }
        catch (Exception ex)
        {
            throw new Exception($"Failed to parse file content as Game Config: {ex.Message}");
        }

        if (TryGet(name, out var existingMap))
        {
            if (!overwrite) throw new InvalidOperationException("A Game Config with the same name already exists.");
            await Delete(existingMap);
        }

        var config = await CreateConfig(name, content);
        ConfigListChanged?.Invoke(this, EventArgs.Empty);

        return config;
    }

    public async Task<GameConfigFileData> Create(string name)
    {
        if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
        if (Contains(name)) throw new InvalidOperationException("A Game Config with the same name already exists.");

        var config = await CreateConfig(name);
        ConfigListChanged?.Invoke(this, EventArgs.Empty);

        return config;
    }

    public async Task Delete(GameConfigFileData config)
    {
        if (!_configs.Remove(config)) return;

        await _editorService.CloseFile(config);
        await config.Delete();

        var currentMapChanged = CurrentFile == config;
        if (currentMapChanged)
        {
            CurrentFile.Modified -= OnConfigModified;
            (CurrentFile, CurrentConfig) = await FindFirstValidConfig(_configs);
            CurrentFile.Modified += OnConfigModified;
        }

        ConfigListChanged?.Invoke(this, EventArgs.Empty);
        if (currentMapChanged) CurrentConfigChanged?.Invoke(this, EventArgs.Empty);
    }

    public async Task Create(MsbpFileData file)
    {
        var result1 = await _dialogService.ShowDialog<FileNameDialog, string>(new Dictionary<string, object> {{nameof(FileNameDialog.Message), "Choose a name for the new Game Config:"}});
        var name = result1.Type is DialogResultType.Ok ? result1.Value : null;
        if (string.IsNullOrEmpty(name)) return;

        var result2 = await _dialogService.ShowDialog<FileNameDialog, string>(new Dictionary<string, object> {{nameof(FileNameDialog.Message), "Name for the game:"}});
        var game = result1.Type is DialogResultType.Ok ? result2.Value : null;

        var model = await file.LoadModel();
        var tags = new List<TagInfo>();

        for (var i = 0; i < model.TagGroups.Length; ++i)
        {
            var tagGroup = model.TagGroups[i];
            if (tagGroup.Tags.Length == 0) continue;

            for (var j = 0; j < tagGroup.Tags.Length; ++j)
            {
                var tag = tagGroup.Tags[j];

                var args = new List<ArgumentInfo>();
                foreach (var parameter in tag.Parameters)
                {
                    var dataType = parameter.Type switch
                    {
                        NintendoTools.FileFormats.Msbp.ValueType.Byte   => DataTypes.Byte,
                        NintendoTools.FileFormats.Msbp.ValueType.UInt16 => DataTypes.UInt16,
                        NintendoTools.FileFormats.Msbp.ValueType.UInt32 => DataTypes.UInt32,
                        NintendoTools.FileFormats.Msbp.ValueType.SByte  => DataTypes.SByte,
                        NintendoTools.FileFormats.Msbp.ValueType.Int16  => DataTypes.Int16,
                        NintendoTools.FileFormats.Msbp.ValueType.Int32  => DataTypes.Int32,
                        NintendoTools.FileFormats.Msbp.ValueType.Single => DataTypes.Single,
                        NintendoTools.FileFormats.Msbp.ValueType.String => DataTypes.String,
                        NintendoTools.FileFormats.Msbp.ValueType.Enum   => DataTypes.Byte,
                        _                                               => throw new InvalidDataException($"Parameter type {(int) parameter.Type} is not yet defined.")
                    };

                    Dictionary<string, ValueInfo> valueMap = [];
                    if (parameter.Type == NintendoTools.FileFormats.Msbp.ValueType.Enum)
                    {
                        var index = 0;
                        foreach (var enumValue in parameter.EnumValues)
                        {
                            valueMap.Add(index++.ToString(), new ValueInfo {Name = enumValue});
                        }
                    }

                    args.Add(new ArgumentInfo
                    {
                        DataType = dataType,
                        ValueMap = valueMap,
                        Name = parameter.Name
                    });
                }

                var tagName = tag.Name;
                var tagIndex = 0;
                foreach (var tagInfo in tags)
                {
                    if (tagInfo.Name != tagName) continue;
                    tagName = $"{tag.Name}_{++tagIndex}";
                }

                tags.Add(new TagInfo
                {
                    Group = i,
                    Type = j,
                    Name = tagName,
                    Arguments = args
                });
            }
        }

        var config = new GameConfig {Game = game ?? string.Empty, Msbt = {Tags = tags}};
        var yaml = YamlConvert.SerializeObject(config, GameConfigFileData.JsonSettings);
        await CreateConfig(name, yaml);
        ConfigListChanged?.Invoke(this, EventArgs.Empty);
    }
    #endregion

    #region private methods
    private void Init()
    {
        Directory.CreateDirectory(CommonPaths.ConfigFolder);

        //convert any old function maps
        var mapsConverted = 0;
        if (Directory.Exists(CommonPaths.MapFolder))
        {
            foreach (var filePath in Directory.GetFiles(CommonPaths.MapFolder, "*.mfm"))
            {
                try
                {
                    var content = File.ReadAllText(filePath);
                    var map = DynamicTagMapParser.ParseMsbtTagMap(content);

                    //build tag map
                    var tags = new List<TagInfo>(map.Count);
                    bool? isBmgMap = null;
                    foreach (var tag in map)
                    {
                        var args = new List<ArgumentInfo>(tag.Arguments.Count);
                        foreach (var arg in tag.Arguments)
                        {
                            var valueMap = new Dictionary<string, ValueInfo>(arg.ValueMap.Count);
                            foreach (var valueInfo in arg.ValueMap)
                            {
                                valueMap.Add(valueInfo.Value, new ValueInfo
                                {
                                    Name = valueInfo.Name,
                                    Description = valueInfo.Description
                                });
                            }

                            args.Add(new ArgumentInfo
                            {
                                DataType = arg.DataType,
                                ValueMap = valueMap,
                                ArrayLength = arg.ArrayLength,
                                Name = arg.Name,
                                Description = arg.Description
                            });
                        }

                        var typeMap = new Dictionary<string, ValueInfo>(tag.TypeMap.Count);
                        foreach (var valueInfo in tag.TypeMap)
                        {
                            typeMap.Add(valueInfo.Value, new ValueInfo
                            {
                                Name = valueInfo.Name,
                                Description = valueInfo.Description
                            });
                        }

                        tags.Add(new TagInfo
                        {
                            Group = tag.Group,
                            Type = tag.Type ?? -1,
                            Types = tag.TypeList.Select(t => (int) t).ToArray(),
                            TypeMap = typeMap,
                            Discard = tag.HasDiscard,
                            Name = tag.Name,
                            Description = tag.Description,
                            Arguments = args
                        });

                        //check if map is a bmg or msbt map
                        if (tag.Group > byte.MaxValue)
                        {
                            isBmgMap = false;
                        }
                        else if (tag is {Group: 255, Type: 0, Arguments.Count: > 0} && tag.Name.Equals("color", StringComparison.OrdinalIgnoreCase) ||
                                 tag is {Group: 255, Type: 2, Arguments.Count: > 1} && tag.Name.Equals("kanjiReplace", StringComparison.OrdinalIgnoreCase))
                        {
                            isBmgMap ??= true;
                        }
                    }

                    var config = new GameConfig();
                    if (isBmgMap == true) config.Bmg.Tags = tags;
                    else config.Msbt.Tags = tags;

                    //write file
                    var newContent = YamlConvert.SerializeObject(config, GameConfigFileData.JsonSettings);
                    File.WriteAllText(Path.Combine(CommonPaths.ConfigFolder, Path.GetFileNameWithoutExtension(filePath) + ".gcf"), newContent);
                    File.Delete(filePath);
                }
                catch
                {
                    continue;
                }

                ++mapsConverted;
            }

            //delete old maps folder if empty
            if (Directory.GetFiles(CommonPaths.MapFolder, "*", SearchOption.AllDirectories).Length == 0)
            {
                try
                {
                    Directory.Delete(CommonPaths.MapFolder, true);
                }
                catch
                {
                    //ignored
                }
            }
        }

        //reload configs
        Task.Run(Reload).Wait();

        //show dialog if a map was converted
        if (mapsConverted == 0) return;
        _dialogService.ShowDialog<GameConfigConversionDialog>(new Dictionary<string, object> {{nameof(GameConfigConversionDialog.ConvertedMaps), mapsConverted}});
    }

    // ReSharper disable once AsyncVoidMethod
    private async void OnConfigModified(object? sender, EventArgs args)
    {
        if (sender is not GameConfigFileData file || file.IsModified) return;

        CurrentConfig = await file.LoadModel();
        CurrentConfigChanged?.Invoke(this, EventArgs.Empty);
    }

    private async Task<(GameConfigFileData, GameConfig)> FindFirstValidConfig(List<GameConfigFileData> configs)
    {
        GameConfigFileData? file = null;
        GameConfig? config = null;

        foreach (var configFile in configs)
        {
            try
            {
                config = await configFile.LoadModel();
                file = configFile;
                break;
            }
            catch
            {
                //ignored
            }
        }

        if (file is null)
        {
            file = await CreateDefaultConfig();
            config = await file.LoadModel();
        }

        return (file, config!);
    }

    private Task<GameConfigFileData> CreateDefaultConfig() => CreateConfig("Default");

    private async Task<GameConfigFileData> CreateConfig(string name, string content = "")
    {
        Directory.CreateDirectory(CommonPaths.ConfigFolder);
        var filePath = Path.Combine(CommonPaths.ConfigFolder, $"{name}.gcf");
        await File.WriteAllTextAsync(filePath, content);

        var config = new GameConfigFileData(new FileStorageProvider(filePath, false));
        _configs.Add(config);
        return config;
    }
    #endregion
}