﻿namespace MsbtEditor;

internal sealed class FileDragDropService
{
    public FileData? File { get; private set; }

    public IEditorData? EditorData { get; private set; }

    public IEditorTabGroup? TabGroup { get; private set; }

    public bool IsDragging { get; private set; }

    public event EventHandler? DragStarted;

    public event EventHandler? DragCompleted;

    public void Start(FileData file)
    {
        if (IsDragging) return;

        File = file;
        IsDragging = true;
        DragStarted?.Invoke(this, EventArgs.Empty);
    }

    public void Start(IEditorData tab, IEditorTabGroup tabGroup)
    {
        if (IsDragging) return;

        EditorData = tab;
        TabGroup = tabGroup;
        IsDragging = true;
        DragStarted?.Invoke(this, EventArgs.Empty);
    }

    public void Clear()
    {
        if (!IsDragging) return;

        IsDragging = false;
        File = null;
        EditorData = null;
        TabGroup = null;
        DragCompleted?.Invoke(this, EventArgs.Empty);
    }
}