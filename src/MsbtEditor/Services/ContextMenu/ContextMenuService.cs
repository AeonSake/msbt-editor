﻿using Microsoft.AspNetCore.Components;

namespace MsbtEditor;

internal sealed class ContextMenuService
{
    #region private members
    private MenuData? _currentData;
    private static readonly Action NullAction = () => {};
    private Action _callback = NullAction;
    #endregion

    #region public events
    public event EventHandler<MenuData>? Opened;

    public event EventHandler? Closed;
    #endregion

    #region public methods
    public void Open(IEnumerable<ContextAction> actions) => Open(actions, default, MenuPosition.BottomLeft, NullAction);

    public void Open(IEnumerable<ContextAction> actions, MenuPosition position) => Open(actions, default, position, NullAction);

    public void Open(IEnumerable<ContextAction> actions, Action callback) => Open(actions, default, MenuPosition.BottomLeft, callback);

    public void Open(IEnumerable<ContextAction> actions, MenuPosition position, Action callback) => Open(actions, default, position, callback);

    public void Open(IEnumerable<ContextAction> actions, ElementReference reference, bool useReferenceForPosition = false) => Open(actions, reference, MenuPosition.BottomLeft, NullAction, useReferenceForPosition);

    public void Open(IEnumerable<ContextAction> actions, ElementReference reference, Action callback, bool useReferenceForPosition = false) => Open(actions, reference, MenuPosition.BottomLeft, callback, useReferenceForPosition);

    public void Open(IEnumerable<ContextAction> actions, ElementReference reference, MenuPosition position, bool useReferenceForPosition = false) => Open(actions, reference, position, NullAction, useReferenceForPosition);

    public void Open(IEnumerable<ContextAction> actions, ElementReference reference, MenuPosition position, Action callback, bool useReferenceForPosition = false)
    {
        Close();

        _currentData = new MenuData
        {
            Reference = reference,
            Position = position,
            UseReferenceForPosition = useReferenceForPosition,
            Actions = actions
        };
        _callback = callback;

        Opened?.Invoke(this, _currentData);
    }

    public void Close()
    {
        if (_currentData is null) return;

        _currentData = null;

        Closed?.Invoke(this, EventArgs.Empty);
        _callback.Invoke();
    }
    #endregion
}