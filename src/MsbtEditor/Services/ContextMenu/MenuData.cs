﻿using Microsoft.AspNetCore.Components;

namespace MsbtEditor;

internal class MenuData
{
    public required ElementReference Reference { get; init; }

    public required MenuPosition Position { get; init; }

    public required bool UseReferenceForPosition { get; init; }

    public required IEnumerable<ContextAction> Actions { get; init; }
}