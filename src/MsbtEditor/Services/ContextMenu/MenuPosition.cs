﻿namespace MsbtEditor;

internal enum MenuPosition
{
    BottomLeft,
    BottomCenter,
    BottomRight,
    TopLeft,
    TopCenter,
    TopRight
}