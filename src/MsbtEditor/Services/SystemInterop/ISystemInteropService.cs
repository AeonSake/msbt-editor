﻿namespace MsbtEditor;

internal interface ISystemInteropService
{
    public Task<DialogResult<string[]>> ShowOpenFileDialog(string filter, bool multiSelect);

    public Task<DialogResult<string>> ShowSaveFileDialog(string fileName);

    public Task<DialogResult<string>> ShowOpenFolderDialog();

    public Task<DialogResult> OpenFileExplorer(string directory);
}