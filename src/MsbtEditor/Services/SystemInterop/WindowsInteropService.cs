﻿using System.Diagnostics;

namespace MsbtEditor;

internal sealed class WindowsInteropService : ISystemInteropService
{
    #region private members
    private Control _context = new();
    #endregion

    #region public methods
    public void SetContext(Control context) => _context = context;

    public Task<DialogResult<string[]>> ShowOpenFileDialog(string filter, bool multiSelect) => Task.Run(() =>
    {
        var dialog = new OpenFileDialog
        {
            Filter = filter,
            Multiselect = multiSelect,
            CheckFileExists = true
        };

        return InvokeIfRequired(() => dialog.ShowDialog(_context) == System.Windows.Forms.DialogResult.OK
            ? new DialogResult<string[]>(dialog.FileNames, DialogResultType.Ok)
            : new DialogResult<string[]>([], DialogResultType.Cancel));
    });

    public Task<DialogResult<string>> ShowSaveFileDialog(string fileName) => Task.Run(() =>
    {
        var dialog = new SaveFileDialog
        {
            FileName = fileName,
            CheckWriteAccess = true
        };

        return InvokeIfRequired(() => dialog.ShowDialog(_context) == System.Windows.Forms.DialogResult.OK
            ? new DialogResult<string>(dialog.FileName, DialogResultType.Ok)
            : new DialogResult<string>(string.Empty, DialogResultType.Cancel));
    });

    public Task<DialogResult<string>> ShowOpenFolderDialog() => Task.Run(() =>
    {
        var dialog = new FolderBrowserDialog();

        return InvokeIfRequired(() => dialog.ShowDialog(_context) == System.Windows.Forms.DialogResult.OK
            ? new DialogResult<string>(dialog.SelectedPath, DialogResultType.Ok)
            : new DialogResult<string>(string.Empty, DialogResultType.Cancel));
    });

    public Task<DialogResult> OpenFileExplorer(string directory)
    {
        Process.Start("explorer.exe", directory);
        return Task.FromResult(new DialogResult(DialogResultType.Ok));
    }
    #endregion

    #region private members
    private T InvokeIfRequired<T>(Func<T> action) => _context.InvokeRequired ? _context.Invoke(action) : action();
    #endregion
}