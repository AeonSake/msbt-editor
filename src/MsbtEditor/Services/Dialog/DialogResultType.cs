﻿namespace MsbtEditor;

public enum DialogResultType
{
    Ok,
    Yes,
    No,
    Retry,
    Cancel,
    Close
}