﻿namespace MsbtEditor;

public class DialogResult(DialogResultType type)
{
    public DialogResultType Type { get; } = type;

    public static implicit operator DialogResult(DialogResultType type) => new(type);

    public static implicit operator DialogResult(DialogButtons button) => button switch
    {
        DialogButtons.Ok     => DialogResultType.Ok,
        DialogButtons.Yes    => DialogResultType.Yes,
        DialogButtons.No     => DialogResultType.No,
        DialogButtons.Retry  => DialogResultType.Retry,
        DialogButtons.Cancel => DialogResultType.Cancel,
        DialogButtons.Close  => DialogResultType.Close,
        _                    => throw new ArgumentOutOfRangeException(nameof(button), button, null)
    };
}

public class DialogResult<T>(T? value, DialogResultType type) : DialogResult(type)
{
    public T? Value { get; } = value;
}