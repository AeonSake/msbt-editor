﻿namespace MsbtEditor;

public interface IDialogReference
{
    public DialogResult? Result { get; }

    public void Close();

    public void Close(DialogResult result);

    public Task<DialogResult> WaitForClose();
}

public interface IDialogReference<T>
{
    public DialogResult<T>? Result { get; }

    public void Close();

    public void Close(DialogResult<T> result);

    public Task<DialogResult<T>> WaitForClose();
}