﻿using Microsoft.AspNetCore.Components;
using MsbtEditor.Components.Dialogs.Base;

namespace MsbtEditor;

internal sealed class DialogService(ISystemInteropService systemInteropService)
{
    #region private members
    private readonly List<RenderFragment> _dialogs = [];
    #endregion

    #region internal properties
    public IEnumerable<RenderFragment> Dialogs => _dialogs;
    #endregion

    #region public events
    public event EventHandler? DialogsChanged;
    #endregion

    #region public methods
    public Task<DialogResult<string[]>> ShowOpenFileDialog(string filter, bool multiSelect) => systemInteropService.ShowOpenFileDialog(filter, multiSelect);

    public Task<DialogResult<string>> ShowSaveFileDialog(string fileName) => systemInteropService.ShowSaveFileDialog(fileName);

    public Task<DialogResult<string>> ShowOpenFolderDialog() => systemInteropService.ShowOpenFolderDialog();

    public Task<DialogResult> OpenFileExplorer(string directory)
    {
        directory = Path.GetFullPath(directory);
        if (!Directory.Exists(directory)) throw new DirectoryNotFoundException(directory);

        return systemInteropService.OpenFileExplorer(directory);
    }

    public Task<DialogResult> ShowDialog<TComponent>(IDictionary<string, object>? attributes = null, bool preventClose = false) where TComponent : IComponent => ShowDialogAsync<TComponent>(attributes, preventClose).WaitForClose();

    public Task<DialogResult<TResult>> ShowDialog<TComponent, TResult>(IDictionary<string, object>? attributes = null, bool preventClose = false) where TComponent : IComponent => ShowDialogAsync<TComponent, TResult>(attributes, preventClose).WaitForClose();

    public IDialogReference ShowDialogAsync<TComponent>(IDictionary<string, object>? attributes = null, bool preventClose = false) where TComponent : IComponent
    {
        var reference = new DialogReference();
        ShowDialogAsyncInternal(reference, BuildDialogContent<TComponent>(attributes), preventClose);
        return reference;
    }

    public IDialogReference<TResult> ShowDialogAsync<TComponent, TResult>(IDictionary<string, object>? attributes = null, bool preventClose = false) where TComponent : IComponent
    {
        var reference = new DialogReference<TResult>();
        ShowDialogAsyncInternal(reference, BuildDialogContent<TComponent>(attributes), preventClose);
        return reference;
    }

    public Task<DialogResult> ShowMessageDialog(string message, StatusType? status = null, DialogButtons buttons = DialogButtons.Ok, bool preventClose = true) => ShowMessageDialogAsync(message, status, buttons, preventClose).WaitForClose();

    public IDialogReference ShowMessageDialogAsync(string message, StatusType? status = null, DialogButtons buttons = DialogButtons.Ok, bool preventClose = true)
    {
        var reference = new DialogReference();
        ShowDialogAsyncInternal(reference, BuildMessageDialogContent(message, status, buttons), preventClose);
        return reference;
    }
    #endregion

    #region private methods
    private static RenderFragment BuildDialogContent<TComponent>(IDictionary<string, object>? attributes) where TComponent : IComponent => builder =>
    {
        builder.OpenComponent<TComponent>(0);
        builder.AddMultipleAttributes(0, attributes ?? new Dictionary<string, object>());
        builder.CloseComponent();
    };

    private static RenderFragment BuildMessageDialogContent(string message, StatusType? status, DialogButtons buttons) => builder =>
    {
        builder.OpenComponent<MessageDialog>(0);
        builder.AddAttribute(0, nameof(MessageDialog.Message), message);
        builder.AddAttribute(1, nameof(MessageDialog.Status), status);
        builder.AddAttribute(2, nameof(MessageDialog.Buttons), buttons);
        builder.CloseComponent();
    };

    private void ShowDialogAsyncInternal(DialogReference reference, RenderFragment dialogContent, bool preventClose)
    {
        reference.Handle = new EventWaitHandle(false, EventResetMode.ManualReset);
        reference.Component = builder =>
        {
            builder.OpenComponent<Dialog>(0);
            builder.AddAttribute(0, nameof(Dialog.PreventClose), preventClose);
            builder.AddAttribute(1, nameof(Dialog.ChildContent), dialogContent);
            builder.AddAttribute(2, nameof(Dialog.OnClose), EventCallback.Factory.Create(this, (Action<DialogResult>) (res =>
            {
                if (reference.Component is null) return;

                reference.Result = res;
                reference.Handle.Set();
                reference.Handle.Dispose();
                reference.Handle = null;
                reference.Instance = null;
                _dialogs.Remove(reference.Component!);
                reference.Component = null;

                DialogsChanged?.Invoke(this, EventArgs.Empty);
            })));
            builder.AddComponentReferenceCapture(3, capture =>
            {
                reference.Instance = (Dialog) capture;
                reference.Instance.Show();
            });
            builder.CloseComponent();
        };

        _dialogs.Add(reference.Component);
        DialogsChanged?.Invoke(this, EventArgs.Empty);
    }
    #endregion

    #region helper classes
    private class DialogReference : IDialogReference
    {
        public EventWaitHandle? Handle { get; set; }

        public RenderFragment? Component { get; set; }

        public Dialog? Instance { get; set; }

        public virtual DialogResult? Result { get; set; }

        public void Close() => Instance?.Close();

        public void Close(DialogResult result) => Instance?.Close(result);

        public Task<DialogResult> WaitForClose() => Task.Run(() =>
        {
            if (Result is not null) return Result;
            Handle?.WaitOne();
            return Result!;
        });
    }

    private class DialogReference<T> : DialogReference, IDialogReference<T>
    {
        private DialogResult<T>? _result;

        public override DialogResult? Result
        {
            get => _result;
            set => _result = value switch
            {
                null                => null,
                DialogResult<T> res => res,
                _                   => new DialogResult<T>(default, value.Type)
            };
        }

        DialogResult<T>? IDialogReference<T>.Result => _result;

        public void Close(DialogResult<T> result) => Instance?.Close(result);

        Task<DialogResult<T>> IDialogReference<T>.WaitForClose() => Task.Run(() =>
        {
            if (_result is not null) return _result;
            Handle?.WaitOne();
            return _result!;
        });
    }
    #endregion
}