﻿namespace MsbtEditor;

public enum DialogSize
{
    MinContent,
    Small,
    Medium,
    Wide,
    ExtraWide
}