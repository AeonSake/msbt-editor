﻿namespace MsbtEditor;

[Flags]
public enum DialogButtons
{
    Ok = 1,
    Yes = 2,
    No = 4,
    Retry = 8,
    Cancel = 16,
    Close = 32
}