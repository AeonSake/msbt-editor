﻿using Microsoft.Extensions.Options;
using MsbtEditor.Components.Dialogs;

namespace MsbtEditor;

internal sealed class ContextActionProvider(AppStateService appStateService,
                                     FileBrowserService fileBrowserService,
                                     EditorService editorService,
                                     DialogService dialogService,
                                     BrowserContextService browserContextService,
                                     FileContextActionProvider fileContextActionProvider,
                                     TextEditorProvider textEditorProvider,
                                     IOptionsMonitor<DebugSettings> debugSettings)
{
    public ContextAction[] GetMenuFileActions() =>
    [
        new()
        {
            Name = "Open File...",
            Icon = GlobalIcons.Open,
            OnClick = appStateService.LoadFiles
        },
        new()
        {
            Name = "Open Folder...",
            Icon = GlobalIcons.Folder,
            OnClick = appStateService.LoadFolder
        },
        ContextAction.Separator,
        new()
        {
            Name = "Save",
            Icon = GlobalIcons.Save,
            OnClick = () => fileBrowserService.SelectedFile is null ? Task.CompletedTask : appStateService.SaveFile(fileBrowserService.SelectedFile),
            Active = fileBrowserService.SelectedFile?.IsModified ?? false
        },
        new()
        {
            Name = "Save All",
            Icon = GlobalIcons.SaveAll,
            OnClick = appStateService.SaveAllFiles,
            Active = fileBrowserService.Files.Any(file => file.IsModified)
        },
        new()
        {
            Name = "Revert Changes",
            Icon = GlobalIcons.Undo,
            OnClick = () => fileBrowserService.SelectedFile is null ? Task.CompletedTask : appStateService.RevertChanges(fileBrowserService.SelectedFile),
            Active = fileBrowserService.SelectedFile?.IsModified ?? false
        },
        ContextAction.Separator,
        new()
        {
            Name = "Close",
            Icon = GlobalIcons.X,
            OnClick = () => editorService.ActiveTabGroup.SelectedTab is null ? Task.CompletedTask : appStateService.CloseFile(editorService.ActiveTabGroup.SelectedTab.File),
            Active = editorService.ActiveTabGroup.SelectedTab is not null
        },
        new()
        {
            Name = "Close All",
            OnClick = () => editorService.ActiveTabGroup.Tabs.Count == 0 ? Task.CompletedTask : appStateService.CloseAllFiles(),
            Active = editorService.ActiveTabGroup.Tabs.Count > 0
        },
        ContextAction.Separator,
        new()
        {
            Name = "Unload",
            OnClick = () => fileBrowserService.SelectedFile?.IsVirtual != false ? Task.CompletedTask : appStateService.UnloadFile(fileBrowserService.SelectedFile),
            Active = fileBrowserService.SelectedFile?.IsVirtual == false
        },
        new()
        {
            Name = "Unload All",
            OnClick = () => fileBrowserService.Count == 0 ? Task.CompletedTask : appStateService.UnloadAllFiles(),
            Active = fileBrowserService.Count > 0
        },
        ContextAction.Separator,
        new()
        {
            Name = "Settings",
            Icon = GlobalIcons.Settings,
            OnClick = () => dialogService.ShowDialog<SettingsDialog>()
        },
        new()
        {
            Name = "Check for Update",
            Icon = GlobalIcons.Package,
            OnClick = () => dialogService.ShowDialog<UpdateDialog>()
        },
        new()
        {
            Name = "About",
            Icon = GlobalIcons.Info,
            OnClick = () => dialogService.ShowDialog<AboutDialog>()
        }
    ];

    public ContextAction[] GetMenuDataActions() =>
    [
        new()
        {
            Name = "Change Game Config...",
            Icon = GlobalIcons.ConfigFile,
            OnClick = () =>
            {
                dialogService.ShowDialog<GameConfigSelectDialog>();
                return Task.CompletedTask;
            }
        }
    ];

    public ContextAction[] GetMenuViewActions()
    {
        var actions = new List<ContextAction>
        {
            new()
            {
                Name = "Split Left",
                Icon = GlobalIcons.ArrowLeft,
                OnClick = () => editorService.ActiveTabGroup.SelectedTab is not null ? editorService.DuplicateTab(editorService.ActiveTabGroup.SelectedTab, editorService.ActiveTabGroup, SplitDirection.Left) : Task.CompletedTask,
                Active = editorService.ActiveTabGroup.SelectedTab is not null
            },
            new()
            {
                Name = "Split Right",
                Icon = GlobalIcons.ArrowRight,
                OnClick = () => editorService.ActiveTabGroup.SelectedTab is not null ? editorService.DuplicateTab(editorService.ActiveTabGroup.SelectedTab, editorService.ActiveTabGroup, SplitDirection.Right) : Task.CompletedTask,
                Active = editorService.ActiveTabGroup.SelectedTab is not null
            },
            new()
            {
                Name = "Split Up",
                Icon = GlobalIcons.ArrowUp,
                OnClick = () => editorService.ActiveTabGroup.SelectedTab is not null ? editorService.DuplicateTab(editorService.ActiveTabGroup.SelectedTab, editorService.ActiveTabGroup, SplitDirection.Up) : Task.CompletedTask,
                Active = editorService.ActiveTabGroup.SelectedTab is not null
            },
            new()
            {
                Name = "Split Down",
                Icon = GlobalIcons.ArrowDown,
                OnClick = () => editorService.ActiveTabGroup.SelectedTab is not null ? editorService.DuplicateTab(editorService.ActiveTabGroup.SelectedTab, editorService.ActiveTabGroup, SplitDirection.Down) : Task.CompletedTask,
                Active = editorService.ActiveTabGroup.SelectedTab is not null
            },
            ContextAction.Separator,
            new()
            {
                Name = "Duplicate Horizontal",
                Icon = GlobalIcons.SplitArrowHorizontal,
                OnClick = () => editorService.ActiveTabGroup.SelectedTab is not null ? editorService.DuplicateTabGroup(editorService.ActiveTabGroup, SplitDirection.Right) : Task.CompletedTask,
                Active = editorService.ActiveTabGroup.SelectedTab is not null
            },
            new()
            {
                Name = "Duplicate Vertical",
                Icon = GlobalIcons.SplitArrowVertical,
                OnClick = () => editorService.ActiveTabGroup.SelectedTab is not null ? editorService.DuplicateTabGroup(editorService.ActiveTabGroup, SplitDirection.Down) : Task.CompletedTask,
                Active = editorService.ActiveTabGroup.SelectedTab is not null
            },
            ContextAction.Separator,
            new()
            {
                Name = "Zoom In",
                Icon = GlobalIcons.Plus,
                OnClick = () =>
                {
                    browserContextService.ZoomIn();
                    return Task.CompletedTask;
                },
                Active = browserContextService.CanZoomIn
            },
            new()
            {
                Name = "Zoom Out",
                Icon = GlobalIcons.Minus,
                OnClick = () =>
                {
                    browserContextService.ZoomOut();
                    return Task.CompletedTask;
                },
                Active = browserContextService.CanZoomOut
            },
            new()
            {
                Name = "Reset Zoom",
                Icon = GlobalIcons.Undo,
                OnClick = () =>
                {
                    browserContextService.ResetZoom();
                    return Task.CompletedTask;
                },
                Active = browserContextService.CurrentZoom is < 1 or > 1
            },
            ContextAction.Separator,
            new()
            {
                Name = "Toggle Tag Glossary",
                Icon = GlobalIcons.Glossary,
                OnClick = () =>
                {
                    appStateService.ToggleTagGlossary();
                    return Task.CompletedTask;
                }
            }
        };

        if (debugSettings.CurrentValue.Enabled)
        {
            actions.Add(ContextAction.Separator);
            actions.Add(new ContextAction
            {
                Name = "Open Dev Tools",
                OnClick = () =>
                {
                    browserContextService.OpenDevTools();
                    return Task.CompletedTask;
                }
            });
            actions.Add(new ContextAction
            {
                Name = "Open Task Manager",
                OnClick = () =>
                {
                    browserContextService.OpenTaskManager();
                    return Task.CompletedTask;
                }
            });
        }

        return [..actions];
    }

    public ContextAction[] GetBrowserFileActions(FileData file)
    {
        var actions = new List<ContextAction>();

        if (file.IsModified)
        {
            actions.Add(new ContextAction
            {
                Name = "Save",
                Icon = GlobalIcons.Save,
                OnClick = () => appStateService.SaveFile(file)
            });
            actions.Add(new ContextAction
            {
                Name = "Revert Changes",
                Icon = GlobalIcons.Undo,
                OnClick = () => appStateService.RevertChanges(file)
            });
        }

        if (file is {IsVirtual: false, IsModified: false})
        {
            actions.Add(new ContextAction
            {
                Name = "Unload",
                Icon = GlobalIcons.X,
                OnClick = () => appStateService.UnloadFile(file)
            });
        }

        return [..actions];
    }

    public ContextAction[] GetBrowserFileContextActions(FileData file)
    {
        var actions = new List<ContextAction>
        {
            new()
            {
                Name = "Save",
                Icon = GlobalIcons.Save,
                OnClick = () => appStateService.SaveFile(file),
                Active = file.IsModified
            },
            new()
            {
                Name = "Revert Changes",
                Icon = GlobalIcons.Undo,
                OnClick = () => appStateService.RevertChanges(file),
                Active = file.IsModified
            },
            ContextAction.Separator,
            new()
            {
                Name = "Show in File Explorer",
                Icon = GlobalIcons.Open,
                OnClick = () => dialogService.OpenFileExplorer(Path.GetDirectoryName(file.InternalFilePath)!)
            }
        };

        if (file is ContainerFileData containerFile1)
        {
            actions.Add(new ContextAction
            {
                Name = "Show Content in File Explorer",
                OnClick = async () =>
                {
                    await containerFile1.Init();
                    if (containerFile1 is FolderFileData) await dialogService.OpenFileExplorer(containerFile1.InternalFilePath);
                    else await dialogService.OpenFileExplorer(containerFile1.GetTempDir());
                }
            });
        }

        if (file is not FolderFileData)
        {
            actions.Add(ContextAction.Separator);
            actions.Add(new ContextAction
            {
                Name = "Export",
                Icon = GlobalIcons.ExportFile,
                OnClick = () => appStateService.ExportRawFile(file)
            });
            if (file.IsCompressed)
            {
                actions.Add(new ContextAction
                {
                    Name = "Export (Decompressed)",
                    OnClick = () => appStateService.ExportFile(file)
                });
            }
            if (textEditorProvider.HasFactory(file)) //maybe refactor this for editors in general
            {
                actions.Add(new ContextAction
                {
                    Name = "Export (Text)",
                    OnClick = () => appStateService.ExportTextFile(file)
                });
            }
        }
        if (file is ContainerFileData containerFile2)
        {
            actions.Add(ContextAction.Separator);
            actions.Add(new ContextAction
            {
                Name = "Export Content",
                Icon = GlobalIcons.ExportFolder,
                OnClick = () => appStateService.ExportRawFolder(containerFile2, true)
            });
            actions.Add(new ContextAction
            {
                Name = "Export Content (Decompressed)",
                OnClick = () => appStateService.ExportFolder(containerFile2, true)
            });
            actions.Add(new ContextAction
            {
                Name = "Export Content (Text)",
                OnClick = () => appStateService.ExportTextFolder(containerFile2, true)
            });
        }

        if (!file.IsReadOnly)
        {
            if (file is not FolderFileData)
            {
                actions.Add(ContextAction.Separator);
                actions.Add(new ContextAction
                {
                    Name = "Import",
                    Icon = GlobalIcons.ImportFile,
                    OnClick = () => appStateService.ImportRawFile(file)
                });
                if (file.IsCompressed)
                {
                    actions.Add(new ContextAction
                    {
                        Name = "Import (Decompressed)",
                        OnClick = () => appStateService.ImportFile(file)
                    });
                }
                if (textEditorProvider.HasFactory(file)) //maybe refactor this for editors in general
                {
                    actions.Add(new ContextAction
                    {
                        Name = "Import (Text)",
                        OnClick = () => appStateService.ImportTextFile(file)
                    });
                }
            }
            if (file is ContainerFileData containerFile)
            {
                actions.Add(ContextAction.Separator);
                actions.Add(new ContextAction
                {
                    Name = "Import Content",
                    Icon = GlobalIcons.ImportFolder,
                    OnClick = () => appStateService.ImportRawFolder(containerFile, true)
                });
                actions.Add(new ContextAction
                {
                    Name = "Import Content (Decompressed)",
                    OnClick = () => appStateService.ImportFolder(containerFile, true)
                });
                actions.Add(new ContextAction
                {
                    Name = "Import Content (Text)",
                    OnClick = () => appStateService.ImportTextFolder(containerFile, true)
                });
            }
        }

        actions.Add(ContextAction.Separator);
        actions.Add(new ContextAction
        {
            Name = "Rename",
            Icon = GlobalIcons.Edit,
            OnClick = () => appStateService.RenameFile(file)
        });

        if (!file.IsVirtual)
        {
            actions.Add(ContextAction.Separator);
            actions.Add(new ContextAction
            {
                Name = "Unload",
                Icon = GlobalIcons.X,
                OnClick = () => appStateService.UnloadFile(file)
            });
        }

        if (file is IAlignedContainerFileData alignedContainerFile) //maybe move this to FileContextActionProvider too
        {
            actions.Add(ContextAction.Separator);
            actions.Add(new ContextAction
            {
                Name = "Check Alignment",
                Icon = GlobalIcons.Binary,
                OnClick = () =>
                {
                    dialogService.ShowDialog<AlignmentCheckDialog>(new Dictionary<string, object> {{nameof(AlignmentCheckDialog.File), alignedContainerFile}});
                    return Task.CompletedTask;
                }
            });
        }

        var fileActions = fileContextActionProvider.GetActions(file);
        if (fileActions.Length > 0)
        {
            actions.Add(ContextAction.Separator);
            actions.AddRange(fileActions);
        }

        return [..actions];
    }

    public ContextAction[] GetEditorTabContextActions(IEditorData tab, IEditorTabGroup tabGroup) =>
    [
        new()
        {
            Name = "Save",
            Icon = GlobalIcons.Save,
            OnClick = () => appStateService.SaveEditorData(tab),
            Active = tab.IsModified
        },
        new()
        {
            Name = "Save All",
            Icon = GlobalIcons.SaveAll,
            OnClick = () => appStateService.SaveEditorData(tabGroup.Tabs),
            Active = tabGroup.Tabs.Any(t => t.IsModified)
        },
        new()
        {
            Name = "Revert Changes",
            Icon = GlobalIcons.Undo,
            OnClick = () => appStateService.ReloadEditorData(tab),
            Active = tab.IsModified
        },
        ContextAction.Separator,
        new()
        {
            Name = "Reopen Editor with...",
            Icon = GlobalIcons.Open,
            OnClick = () => dialogService.ShowDialog<SelectEditorDialog>(new Dictionary<string, object> {{nameof(SelectEditorDialog.File), tab.File}, {nameof(SelectEditorDialog.OldTab), tab}, {nameof(SelectEditorDialog.TabGroup), tabGroup}}),
            Active = editorService.GetFactories(tab.File).Count > 1
        },
        ContextAction.Separator,
        new()
        {
            Name = "Close",
            Icon = GlobalIcons.X,
            OnClick = () => appStateService.CloseTab(tab, tabGroup)
        },
        new()
        {
            Name = "Close Others",
            OnClick = () => appStateService.CloseTabs(tabGroup.Tabs.Where(t => t != tab), tabGroup)
        },
        new()
        {
            Name = "Close Saved",
            OnClick = () => appStateService.CloseTabs(tabGroup.Tabs.Where(t => !t.IsModified), tabGroup)
        },
        new()
        {
            Name = "Close All",
            OnClick = () => appStateService.CloseTabGroup(tabGroup)
        },
        ContextAction.Separator,
        new()
        {
            Name = "Split Left",
            Icon = GlobalIcons.ArrowLeft,
            OnClick = () => editorService.DuplicateTab(tab, tabGroup, SplitDirection.Left)
        },
        new()
        {
            Name = "Split Right",
            Icon = GlobalIcons.ArrowRight,
            OnClick = () => editorService.DuplicateTab(tab, tabGroup, SplitDirection.Right)
        },
        new()
        {
            Name = "Split Up",
            Icon = GlobalIcons.ArrowUp,
            OnClick = () => editorService.DuplicateTab(tab, tabGroup, SplitDirection.Up)
        },
        new()
        {
            Name = "Split Down",
            Icon = GlobalIcons.ArrowDown,
            OnClick = () => editorService.DuplicateTab(tab, tabGroup, SplitDirection.Down)
        }
    ];

    public ContextAction[] GetEditorTabGroupContextActions(IEditorTabGroup tabGroup) =>
    [
        new()
        {
            Name = "Save All",
            Icon = GlobalIcons.SaveAll,
            OnClick = () => appStateService.SaveEditorData(tabGroup.Tabs),
            Active = tabGroup.Tabs.Any(tab => tab.IsModified)
        },
        ContextAction.Separator,
        new()
        {
            Name = "Close Saved",
            Icon = GlobalIcons.X,
            OnClick = () => appStateService.CloseTabs(tabGroup.Tabs.Where(tab => !tab.IsModified), tabGroup)
        },
        new()
        {
            Name = "Close All",
            OnClick = () => appStateService.CloseTabGroup(tabGroup)
        },
        ContextAction.Separator,
        new()
        {
            Name = "Duplicate Horizontal",
            Icon = GlobalIcons.SplitArrowHorizontal,
            OnClick = () => editorService.DuplicateTabGroup(tabGroup, SplitDirection.Right)
        },
        new()
        {
            Name = "Duplicate Vertical",
            Icon = GlobalIcons.SplitArrowVertical,
            OnClick = () => editorService.DuplicateTabGroup(tabGroup, SplitDirection.Down)
        }
    ];
}