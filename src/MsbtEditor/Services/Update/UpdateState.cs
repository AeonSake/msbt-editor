﻿namespace MsbtEditor;

internal enum UpdateState
{
    Idle = 0,
    NewVersionFound = 1,
    UpdateDone = 2,

    Checking = 10,
    Downloading = 11,
    Unpacking = 12,
    Restarting = 13,

    CheckFailed = 20,
    DownloadFailed = 21,
    UnpackFailed = 22
}