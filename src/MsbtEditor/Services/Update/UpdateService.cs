﻿using System.Diagnostics;
using System.IO.Compression;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace MsbtEditor;

internal sealed partial class UpdateService
{
    #region private members
    private const string ReleaseUrl = "https://gitlab.com/api/v4/projects/53883547/releases";
    private const string ReleasePagePrefix = "https://gitlab.com/AeonSake/msbt-editor/-/releases/";
    private const string UploadFilePrefix = "https://gitlab.com/-/project/53883547";
    private const string UpdaterFileName = "MsbtEditor.Updater";

    private readonly ScopeManagerService _scopeManagerService;
    private readonly ILogger<UpdateService> _logger;
    private UpdateState _state;
    #endregion

    #region constructor
    public UpdateService(ScopeManagerService scopeManagerService, IOptionsSnapshot<UpdateSettings> settings, ILogger<UpdateService> logger)
    {
        _scopeManagerService = scopeManagerService;
        _logger = logger;

        var assembly = Assembly.GetExecutingAssembly();
        var version = assembly.GetName().Version!;
        var hash = assembly.GetCustomAttribute<GitHashAttribute>()?.Hash ?? string.Empty;
        var buildTime = assembly.GetCustomAttribute<BuildTimeAttribute>()?.Time ?? DateTime.MinValue;
        var copyright = assembly.GetCustomAttribute<AssemblyCopyrightAttribute>()?.Copyright ?? string.Empty;

        CurrentVersion = new LocalVersionInfo
        {
            Major = version.Major,
            Minor = version.Minor,
            Patch = version.Build,
            Hash = hash,
            BuildTime = buildTime,
            Copyright = copyright
        };

        if (settings.Value.CheckOnStart) _ = Check();
    }
    #endregion

    #region public properties
    public LocalVersionInfo CurrentVersion { get; }

    public UpdateVersionInfo? NextVersion { get; private set; }

    public UpdateState State
    {
        get => _state;
        set
        {
            if (_state == value) return;
            _state = value;
            StateChanged?.Invoke(this, EventArgs.Empty);
        }
    }
    #endregion

    #region public events
    public event EventHandler? StateChanged;
    #endregion

    #region public methods
    public async Task Check()
    {
        if (State is > UpdateState.NewVersionFound and not UpdateState.CheckFailed) return;

        State = UpdateState.Checking;
        try
        {
            using var client = new HttpClient();
            var response = await client.GetStringAsync(ReleaseUrl);

            if (string.IsNullOrEmpty(response))
            {
                State = UpdateState.CheckFailed;
                return;
            }

            var latestEntry = JsonConvert.DeserializeObject<IList<UpdateData>>(response)?.FirstOrDefault();
            if (latestEntry is null || !Version.TryParse(latestEntry.TagName.TrimStart('v') + ".0", out var latestVersion))
            {
                State = UpdateState.CheckFailed;
                return;
            }

            string[] identifiers = [];
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                identifiers = RuntimeInformation.OSArchitecture switch
                {
                    Architecture.X86   => ["win_x86", "win-x86"],
                    Architecture.X64   => ["win_x64", "win-x64"],
                    Architecture.Arm   => ["win_arm32", "win-arm32"],
                    Architecture.Arm64 => ["win_arm64", "win-arm64"],
                    _                  => [],
                };
            }

            string? updateLink = null;
            foreach (Match match in LinkRegex().Matches(latestEntry.Description))
            {
                var link = match.Groups[2].Value;
                if (!link.EndsWith(".zip")) continue;

                foreach (var identifier in identifiers)
                {
                    if (!link.EndsWith("_" + identifier + ".zip", StringComparison.OrdinalIgnoreCase)) continue;
                    updateLink = link;
                    break;
                }

                if (updateLink is not null) break;
            }

            var description = latestEntry.Description;
            var assetPosition = description.LastIndexOf("\n\n", StringComparison.InvariantCulture);
            if (assetPosition > -1) description = description[..assetPosition];
            description = LinkRegex().Replace(description.Replace('`', '"'), m => m.Groups[1].Value).Trim();

            var updateInfo = new UpdateVersionInfo
            {
                Major = latestVersion.Major,
                Minor = latestVersion.Minor,
                Patch = latestVersion.Build,
                Hash = latestEntry.Commit.Id,
                Title = latestEntry.Name,
                Description = description,
                ReleasedTime = latestEntry.ReleasedAt,
                ReleaseLink = ReleasePagePrefix + latestEntry.TagName,
                UpdateLink = updateLink is null ? null : updateLink.StartsWith("https://") ? updateLink : UploadFilePrefix + updateLink
            };

            NextVersion = updateInfo;
            State = IsNewUpdate(updateInfo) ? UpdateState.NewVersionFound : UpdateState.Idle;
        }
        catch (Exception ex)
        {
            State = UpdateState.CheckFailed;
            _logger.LogError(ex, "Failed to check for update.");
        }
    }

    public async Task Install()
    {
        if (State is not (UpdateState.NewVersionFound or UpdateState.DownloadFailed or UpdateState.UnpackFailed) || NextVersion?.UpdateLink is null) return;

        var fileName = Path.Combine(CommonPaths.UpdateFolder, Path.GetFileName(NextVersion.UpdateLink));
        var contentFolder = Path.Combine(CommonPaths.UpdateFolder, "content");

        try
        {
            State = UpdateState.Downloading;

            if (Directory.Exists(CommonPaths.UpdateFolder)) Directory.Delete(CommonPaths.UpdateFolder, true);
            Directory.CreateDirectory(CommonPaths.UpdateFolder);

            using var client = new HttpClient();
            await using var downloadStream = await client.GetStreamAsync(NextVersion.UpdateLink);
            await using var writeStream = File.Create(fileName);
            await downloadStream.CopyToAsync(writeStream);
        }
        catch (Exception ex)
        {
            State = UpdateState.DownloadFailed;
            _logger.LogError(ex, "Failed to download update.");
            return;
        }

        try
        {
            State = UpdateState.Unpacking;

            Directory.CreateDirectory(contentFolder);

            await using var readStream = File.OpenRead(fileName);
            ZipFile.ExtractToDirectory(readStream, Path.Combine(CommonPaths.UpdateFolder, contentFolder));

            foreach (var file in Directory.GetFiles(contentFolder, UpdaterFileName + "*"))
            {
                File.Move(file, Path.Combine(CommonPaths.AppRoot, Path.GetRelativePath(contentFolder, file)), true);
            }
        }
        catch (Exception ex)
        {
            State = UpdateState.UnpackFailed;
            _logger.LogError(ex, "Failed to unpack update.");
            return;
        }

        State = UpdateState.UpdateDone;
    }

    public async Task Restart()
    {
        if (State is not UpdateState.UpdateDone) return;

        State = UpdateState.Restarting;
        try
        {
            foreach (var provider in _scopeManagerService.Scopes.ToArray())
            {
                var appStateService = provider.GetRequiredService<AppStateService>();
                if (await appStateService.OnWindowClose()) continue;

                State = UpdateState.UpdateDone;
                return;
            }

            Process.Start(new ProcessStartInfo(Path.Combine(CommonPaths.AppRoot, UpdaterFileName + ".exe")));
            Environment.Exit(0);
        }
        catch (Exception ex)
        {
            State = UpdateState.UpdateDone;
            _logger.LogError(ex, "Failed to restart application.");
        }
    }
    #endregion

    #region private methods
    private bool IsNewUpdate(UpdateVersionInfo info)
    {
        if (info.Hash.Equals(CurrentVersion.Hash)) return false;
        if (info.Major > CurrentVersion.Major) return true;
        if (info.Major < CurrentVersion.Major) return false;
        if (info.Minor > CurrentVersion.Minor) return true;
        if (info.Minor < CurrentVersion.Minor) return false;
        if (info.Patch > CurrentVersion.Patch) return true;
        if (info.Patch < CurrentVersion.Patch) return false;
        return info.ReleasedTime > CurrentVersion.BuildTime;
    }
    #endregion

    #region helper classes
    private class UpdateData
    {
        [JsonProperty("name")]
        public string Name { get; set; } = null!;

        [JsonProperty("tag_name")]
        public string TagName { get; set; } = null!;

        [JsonProperty("description")]
        public string Description { get; set; } = null!;

        [JsonProperty("released_at")]
        public DateTime ReleasedAt { get; set; }

        [JsonProperty("commit")]
        public CommitData Commit { get; set; } = null!;
    }

    private class CommitData
    {
        [JsonProperty("id")]
        public string Id { get; set; } = null!;
    }

    [GeneratedRegex(@"\[([^\]]+)\]\(([a-zA-Z0-9#\-_/.:]+)\)")]
    private static partial Regex LinkRegex();
    #endregion
}