﻿namespace MsbtEditor;

internal class VersionInfo
{
    #region public properties
    public required int Major { get; init; }

    public required int Minor { get; init; }

    public required int Patch { get; init; }

    public required string Hash { get; init; } = string.Empty;

    public string ShortHash => Hash.Length > 8 ? Hash[..8] : Hash;
    #endregion

    #region public methods
    public override string ToString() => ToLongString();

    public string ToShortString() => $"{Major}.{Minor}.{Patch}";

    public string ToLongString() => string.IsNullOrEmpty(Hash) ? $"{Major}.{Minor}.{Patch}" : $"{Major}.{Minor}.{Patch}-{ShortHash}";

    public string ToFullString() => string.IsNullOrEmpty(Hash) ? $"{Major}.{Minor}.{Patch}" : $"{Major}.{Minor}.{Patch}-{Hash}";
    #endregion
}

internal sealed class LocalVersionInfo : VersionInfo
{
    public required DateTime BuildTime { get; init; }

    public required string Copyright { get; init; }
}

internal sealed class UpdateVersionInfo : VersionInfo
{
    public required string Title { get; init; }

    public required string Description { get; init; }

    public required DateTime ReleasedTime { get; set; }

    public required string ReleaseLink { get; init; }

    public required string? UpdateLink { get; init; }
}