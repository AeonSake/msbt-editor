﻿using Microsoft.JSInterop;

namespace MsbtEditor;

internal sealed class JsRuntimeProvider
{
    #region public properties
    public bool Initialized => Runtime is not MockRuntime;

    public IJSRuntime Runtime { get; private set; } = new MockRuntime();
    #endregion

    #region public methods
    public void SetRuntime(IJSRuntime jsRuntime) => Runtime = jsRuntime;
    #endregion

    #region helper class
    private class MockRuntime : IJSRuntime
    {
        public ValueTask<TValue> InvokeAsync<TValue>(string identifier, object?[]? args) => ValueTask.FromResult(Activator.CreateInstance<TValue>());

        public ValueTask<TValue> InvokeAsync<TValue>(string identifier, CancellationToken cancellationToken, object?[]? args) => ValueTask.FromResult(Activator.CreateInstance<TValue>());
    }
    #endregion
}