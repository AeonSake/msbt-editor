﻿using System.Diagnostics;
using System.Text;
using MsbtEditor.Components.Dialogs;

namespace MsbtEditor;

internal sealed class AppStateService : IDisposable
{
    #region private members
    private readonly FileBrowserService _fileBrowserService;
    private readonly FileDataFactory _fileDataFactory;
    private readonly TextEditorProvider _textEditorProvider;
    private readonly EditorService _editorService;
    private readonly GameConfigService _gameConfigService;
    private readonly StatusService _statusService;
    private readonly DialogService _dialogService;
    private GameConfigFileData _currentGameConfig;
    #endregion

    #region constructor
    public AppStateService(
        FileBrowserService fileBrowserService,
        FileDataFactory fileDataFactory,
        TextEditorProvider textEditorProvider,
        EditorService editorService,
        GameConfigService gameConfigService,
        StatusService statusService,
        DialogService dialogService)
    {
        _fileBrowserService = fileBrowserService;
        _textEditorProvider = textEditorProvider;
        _editorService = editorService;
        _gameConfigService = gameConfigService;
        _statusService = statusService;
        _dialogService = dialogService;
        _fileDataFactory = fileDataFactory;

        _currentGameConfig = gameConfigService.CurrentFile;

        _fileBrowserService.SelectionChanged += OnFileBrowserSelectionChanged;
        _gameConfigService.CurrentConfigChanged += OnGameConfigChanged;
    }
    #endregion

    #region public events
    public event EventHandler? GlossaryToggled;
    #endregion

    #region public methods
    public Task<bool> LoadFiles() => LoadFiles(null);

    public async Task<bool> LoadFiles(string[]? filePaths)
    {
        if (filePaths is null)
        {
            var result = await _dialogService.ShowOpenFileDialog("All Files|*.*", true);
            if (result.Type is DialogResultType.Ok) filePaths = result.Value;
        }
        if (filePaths is null || filePaths.Length == 0) return false;

        _statusService.Set("Loading files...");
        var sw = Stopwatch.StartNew();

        var files = new List<FileData>();
        foreach (var filePath in filePaths)
        {
            if (!File.Exists(filePath)) continue;

            try
            {
                var provider = new FileStorageProvider(filePath, false);
                files.Add(_fileDataFactory.Create(provider));
            }
            catch (Exception ex)
            {
                _statusService.Set("Failed to load file");
                await _dialogService.ShowMessageDialog($"Failed to load \"{filePath}\": {ex.Message}", StatusType.Error);
                foreach (var file in files) file.Dispose();
                return false;
            }
        }

        var addedFiles = _fileBrowserService.AddRange(files);
        var fileSelected = false;
        foreach (var file in files)
        {
            if (_fileBrowserService.Contains(file))
            {
                if (fileSelected) continue;
                _fileBrowserService.Select(file);
                fileSelected = true;
            }
            else file.Dispose();
        }

        sw.Stop();
        _statusService.Set($"Loaded {addedFiles} file{(addedFiles == 1 ? "" : "s")} in {sw.Elapsed.TotalSeconds:F2}s");
        return true;
    }

    public Task<bool> LoadFolder() => LoadFolder(null);

    public async Task<bool> LoadFolder(string? folderPath)
    {
        if (folderPath is null)
        {
            var result = await _dialogService.ShowOpenFolderDialog();
            if (result.Type == DialogResultType.Ok) folderPath = result.Value;
        }
        if (string.IsNullOrEmpty(folderPath) || !Directory.Exists(folderPath)) return false;

        _statusService.Set("Loading folder...");
        var sw = Stopwatch.StartNew();

        FolderFileData? file = null;
        try
        {
            file = new FolderFileData(folderPath, _fileDataFactory, false);
            if (_fileBrowserService.Add(file)) await file.Init();
            else file.Dispose();
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to load folder");
            await _dialogService.ShowMessageDialog($"Failed to load \"{folderPath}\": {ex.Message}", StatusType.Error);
            file?.Dispose();
            return false;
        }

        sw.Stop();
        _statusService.Set($"Loaded folder content in {sw.Elapsed.TotalSeconds:F2}s");
        return true;
    }

    public async Task<bool> SaveFile(FileData file)
    {
        if (!file.IsModified) return true;

        Stopwatch? sw = null;

        //save all other editors first
        if (file == _currentGameConfig)
        {
            var unsavedTabs = _editorService.GetAllUniqueTabs();
            if (unsavedTabs.Count > 0)
            {
                var result = await _dialogService.ShowMessageDialog("You have unsaved changes. Save files before updating Game Config?", StatusType.Warning, DialogButtons.Yes|DialogButtons.No|DialogButtons.Cancel);
                switch (result.Type)
                {
                    case DialogResultType.Yes:
                        sw = Stopwatch.StartNew();
                        if (!await SaveEditorData(unsavedTabs)) return false;
                        break;
                    case DialogResultType.Cancel:
                        return false;
                }
            }
        }

        var type = file is FolderFileData ? "folder content" : "file";
        _statusService.Set($"Saving {type}...");
        sw ??= Stopwatch.StartNew();

        try
        {
            await file.Save();
        }
        catch (Exception ex)
        {
            _statusService.Set($"Failed to save {type}");
            await _dialogService.ShowMessageDialog($"Failed to save \"{file.Name}\": {ex.Message}", StatusType.Error);
            return false;
        }

        sw.Stop();
        _statusService.Set($"File saved in {sw.Elapsed.TotalSeconds:F2}s");
        return true;
    }

    public async Task<bool> SaveFiles(IEnumerable<FileData> files)
    {
        var filesToSave = files.Where(file => file.IsModified).ToArray();
        if (filesToSave.Length == 0) return true;

        Stopwatch? sw = null;

        //save all other editors first
        if (filesToSave.Contains(_currentGameConfig))
        {
            var unsavedTabs = _editorService.GetAllUniqueTabs();
            if (unsavedTabs.Count > 0)
            {
                var result = await _dialogService.ShowMessageDialog("You have unsaved changes. Save files before updating Game Config?", StatusType.Warning, DialogButtons.Yes|DialogButtons.No|DialogButtons.Cancel);
                switch (result.Type)
                {
                    case DialogResultType.Yes:
                        sw = Stopwatch.StartNew();
                        if (!await SaveEditorData(unsavedTabs)) return false;
                        break;
                    case DialogResultType.Cancel:
                        return false;
                }
            }
        }

        _statusService.Set("Saving files...");
        sw ??= Stopwatch.StartNew();

        foreach (var file in filesToSave)
        {
            try
            {
                await file.Save();
            }
            catch (Exception ex)
            {
                _statusService.Set("Failed to save file");
                await _dialogService.ShowMessageDialog($"Failed to save \"{file.Name}\": {ex.Message}", StatusType.Error);
                return false;
            }
        }

        sw.Stop();
        _statusService.Set($"Files saved in {sw.Elapsed.TotalSeconds:F2}s");
        return true;
    }

    public async Task<bool> SaveAllFiles()
    {
        _statusService.Set("Saving files...");
        var sw = Stopwatch.StartNew();

        try
        {
            foreach (var file in _gameConfigService.Configs) await file.Save();
            foreach (var file in _fileBrowserService.Files) await file.Save();
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to save file");
            await _dialogService.ShowMessageDialog($"Failed to save a file: {ex.Message}", StatusType.Error);
            return false;
        }

        sw.Stop();
        _statusService.Set($"Files saved in {sw.Elapsed.TotalSeconds:F2}s");
        return true;
    }

    public async Task<bool> RevertChanges(FileData file)
    {
        if (!file.IsModified) return true;

        var result = await _dialogService.ShowMessageDialog($"Are you sure you want to revert changes on \"{file.Name}\"?", buttons: DialogButtons.Ok|DialogButtons.Cancel);
        if (result.Type is not DialogResultType.Ok) return false;

        _statusService.Set($"Reverting changes on \"{file.Name}\"...");
        var sw = Stopwatch.StartNew();

        try
        {
            foreach (var tab in _editorService.GetAllUniqueTabs(file)) await tab.Reload();

            if (file is ContainerFileData containerFile)
            {
                var activeTabs = _editorService.GetAllUniqueTabs();
                var oldEditorFiles = containerFile.FindAll(f => activeTabs.Any(tab => tab.File == f));
                await containerFile.Reload();
                foreach (var oldEditorFile in oldEditorFiles) await _editorService.CloseFile(oldEditorFile);

                var selectedFile = _fileBrowserService.SelectedFile;
                if (selectedFile is not null)
                {
                    var oldFilePath = selectedFile.GetFilePath();
                    var foundFile = containerFile.Find(f => f.GetFilePath().Equals(oldFilePath));
                    _fileBrowserService.Select(foundFile);
                }
            }
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to revert changes");
            await _dialogService.ShowMessageDialog($"Failed to revert changes on \"{file.Name}\": {ex.Message}", StatusType.Error);
            return false;
        }

        sw.Stop();
        _statusService.Set($"Changes reverted in {sw.Elapsed.TotalSeconds:F2}s");
        return true;
    }

    public async Task ExportFile(FileData file)
    {
        if (file is FolderFileData folder)
        {
            await ExportFolder(folder);
            return;
        }

        var result = await _dialogService.ShowSaveFileDialog(file.Name);
        var filePath = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(filePath)) return;

        _statusService.Set("Exporting file...");
        var sw = Stopwatch.StartNew();

        try
        {
            await using var writeStream = File.Create(filePath);
            await file.Read(writeStream);
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to export file");
            await _dialogService.ShowMessageDialog($"Failed to export \"{file.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"File exported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ExportRawFile(FileData file)
    {
        if (file is FolderFileData folder)
        {
            await ExportRawFolder(folder);
            return;
        }

        var result = await _dialogService.ShowSaveFileDialog(file.Name);
        var filePath = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(filePath)) return;

        _statusService.Set("Exporting file...");
        var sw = Stopwatch.StartNew();

        try
        {
            await using var writeStream = File.Create(filePath);
            await file.Read(writeStream, true);
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to export file");
            await _dialogService.ShowMessageDialog($"Failed to export \"{file.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"File exported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ExportTextFile(FileData file)
    {
        if (file is FolderFileData folder)
        {
            await ExportTextFolder(folder);
            return;
        }

        var factory = _textEditorProvider.GetFactories(file).FirstOrDefault();
        if (factory is null) return;

        var result = await _dialogService.ShowSaveFileDialog(file.Name + ".txt");
        var filePath = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(filePath)) return;

        _statusService.Set("Exporting file...");
        var sw = Stopwatch.StartNew();

        var data = (ITextEditorData) await factory.CreateEditor(file);
        try
        {
            var content = await data.ExportText();
            await File.WriteAllTextAsync(filePath, content, Encoding.UTF8);
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to export file");
            await _dialogService.ShowMessageDialog($"Failed to export \"{file.Name}\": {ex.Message}", StatusType.Error);
            return;
        }
        finally
        {
            await data.DisposeAsync();
        }

        sw.Stop();
        _statusService.Set($"File exported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ExportFolder(ContainerFileData containerFile, bool containerAsFolder = false)
    {
        var result = await _dialogService.ShowOpenFolderDialog();
        var filePath = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(filePath)) return;

        var type = containerFile is FolderFileData ? "folder" : "file content";
        _statusService.Set($"Exporting {type}...");
        var sw = Stopwatch.StartNew();

        try
        {
            await containerFile.InitAll();
            var containerPath = containerFile.GetFilePath();
            foreach (var (_, file) in FolderBuilder.BuildMap(containerFile.Children, containerAsFolder))
            {
                var relativePath = Path.GetRelativePath(containerPath, file.GetFilePath());
                var targetPath = Path.Combine(filePath, relativePath);
                var targetDir = Path.GetDirectoryName(targetPath);

                try
                {
                    if (!string.IsNullOrEmpty(targetDir)) Directory.CreateDirectory(targetDir);
                    await using var writeStream = File.Create(targetPath);
                    await file.Read(writeStream);
                }
                catch (Exception ex)
                {
                    _statusService.Set($"Failed to export {type}");
                    await _dialogService.ShowMessageDialog($"Failed to export \"{file.Name}\": {ex.Message}", StatusType.Error);
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            _statusService.Set($"Failed to export {type}");
            await _dialogService.ShowMessageDialog($"Failed to export \"{containerFile.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"{type.Capitalize()} exported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ExportRawFolder(ContainerFileData containerFile, bool containerAsFolder = false)
    {
        var result = await _dialogService.ShowOpenFolderDialog();
        var filePath = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(filePath)) return;

        var type = containerFile is FolderFileData ? "folder" : "file content";
        _statusService.Set($"Exporting {type}...");
        var sw = Stopwatch.StartNew();

        try
        {
            await containerFile.InitAll();
            var containerPath = containerFile.GetFilePath();
            foreach (var (_, file) in FolderBuilder.BuildMap(containerFile.Children, containerAsFolder))
            {
                var relativePath = Path.GetRelativePath(containerPath, file.GetFilePath());
                var targetPath = Path.Combine(filePath, relativePath);
                var targetDir = Path.GetDirectoryName(targetPath);

                try
                {
                    if (!string.IsNullOrEmpty(targetDir)) Directory.CreateDirectory(targetDir);
                    await using var writeStream = File.Create(targetPath);
                    await file.Read(writeStream, true);
                }
                catch (Exception ex)
                {
                    _statusService.Set($"Failed to export {type}");
                    await _dialogService.ShowMessageDialog($"Failed to export \"{file.Name}\": {ex.Message}", StatusType.Error);
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            _statusService.Set($"Failed to export {type}");
            await _dialogService.ShowMessageDialog($"Failed to export \"{containerFile.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"{type.Capitalize()} exported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ExportTextFolder(ContainerFileData containerFile, bool containerAsFolder = false)
    {
        var result = await _dialogService.ShowOpenFolderDialog();
        var filePath = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(filePath)) return;

        var type = containerFile is FolderFileData ? "folder" : "file content";
        _statusService.Set($"Exporting {type}...");
        var sw = Stopwatch.StartNew();

        try
        {
            await containerFile.InitAll();
            var containerPath = containerFile.GetFilePath();
            foreach (var (_, file) in FolderBuilder.BuildMap(containerFile.Children,containerAsFolder))
            {
                var factory = _textEditorProvider.GetFactories(file).FirstOrDefault();
                if (factory is null) continue;

                var relativePath = Path.GetRelativePath(containerPath, file.GetFilePath());
                var targetPath = Path.Combine(filePath, relativePath) + ".txt";
                var targetDir = Path.GetDirectoryName(targetPath);

                var data = (ITextEditorData) await factory.CreateEditor(file);
                try
                {
                    if (!string.IsNullOrEmpty(targetDir)) Directory.CreateDirectory(targetDir);
                    var content = await data.ExportText();
                    await File.WriteAllTextAsync(targetPath, content, Encoding.UTF8);
                }
                catch (Exception ex)
                {
                    _statusService.Set($"Failed to export {type}");
                    await _dialogService.ShowMessageDialog($"Failed to export \"{file.Name}\": {ex.Message}", StatusType.Error);
                    return;
                }
                finally
                {
                    await data.DisposeAsync();
                }
            }
        }
        catch (Exception ex)
        {
            _statusService.Set($"Failed to export {type}");
            await _dialogService.ShowMessageDialog($"Failed to export \"{containerFile.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"{type.Capitalize()} exported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ImportFile(FileData file)
    {
        if (file is FolderFileData folder)
        {
            await ImportFolder(folder);
            return;
        }

        var result = await _dialogService.ShowOpenFileDialog("All Files|*.*", false);
        var filePath = result.Type is DialogResultType.Ok && result.Value?.Length > 0 ? result.Value[0] : null;
        if (string.IsNullOrEmpty(filePath)) return;

        _statusService.Set("Importing file...");
        var sw = Stopwatch.StartNew();

        try
        {
            await using var readStream = File.OpenRead(filePath);
            await file.Write(readStream);
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to import file");
            await _dialogService.ShowMessageDialog($"Failed to import \"{file.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"File imported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ImportRawFile(FileData file)
    {
        if (file is FolderFileData folder)
        {
            await ImportRawFolder(folder);
            return;
        }

        var result = await _dialogService.ShowOpenFileDialog("All Files|*.*", false);
        var filePath = result.Type is DialogResultType.Ok && result.Value?.Length > 0 ? result.Value[0] : null;
        if (string.IsNullOrEmpty(filePath)) return;

        _statusService.Set("Importing file...");
        var sw = Stopwatch.StartNew();

        try
        {
            await using var readStream = File.OpenRead(filePath);
            await file.Write(readStream, true);
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to import file");
            await _dialogService.ShowMessageDialog($"Failed to import \"{file.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"File imported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ImportTextFile(FileData file)
    {
        if (file is FolderFileData folder)
        {
            await ImportTextFolder(folder);
            return;
        }

        var factory = _textEditorProvider.GetFactories(file).FirstOrDefault();
        if (factory is null) return;

        var result = await _dialogService.ShowOpenFileDialog("All Files|*.*", false);
        var filePath = result.Type is DialogResultType.Ok && result.Value?.Length > 0 ? result.Value[0] : null;
        if (string.IsNullOrEmpty(filePath)) return;

        _statusService.Set("Importing file...");
        var sw = Stopwatch.StartNew();

        var data = (ITextEditorData) await factory.CreateEditor(file);
        try
        {
            var content = await File.ReadAllTextAsync(filePath);
            await data.ImportText(content);
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to import file");
            await _dialogService.ShowMessageDialog($"Failed to import \"{file.Name}\": {ex.Message}", StatusType.Error);
            return;
        }
        finally
        {
            await data.DisposeAsync();
        }

        sw.Stop();
        _statusService.Set($"File imported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ImportFolder(ContainerFileData containerFile, bool containerAsFolder = false)
    {
        var result = await _dialogService.ShowOpenFolderDialog();
        var filePath = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(filePath)) return;

        var type = containerFile is FolderFileData ? "folder" : "file content";
        _statusService.Set($"Importing {type}...");
        var sw = Stopwatch.StartNew();

        try
        {
            await containerFile.InitAll();
            var filePaths = Directory.GetFiles(filePath, "*", SearchOption.AllDirectories);
            foreach (var (importFilePath, file) in FolderBuilder.MatchPaths(containerFile, filePath, filePaths, (file, fileName) => file.Name.Equals(fileName), containerAsFolder))
            {
                try
                {
                    await using var readStream = File.OpenRead(importFilePath);
                    await file.Write(readStream);
                }
                catch (Exception ex)
                {
                    _statusService.Set($"Failed to import {type}");
                    await _dialogService.ShowMessageDialog($"Failed to import \"{file.Name}\": {ex.Message}", StatusType.Error);
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            _statusService.Set($"Failed to import {type}");
            await _dialogService.ShowMessageDialog($"Failed to import \"{containerFile.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"{type.Capitalize()} imported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ImportRawFolder(ContainerFileData containerFile, bool containerAsFolder = false)
    {
        var result = await _dialogService.ShowOpenFolderDialog();
        var filePath = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(filePath)) return;

        var type = containerFile is FolderFileData ? "folder" : "file content";
        _statusService.Set($"Importing {type}...");
        var sw = Stopwatch.StartNew();

        try
        {
            await containerFile.InitAll();
            var filePaths = Directory.GetFiles(filePath, "*", SearchOption.AllDirectories);
            foreach (var (importFilePath, file) in FolderBuilder.MatchPaths(containerFile, filePath, filePaths, (file, fileName) => file.Name.Equals(fileName), containerAsFolder))
            {
                try
                {
                    await using var readStream = File.OpenRead(importFilePath);
                    await file.Write(readStream, true);
                }
                catch (Exception ex)
                {
                    _statusService.Set($"Failed to import {type}");
                    await _dialogService.ShowMessageDialog($"Failed to import \"{file.Name}\": {ex.Message}", StatusType.Error);
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            _statusService.Set($"Failed to import {type}");
            await _dialogService.ShowMessageDialog($"Failed to import \"{containerFile.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"{type.Capitalize()} imported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task ImportTextFolder(ContainerFileData containerFile, bool containerAsFolder = false)
    {
        var result = await _dialogService.ShowOpenFolderDialog();
        var filePath = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(filePath)) return;

        var type = containerFile is FolderFileData ? "folder" : "file content";
        _statusService.Set($"Importing {type}...");
        var sw = Stopwatch.StartNew();

        try
        {
            await containerFile.InitAll();
            var filePaths = Directory.GetFiles(filePath, "*", SearchOption.AllDirectories);
            foreach (var (importFilePath, file) in FolderBuilder.MatchPaths(containerFile, filePath, filePaths, (file, fileName) => (file.Name + ".txt").Equals(fileName), containerAsFolder))
            {
                var factory = _textEditorProvider.GetFactories(file).FirstOrDefault();
                if (factory is null) continue;

                var data = (ITextEditorData) await factory.CreateEditor(file);
                try
                {
                    var content = await File.ReadAllTextAsync(importFilePath, Encoding.UTF8);
                    await data.ImportText(content);
                }
                catch (Exception ex)
                {
                    _statusService.Set($"Failed to import {type}");
                    await _dialogService.ShowMessageDialog($"Failed to import \"{file.Name}\": {ex.Message}", StatusType.Error);
                    return;
                }
                finally
                {
                    await data.DisposeAsync();
                }
            }
        }
        catch (Exception ex)
        {
            _statusService.Set($"Failed to import {type}");
            await _dialogService.ShowMessageDialog($"Failed to import \"{containerFile.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        sw.Stop();
        _statusService.Set($"{type.Capitalize()} imported in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task<bool> RenameFile(FileData file)
    {
        if (file.Parent?.IsReadOnly ?? false) return false;

        var result = await _dialogService.ShowDialog<FileNameDialog, string>(new Dictionary<string, object>
        {
            {nameof(FileNameDialog.Message), "Choose a new name for the file:"},
            {nameof(FileNameDialog.Name), file.Name}
        });
        var fileName = result.Type is DialogResultType.Ok ? result.Value : null;
        if (string.IsNullOrEmpty(fileName)) return false;

        try
        {
            await file.Rename(fileName);
            _fileBrowserService.Refresh();
        }
        catch (Exception ex)
        {
            _statusService.Set($"Failed to rename {(file is FolderFileData ? "folder" : "file")}");
            await _dialogService.ShowMessageDialog($"Failed to rename \"{file.Name}\": {ex.Message}", StatusType.Error);
            return false;
        }

        return true;
    }

    public async Task<bool> LoadEditorData(FileData file, bool asPreview = false)
    {
        var factory = _editorService.GetDefaultFactory(file);
        if (factory is null) return false;

        try
        {
            var data = await factory.CreateEditor(file);
            await _editorService.LoadTab(data, _editorService.ActiveTabGroup, asPreview);
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to load editor content");
            await _dialogService.ShowMessageDialog($"Failed to create editor content for file: {ex.Message}", StatusType.Error);
            return false;
        }

        return true;
    }

    public async Task<bool> LoadEditorData(FileData file, IEditorTabGroup tabGroup, int index)
    {
        var factory = _editorService.GetDefaultFactory(file);
        if (factory is null) return false;

        try
        {
            var data = await factory.CreateEditor(file);
            await _editorService.LoadTab(data, tabGroup, index);
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to load editor content");
            await _dialogService.ShowMessageDialog($"Failed to create editor content for file: {ex.Message}", StatusType.Error);
            return false;
        }

        return true;
    }

    public async Task<bool> LoadEditorData(FileData file, IEditorTabGroup tabGroup, SplitDirection direction)
    {
        var factory = _editorService.GetDefaultFactory(file);
        if (factory is null) return false;

        try
        {
            var data = await factory.CreateEditor(file);
            await _editorService.LoadTab(data, tabGroup, direction);
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to load editor content");
            await _dialogService.ShowMessageDialog($"Failed to create editor content for file: {ex.Message}", StatusType.Error);
            return false;
        }

        return true;
    }

    public async Task<bool> ReloadEditorData(IEditorData tab)
    {
        try
        {
            await tab.Reload();
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to reload editor content");
            await _dialogService.ShowMessageDialog($"Failed to reload editor content: {ex.Message}", StatusType.Error);
            return false;
        }

        return true;
    }

    public async Task<bool> SaveEditorData(IEditorData tab)
    {
        try
        {
            await tab.Save();
        }
        catch (Exception ex)
        {
            _statusService.Set("Failed to save editor content");
            await _dialogService.ShowMessageDialog($"Failed to save editor content to file: {ex.Message}", StatusType.Error);
            return false;
        }

        return true;
    }

    public async Task<bool> SaveEditorData(IEnumerable<IEditorData> tabs)
    {
        foreach (var tab in tabs)
        {
            try
            {
                await tab.Save();
            }
            catch (Exception ex)
            {
                _statusService.Set("Failed to save editor content");
                await _dialogService.ShowMessageDialog($"Failed to save editor content to file: {ex.Message}", StatusType.Error);
                return false;
            }
        }

        return true;
    }

    public async Task CloseTab(IEditorData tab, IEditorTabGroup tabGroup)
    {
        if (!tabGroup.Tabs.Contains(tab)) return;

        if (tab.IsModified && _editorService.IsLastTab(tab))
        {
            var result = await _dialogService.ShowMessageDialog("You have unsaved changes. Save file before closing?", StatusType.Warning, DialogButtons.Yes|DialogButtons.No|DialogButtons.Cancel);
            switch (result.Type)
            {
                case DialogResultType.Yes:
                    if (!await SaveEditorData(tab)) return;
                    break;
                case DialogResultType.No:
                    tab.File.IsModified = false;
                    break;
                case DialogResultType.Cancel:
                    return;
            }
        }

        await _editorService.CloseTab(tab, tabGroup);
    }

    public async Task CloseTabs(IEnumerable<IEditorData> tabs)
    {
        var tabArr = tabs as IEditorData[] ?? tabs.ToArray();

        var modifiedTabs = tabArr.Where(tab => tab.IsModified && _editorService.IsLastTab(tab)).ToList();
        if (modifiedTabs.Count > 0)
        {
            var result = await _dialogService.ShowMessageDialog("You have unsaved changes. Save files before closing?", StatusType.Warning, DialogButtons.Yes|DialogButtons.No|DialogButtons.Cancel);
            switch (result.Type)
            {
                case DialogResultType.Yes:
                    if (!await SaveEditorData(modifiedTabs)) return;
                    break;
                case DialogResultType.No:
                    foreach (var tab in modifiedTabs) tab.File.IsModified = false;
                    break;
                case DialogResultType.Cancel:
                    return;
            }
        }

        await _editorService.CloseTabs(tabArr);
    }

    public async Task CloseTabs(IEnumerable<IEditorData> tabs, IEditorTabGroup tabGroup)
    {
        var foundTabs = new List<IEditorData>();
        var modifiedTabs = new List<IEditorData>();
        foreach (var tab in tabs)
        {
            if (!tabGroup.Tabs.Contains(tab)) continue;
            foundTabs.Add(tab);
            if (tab.IsModified && _editorService.IsLastTab(tab)) modifiedTabs.Add(tab);
        }
        if (foundTabs.Count == 0) return;

        if (modifiedTabs.Count > 0)
        {
            var result = await _dialogService.ShowMessageDialog("You have unsaved changes. Save files before closing?", StatusType.Warning, DialogButtons.Yes|DialogButtons.No|DialogButtons.Cancel);
            switch (result.Type)
            {
                case DialogResultType.Yes:
                    if (!await SaveEditorData(modifiedTabs)) return;
                    break;
                case DialogResultType.No:
                    foreach (var tab in modifiedTabs) tab.File.IsModified = false;
                    break;
                case DialogResultType.Cancel:
                    return;
            }
        }

        await _editorService.CloseTabs(foundTabs, tabGroup);
    }

    public Task CloseTabGroup(IEditorTabGroup tabGroup) => CloseTabs(tabGroup.Tabs, tabGroup);

    public Task CloseFile(FileData file) => CloseTabs(_editorService.GetAllUniqueTabs(file));

    public async Task CloseAllFiles()
    {
        var tabs = _editorService.GetAllUniqueTabs().ToArray();

        if (tabs.Any(file => file.IsModified))
        {
            var result = await _dialogService.ShowMessageDialog("You have unsaved changes. Save files before closing?", StatusType.Warning, DialogButtons.Yes|DialogButtons.No|DialogButtons.Cancel);
            switch (result.Type)
            {
                case DialogResultType.Yes:
                    if (!await SaveEditorData(tabs)) return;
                    break;
                case DialogResultType.Cancel:
                    return;
            }
        }

        await _editorService.CloseAllFiles();
    }

    public async Task UnloadFile(FileData file)
    {
        if (file.IsVirtual) return;

        if (file.IsModified)
        {
            var result = await _dialogService.ShowMessageDialog($"Do you want to save the changes you made to \"{file.Name}\"? Your changes will be lost if you don't save them.", StatusType.Warning, DialogButtons.Yes|DialogButtons.No|DialogButtons.Cancel);
            switch (result.Type)
            {
                case DialogResultType.Yes:
                    if (!await SaveFile(file)) return;
                    break;
                case DialogResultType.No:
                    file.IsModified = false;
                    break;
                case DialogResultType.Cancel:
                    return;
            }
        }

        await _editorService.CloseFile(file);
        if (file is ContainerFileData containerFile) await _editorService.CloseFiles(containerFile.FindAll());

        var parent = file.Parent;
        file.Unload();

        while (parent is not null && parent.Children.Count == 0)
        {
            var nextParent = parent.Parent;
            parent.Unload();
            parent = nextParent;
        }

        _fileBrowserService.Remove(file);
        _fileBrowserService.Refresh();
    }

    public async Task UnloadAllFiles()
    {
        var files = _fileBrowserService.Files.ToArray();
        var anyModified = false;
        foreach (var file in files)
        {
            if (!file.IsModified) continue;
            anyModified = true;
            break;
        }

        if (anyModified)
        {
            var result = await _dialogService.ShowMessageDialog("You have unsaved changes. Save files before unloading?", StatusType.Warning, DialogButtons.Yes|DialogButtons.No|DialogButtons.Cancel);
            switch (result.Type)
            {
                case DialogResultType.Yes:
                    if (!await SaveAllFiles()) return;
                    break;
                case DialogResultType.Cancel:
                    return;
            }
        }

        await _editorService.CloseAllFiles();
        _fileBrowserService.Clear();
        foreach (var file in files) file.Unload();
    }

    public async Task DeleteFile(FileData file)
    {
        if (file.IsVirtual) return;

        var result = await _dialogService.ShowMessageDialog($"Are you sure you want to delete \"{file.Name}\"? This action cannot be undone.", StatusType.Warning, DialogButtons.Yes|DialogButtons.No);
        if (result.Type is not DialogResultType.Yes) return;

        try
        {
            await file.Delete();
        }
        catch (Exception ex)
        {
            await _dialogService.ShowMessageDialog($"Failed to delete \"{file.Name}\": {ex.Message}", StatusType.Error);
            return;
        }

        await _editorService.CloseFile(file);
    }

    public void ToggleTagGlossary() => GlossaryToggled?.Invoke(this, EventArgs.Empty);

    public async Task<bool> OnWindowClose()
    {
        if (_fileBrowserService.Files.Any(file => file.IsModified) || _gameConfigService.Configs.Any(file => file.IsModified))
        {
            var result = await _dialogService.ShowMessageDialog("You have unsaved changes. Your changes will be lost if you don't save them. Save changes?", StatusType.Warning, DialogButtons.Yes|DialogButtons.No|DialogButtons.Cancel);
            switch (result.Type)
            {
                case DialogResultType.Yes:
                    if (!await SaveAllFiles()) return false;
                    break;
                case DialogResultType.Cancel:
                    return false;
            }
        }

        var reference = _dialogService.ShowDialogAsync<ClosingFilesDialog>(preventClose: true);
        try
        {
            var files = _fileBrowserService.Files.ToArray();
            await _editorService.CloseAllFiles();
            _fileBrowserService.Clear();
            foreach (var file in files) file.Unload();

            return true;
        }
        catch (Exception ex)
        {
            reference.Close();
            await _dialogService.ShowMessageDialog($"Failed to close files: {ex.Message}", StatusType.Error);
            return false;
        }
    }
    #endregion

    #region private methods
    // ReSharper disable once AsyncVoidMethod
    private async void OnGameConfigChanged(object? sender, EventArgs args)
    {
        _currentGameConfig = _gameConfigService.CurrentFile;

        var exceptions = new List<Exception>();
        IEditorData? firstTab = null;
        foreach (var tab in _editorService.GetAllUniqueTabs())
        {
            try
            {
                await tab.Reload();
            }
            catch (Exception ex)
            {
                firstTab ??= tab;
                exceptions.Add(ex);
            }
        }

        if (exceptions.Count == 0) return;
        await _dialogService.ShowMessageDialog(exceptions.Count == 1 ?
                                         $"Failed to load \"{firstTab!.File.Name}\". Please verify that the Game Config aligns with the actual game data: {exceptions[0].Message}" :
                                         "Several files failed to load with the selected Game Config. Please verify that the Game Config aligns with the actual game data.", StatusType.Error);
    }

    // ReSharper disable once AsyncVoidMethod
    private async void OnFileBrowserSelectionChanged(object? sender, EventArgs args)
    {
        var file = (sender as IFileList)?.SelectedFile;
        if (file is null || file.IsLoading) return;

        if (file is ContainerFileData containerFile)
        {
            try
            {
                await containerFile.Init();
            }
            catch (Exception ex)
            {
                await _dialogService.ShowMessageDialog($"Failed to load \"{file.Name}\": {ex.Message}", StatusType.Error);
                return;
            }
        }

        await LoadEditorData(file, true);
    }
    #endregion

    #region IDisposable interface
    public void Dispose()
    {
        _fileBrowserService.SelectionChanged -= OnFileBrowserSelectionChanged;
        _gameConfigService.CurrentConfigChanged -= OnGameConfigChanged;
    }
    #endregion
}