﻿namespace MsbtEditor;

internal sealed class ScopeHandleService : IDisposable
{
    private readonly ScopeManagerService _scopeManagerService;
    private readonly IServiceProvider _serviceProvider;

    public ScopeHandleService(ScopeManagerService scopeManagerService, IServiceProvider serviceProvider)
    {
        _scopeManagerService = scopeManagerService;
        _serviceProvider = serviceProvider;

        scopeManagerService.RegisterScope(serviceProvider);
    }

    public void Dispose() => _scopeManagerService.UnregisterScope(_serviceProvider);
}