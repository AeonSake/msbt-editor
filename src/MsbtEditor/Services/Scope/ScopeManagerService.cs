﻿namespace MsbtEditor;

internal sealed class ScopeManagerService
{
    #region private members
    private readonly Dictionary<IServiceProvider, Action<IServiceProvider>?> _scopes = [];
    private readonly Queue<Action<IServiceProvider>> _queue = [];
    #endregion

    #region public properties
    public IEnumerable<IServiceProvider> Scopes => _scopes.Keys;
    #endregion

    #region public methods
    public void RegisterScope(IServiceProvider provider)
    {
        if (_queue.TryDequeue(out var handle))
        {
            _scopes.Add(provider, handle);
            handle.Invoke(provider);
        }
        else _scopes.Add(provider, null);
    }

    public void UnregisterScope(IServiceProvider provider)
    {
        if (!_scopes.Remove(provider, out var handle)) return;

        if (handle is not null) _queue.Enqueue(handle);
    }

    public void WaitForScope(Action<IServiceProvider> handle)
    {
        var resolved = false;
        foreach (var (provider, handle1) in _scopes)
        {
            if (handle1 is not null) continue;
            _scopes[provider] = handle;
            resolved = true;
        }

        if (!resolved) _queue.Enqueue(handle);
    }
    #endregion
}