﻿namespace MsbtEditor;

public interface IEditorTabGroup
{
    public IReadOnlyList<IEditorData> Tabs { get; }

    public IEditorData? SelectedTab { get; }

    public IEditorData? PreviewTab { get; }

    public event EventHandler? TabsChanged;

    public event EventHandler? SelectedTabChanged;

    public event EventHandler? PreviewTabChanged;

    public void Select(IEditorData tab);
}