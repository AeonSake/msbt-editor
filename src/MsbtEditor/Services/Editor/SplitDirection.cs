﻿namespace MsbtEditor;

public enum SplitDirection
{
    Full,
    Left,
    Right,
    Up,
    Down
}