﻿namespace MsbtEditor;

public interface IEditorTreeNode
{
    public IReadOnlyList<IEditorTreeNode> Children { get; }

    public bool Vertical { get; }

    public IEditorTabGroup? Tabs { get; }

    public event EventHandler? NodeChanged;
}