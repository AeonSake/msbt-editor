﻿using System.Diagnostics.CodeAnalysis;

namespace MsbtEditor;

internal sealed class EditorService : IDisposable
{
    #region private members
    private readonly IEnumerable<IEditorProvider> _editorProviders;
    private readonly Dictionary<Type, List<IEditorFactory>> _factoryCache = [];
    private readonly Dictionary<EditorTabGroup, EditorTreeNode> _tabGroups = [];
    private readonly Dictionary<string, HashSet<IEditorData>> _tabCache = [];
    private IEditorTabGroup _activeTabGroup;
    private IEditorData? _activeTab;
    #endregion

    #region constructor
    public EditorService(IEnumerable<IEditorProvider> editorProviders)
    {
        _editorProviders = editorProviders;

        var rootNode = new EditorTreeNode();
        var node = new EditorTreeNode {Parent = rootNode, Tabs = new EditorTabGroup()};
        rootNode.Children.Add(node);
        _tabGroups.Add(node.Tabs, node);

        RootNode = rootNode;
        _activeTabGroup = node.Tabs;
        _activeTabGroup.SelectedTabChanged += OnSelectedTabChanged;
    }
    #endregion

    #region public properties
    public IEditorTreeNode RootNode { get; }

    public IEnumerable<IEditorTabGroup> TabGroups => _tabGroups.Keys;

    public IEditorTabGroup ActiveTabGroup
    {
        get => _activeTabGroup;
        private set
        {
            if (_activeTabGroup == value) return;
            _activeTabGroup.SelectedTabChanged -= OnSelectedTabChanged;
            _activeTabGroup = value;
            _activeTab = _activeTabGroup.SelectedTab;
            _activeTabGroup.SelectedTabChanged += OnSelectedTabChanged;
            ActiveTabGroupChanged?.Invoke(this, EventArgs.Empty);
            ActiveTabChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    public IEditorData? ActiveTab
    {
        get => _activeTab;
        private set
        {
            if (_activeTab == value) return;
            _activeTab = value;
            ActiveTabChanged?.Invoke(this, EventArgs.Empty);
        }
    }
    #endregion

    #region public events
    public event EventHandler? ActiveTabGroupChanged;

    public event EventHandler? ActiveTabChanged;
    #endregion

    #region public methods
    public IReadOnlyCollection<IEditorFactory> GetFactories(FileData file)
    {
        var fileType = file.GetType();
        if (_factoryCache.TryGetValue(fileType, out var factories)) return factories;

        factories = [];
        foreach (var editorProvider in _editorProviders)
        {
            factories.AddRange(editorProvider.GetFactories(file));
        }
        _factoryCache.Add(fileType, factories);

        return factories;
    }

    public IEditorFactory? GetDefaultFactory(FileData file)
    {
        //maybe this could/should be configurable in settings?
        return GetFactories(file).FirstOrDefault();
    }

    public bool HasFactory(FileData file) => GetDefaultFactory(file) is not null;

    public void SetActiveTabGroup(IEditorTabGroup tabGroup)
    {
        if (!VerifyTabGroup(tabGroup, out _)) return;
        ActiveTabGroup = tabGroup;
    }

    public Task LoadTab(IEditorData tab, IEditorTabGroup tabGroup, bool asPreview = false) => InternalLoadTab(tab, ActiveTabGroup, -1, SplitDirection.Full, asPreview);

    public Task LoadTab(IEditorData tab, IEditorTabGroup tabGroup, int index) => InternalLoadTab(tab, tabGroup, index, SplitDirection.Full, false);

    public Task LoadTab(IEditorData tab, IEditorTabGroup tabGroup, SplitDirection direction) => InternalLoadTab(tab, tabGroup, -1, direction, false);

    public async Task ReplaceTab(IEditorData oldTab, IEditorData newTab, IEditorTabGroup tabGroup)
    {
        if (!VerifyTabGroup(tabGroup, out var internalTabGroup)) return;

        var disposeTab = internalTabGroup.Replace(oldTab, newTab) ? oldTab : internalTabGroup.AddOrSelect(newTab);

        AddTabToCache(newTab);
        if (disposeTab is not null)
        {
            RemoveTabFromCache(disposeTab);
            await disposeTab.DisposeAsync();
        }
    }

    public async Task CloseTab(IEditorData tab)
    {
        if (!_tabCache.TryGetValue(tab.Id, out var tabList)) return;
        if (!tabList.Remove(tab)) return;
        foreach (var tabGroup in _tabGroups.Keys)
        {
            if (tabGroup.Remove(tab)) break;
        }

        if (tabList.Count == 0) _tabCache.Remove(tab.Id);
        await tab.DisposeAsync();
        TrimTree();
    }

    public async Task CloseTab(IEditorData tab, IEditorTabGroup tabGroup)
    {
        if (!VerifyTabGroup(tabGroup, out var internalTabGroup)) return;
        if (!_tabCache.TryGetValue(tab.Id, out var tabList)) return;
        if (!internalTabGroup.Remove(tab) || !tabList.Remove(tab)) return;

        if (tabList.Count == 0) _tabCache.Remove(tab.Id);
        await tab.DisposeAsync();
        TrimTree();
    }

    public async Task CloseTabs(IEnumerable<IEditorData> tabs)
    {
        var anyRemoved = false;
        var disposeList = new List<IEditorData>();
        foreach (var tab in tabs)
        {
            if (!_tabCache.TryGetValue(tab.Id, out var tabList)) continue;
            if (!tabList.Remove(tab)) continue;
            foreach (var tabGroup in _tabGroups.Keys)
            {
                if (tabGroup.Remove(tab)) break;
            }

            if (tabList.Count == 0) _tabCache.Remove(tab.Id);
            disposeList.Add(tab);
            anyRemoved = true;
        }
        if (!anyRemoved) return;

        await Parallel.ForEachAsync(disposeList, (tab, _) => tab.DisposeAsync());
        TrimTree();
    }

    public async Task CloseTabs(IEnumerable<IEditorData> tabs, IEditorTabGroup tabGroup)
    {
        if (!VerifyTabGroup(tabGroup, out var internalTabGroup)) return;

        var anyRemoved = false;
        var disposeList = new List<IEditorData>();
        foreach (var tab in tabs)
        {
            if (!_tabCache.TryGetValue(tab.Id, out var tabList)) continue;
            if (!internalTabGroup.Remove(tab) || !tabList.Remove(tab)) continue;

            if (tabList.Count == 0) _tabCache.Remove(tab.Id);
            disposeList.Add(tab);
            anyRemoved = true;
        }
        if (!anyRemoved) return;

        await Parallel.ForEachAsync(disposeList, (tab, _) => tab.DisposeAsync());
        TrimTree();
    }

    public Task CloseTabGroup(IEditorTabGroup tabGroup) => CloseTabs(tabGroup.Tabs, tabGroup);

    public async Task MoveTab(IEditorData tab, IEditorTabGroup sourceTabGroup, IEditorTabGroup targetTabGroup, int index = -1)
    {
        if (sourceTabGroup == targetTabGroup && (index < 0 || sourceTabGroup.Tabs.Count == 1)) return;
        if (!VerifyTabGroup(sourceTabGroup, out var internalSourceTabGroup) || !VerifyTabGroup(targetTabGroup, out var internalTargetTabGroup)) return;

        //fix index if removed from same group
        if (internalSourceTabGroup == internalTargetTabGroup)
        {
            var currentIndex = -1;
            for (var i = 0; i < internalSourceTabGroup.Tabs.Count; ++i)
            {
                if (internalSourceTabGroup.Tabs[i] != tab) continue;
                currentIndex = i;
                break;
            }
            index = currentIndex < index ? index - 1 : index;
        }

        if (!internalSourceTabGroup.Remove(tab)) return;

        var oldTab = index < 0 ? internalTargetTabGroup.AddOrSelect(tab) : internalTargetTabGroup.Insert(tab, index);
        if (oldTab is not null)
        {
            RemoveTabFromCache(oldTab);
            await oldTab.DisposeAsync();
        }

        ActiveTabGroup = targetTabGroup;
        TrimTree();
    }

    public async Task MoveTab(IEditorData tab, IEditorTabGroup sourceTabGroup, IEditorTabGroup targetTabGroup, SplitDirection direction)
    {
        if (sourceTabGroup == targetTabGroup && sourceTabGroup.Tabs.Count == 1) return;
        if (direction is SplitDirection.Full)
        {
            await MoveTab(tab, sourceTabGroup, targetTabGroup);
            return;
        }

        if (!VerifyTabGroup(sourceTabGroup, out var internalSourceTabGroup) || !VerifyTabGroup(targetTabGroup, out var internalTargetTabGroup)) return;
        if (!internalSourceTabGroup.Remove(tab)) return;

        var treeNode = _tabGroups[internalTargetTabGroup];
        targetTabGroup = CreateNode(treeNode, [tab], direction, out var oldTabs).Tabs!;
        foreach (var oldTab in oldTabs)
        {
            RemoveTabFromCache(oldTab);
            await oldTab.DisposeAsync();
        }

        ActiveTabGroup = targetTabGroup;
        TrimTree();
    }

    public async Task DuplicateTab(IEditorData tab, IEditorTabGroup tabGroup, SplitDirection direction)
    {
        if (direction is SplitDirection.Full) return;
        if (!VerifyTabGroup(tabGroup, out var internalTabGroup) || !tabGroup.Tabs.Contains(tab)) return;

        var treeNode = _tabGroups[internalTabGroup];
        var newTab = tab.Clone();
        AddTabToCache(newTab);

        tabGroup = CreateNode(treeNode, [newTab], direction, out var oldTabs).Tabs!;
        foreach (var oldTab in oldTabs)
        {
            RemoveTabFromCache(oldTab);
            await oldTab.DisposeAsync();
        }

        ActiveTabGroup = tabGroup;
        treeNode.Parent!.NotifyChanges();
    }

    public async Task DuplicateTabGroup(IEditorTabGroup tabGroup, SplitDirection direction)
    {
        if (direction is SplitDirection.Full) return;
        if (!VerifyTabGroup(tabGroup, out var internalTabGroup)) return;

        var treeNode = _tabGroups[internalTabGroup];
        var tabs = new IEditorData[tabGroup.Tabs.Count];
        var selectedTabIndex = -1;
        for (var i = 0; i < tabGroup.Tabs.Count; ++i)
        {
            if (tabGroup.Tabs[i] == tabGroup.SelectedTab) selectedTabIndex = i;

            var tab = tabGroup.Tabs[i].Clone();
            tabs[i] = tab;
            AddTabToCache(tab);
        }

        tabGroup = CreateNode(treeNode, tabs, direction, out var oldTabs).Tabs!;
        if (selectedTabIndex > -1) tabGroup.Select(tabGroup.Tabs[selectedTabIndex]);
        foreach (var oldTab in oldTabs)
        {
            RemoveTabFromCache(oldTab);
            await oldTab.DisposeAsync();
        }

        ActiveTabGroup = tabGroup;
        treeNode.Parent!.NotifyChanges();
    }

    public bool IsLastTab(IEditorData tab) => _tabCache.TryGetValue(tab.Id, out var tabList) && tabList.Count == 1;

    public IReadOnlyCollection<IEditorData> GetAllUniqueTabs(FileData file)
    {
        var tabs = new List<IEditorData>();
        foreach (var tabList in _tabCache.Values)
        {
            if (tabList.Count == 0) continue;
            var tab = tabList.First();
            if (tab.File == file) tabs.Add(tab);
        }
        return tabs;
    }

    public IReadOnlyCollection<IEditorData> GetAllUniqueTabs()
    {
        var tabs = new List<IEditorData>();
        foreach (var tabList in _tabCache.Values)
        {
            if (tabList.Count == 0) continue;
            tabs.Add(tabList.First());
        }
        return tabs;
    }

    public Task CloseFile(FileData file)
    {
        var tabs = new List<IEditorData>();
        foreach (var editorList in _tabCache.Values)
        {
            if (editorList.Count == 0 || editorList.First().File != file) continue;
            tabs.AddRange(editorList);
        }

        return CloseTabs(tabs);
    }

    public Task CloseFiles(IEnumerable<FileData> files)
    {
        var tabs = new List<IEditorData>();
        foreach (var file in files)
        {
            foreach (var editorList in _tabCache.Values)
            {
                if (editorList.Count == 0 || editorList.First().File != file) continue;
                tabs.AddRange(editorList);
            }
        }

        return CloseTabs(tabs);
    }

    public async Task CloseAllFiles()
    {
        var tabs = new List<IEditorData>();
        foreach (var tabGroup in _tabGroups.Keys)
        {
            tabs.AddRange(tabGroup.Tabs);
            tabGroup.Dispose();
        }
        _tabCache.Clear();

        //reset tree
        var rootNode = (EditorTreeNode) RootNode;
        var node = new EditorTreeNode {Parent = rootNode, Tabs = new EditorTabGroup()};
        rootNode.Vertical = false;
        rootNode.Children.Clear();
        rootNode.Children.Add(node);
        _tabGroups.Clear();
        _tabGroups.Add(node.Tabs, node);
        ActiveTabGroup = node.Tabs;

        //dispose models
        await Parallel.ForEachAsync(tabs, (tab, _) => tab.DisposeAsync());
    }
    #endregion

    #region private methods
    private void OnSelectedTabChanged(object? sender, EventArgs args) => ActiveTab = ActiveTabGroup.SelectedTab;

    private bool VerifyTabGroup(IEditorTabGroup tabGroup, [MaybeNullWhen(false)] out EditorTabGroup foundTabGroup)
    {
        foundTabGroup = null;
        if (tabGroup is not EditorTabGroup editorTabGroup || !_tabGroups.ContainsKey(editorTabGroup)) return false;
        foundTabGroup = editorTabGroup;
        return true;
    }

    private async Task InternalLoadTab(IEditorData tab, IEditorTabGroup tabGroup, int index, SplitDirection direction, bool asPreview)
    {
        if (!VerifyTabGroup(tabGroup, out var editorTabGroup)) return;

        IEditorData? oldTab;
        if (direction is not SplitDirection.Full)
        {
            var treeNode = _tabGroups[editorTabGroup];
            tabGroup = CreateNode(treeNode, [tab], direction, out var oldTabs).Tabs!;
            oldTab = oldTabs.Count > 0 ? oldTabs[0] : null;
        }
        else if (index < 0 || asPreview)
        {
            oldTab = editorTabGroup.AddOrSelect(tab, asPreview);
        }
        else
        {
            oldTab = editorTabGroup.Insert(tab, index);
        }

        AddTabToCache(tab);
        if (oldTab is not null)
        {
            RemoveTabFromCache(oldTab);
            await oldTab.DisposeAsync();
        }

        ActiveTabGroup = tabGroup;
        _tabGroups[editorTabGroup].Parent!.NotifyChanges();
    }

    private void AddTabToCache(IEditorData tab)
    {
        if (_tabCache.TryGetValue(tab.Id, out var tabList)) tabList.Add(tab);
        else _tabCache.Add(tab.Id, [tab]);
    }

    private void RemoveTabFromCache(IEditorData tab)
    {
        if (!_tabCache.TryGetValue(tab.Id, out var tabList)) return;
        if (!tabList.Remove(tab) || tabList.Count > 0) return;
        _tabCache.Remove(tab.Id);
    }

    private void TrimTree()
    {
        var rootNode = (EditorTreeNode) RootNode;
        var updateActiveTabs = false;
        var anyChanges = false;

        //remove empty branches
        foreach (var (tabGroup, node) in _tabGroups.ToArray())
        {
            if (tabGroup.Tabs.Count > 0) continue;

            _tabGroups.Remove(tabGroup);
            tabGroup.Dispose();
            if (ActiveTabGroup == tabGroup) updateActiveTabs = true;
            anyChanges = true;

            var currentNode = node;
            do
            {
                var parent = currentNode.Parent;
                parent?.Children.Remove(currentNode);
                currentNode = parent;
            }
            while (currentNode is not null && currentNode.Children.Count == 0);
        }
        if (!anyChanges)
        {
            rootNode.NotifyChanges();
            return;
        }

        //collapse single branches
        foreach (var node in _tabGroups.Values)
        {
            var lastNode = node;
            var currentNode = node;
            while (currentNode.Parent is not null)
            {
                var parent = currentNode.Parent;

                if (lastNode != currentNode)
                {
                    var index = parent.Children.IndexOf(currentNode);
                    parent.Children[index] = lastNode;
                    lastNode.Parent = parent;
                }

                if (parent.Children.Count > 1) lastNode = parent;
                currentNode = parent;
            }
        }

        //validate root node
        if (rootNode.Children.Count == 0)
        {
            var node = new EditorTreeNode {Parent = rootNode, Tabs = new EditorTabGroup()};
            rootNode.Children.Add(node);
            _tabGroups.Add(node.Tabs, node);
        }

        if (updateActiveTabs) ActiveTabGroup = _tabGroups.First().Key;
        rootNode.NotifyChanges();
    }

    private EditorTreeNode CreateNode(EditorTreeNode node, IEnumerable<IEditorData> tabs, SplitDirection direction, out IList<IEditorData> oldTabs)
    {
        if (direction is SplitDirection.Full)
        {
            oldTabs = node.Tabs?.AddRange(tabs) ?? [];
            return node;
        }

        var newNode = new EditorTreeNode {Tabs = new EditorTabGroup(tabs)};
        switch (direction, node.Parent!.Vertical)
        {
            case (SplitDirection.Up, true):
            case (SplitDirection.Left, false):
                AddSiblingNode(node, newNode, true);
                break;
            case (SplitDirection.Down, true):
            case (SplitDirection.Right, false):
                AddSiblingNode(node, newNode, false);
                break;
            case (SplitDirection.Up, false):
            case (SplitDirection.Left, true):
                SplitNode(node, newNode, !node.Parent!.Vertical, true);
                break;
            case (SplitDirection.Down, false):
            case (SplitDirection.Right, true):
                SplitNode(node, newNode, !node.Parent!.Vertical, false);
                break;
        }

        oldTabs = [];
        return newNode;
    }

    private void AddSiblingNode(EditorTreeNode node, EditorTreeNode newNode, bool before)
    {
        if (node.Parent is null || newNode.Tabs is null) return;
        var index = node.Parent.Children.IndexOf(node);
        if (index == -1) return;

        newNode.Parent = node.Parent;
        node.Parent.Children.Insert(index + (before ? 0 : 1), newNode);

        _tabGroups[newNode.Tabs] = newNode;
    }

    private void SplitNode(EditorTreeNode node, EditorTreeNode newNode, bool vertical, bool before)
    {
        if (node.Tabs is null || node.Children.Count > 0 || newNode.Tabs is null) return;

        newNode.Parent = node;
        var newNode2 = new EditorTreeNode {Parent = node, Tabs = node.Tabs};
        if (!before) (newNode2, newNode) = (newNode, newNode2);

        node.Vertical = vertical;
        node.Tabs = null;

        node.Children.Add(newNode);
        node.Children.Add(newNode2);

        _tabGroups[newNode.Tabs] = newNode;
        _tabGroups[newNode2.Tabs] = newNode2;
    }
    #endregion

    #region IDisposable interface
    public void Dispose()
    {
        _activeTabGroup.SelectedTabChanged -= OnSelectedTabChanged;
        _activeTab = null;

        var tabs = new List<IEditorData>();
        foreach (var tabGroup in _tabGroups.Keys)
        {
            tabs.AddRange(tabGroup.Tabs);
            tabGroup.Dispose();
        }

        Parallel.ForEach(tabs, tab =>
        {
            try
            {
                Task.Run(tab.DisposeAsync).Wait();
            }
            catch
            {
                //ignored
            }
        });
    }
    #endregion

    #region helper classes
    private class EditorTreeNode : IEditorTreeNode
    {
        #region public properties
        public EditorTreeNode? Parent { get; set; }

        public List<EditorTreeNode> Children { get; } = [];

        IReadOnlyList<IEditorTreeNode> IEditorTreeNode.Children => Children;

        public bool Vertical { get; set; }

        public EditorTabGroup? Tabs { get; set; }

        IEditorTabGroup? IEditorTreeNode.Tabs => Tabs;
        #endregion

        #region public events
        public event EventHandler? NodeChanged;
        #endregion

        #region public methods
        public void NotifyChanges() => NodeChanged?.Invoke(this, EventArgs.Empty);
        #endregion
    }

    private class EditorTabGroup : IEditorTabGroup, IDisposable
    {
        #region private members
        private readonly List<IEditorData> _tabs;
        #endregion

        #region constructors
        public EditorTabGroup() => _tabs = [];

        public EditorTabGroup(IEnumerable<IEditorData> tabs)
        {
            _tabs = [..tabs];
            SelectedTab = _tabs.Count > 0 ? _tabs[0] : null;
        }
        #endregion

        #region public properties
        public IReadOnlyList<IEditorData> Tabs => _tabs;

        public IEditorData? SelectedTab { get; private set; }

        public IEditorData? PreviewTab { get; private set; }
        #endregion

        #region public events
        public event EventHandler? TabsChanged;

        public event EventHandler? SelectedTabChanged;

        public event EventHandler? PreviewTabChanged;
        #endregion

        #region public methods
        public IEditorData? AddOrSelect(IEditorData tab, bool asPreview = false)
        {
            var result = FindId(tab);
            var tabExists = result is not null;
            var isSelected = tabExists && SelectedTab == result;
            var isPreview = tabExists && PreviewTab == result;
            var previewChanged = false;

            //only add tab if it doesn't exist yet
            if (tabExists)
            {
                SelectedTab = result;
                result = tab;
            }
            else
            {
                if (PreviewTab is not null && !asPreview) _tabs.Insert(_tabs.Count - 1, tab);
                else _tabs.Add(tab);
                SelectedTab = tab;
            }

            //update preview
            if (PreviewTab is not null && (isPreview || asPreview && !tabExists))
            {
                if (!isPreview)
                {
                    _tabs.Remove(PreviewTab);
                    result = PreviewTab;
                }

                PreviewTab.Modified -= OnPreviewTabModified;
                PreviewTab = null;
                previewChanged = true;
            }
            if (asPreview && !tabExists)
            {
                //any change will make tab sticky
                tab.Modified += OnPreviewTabModified;
                PreviewTab = tab;
                previewChanged = true;
            }

            if (!tabExists) TabsChanged?.Invoke(this, EventArgs.Empty);
            if (!isSelected) SelectedTabChanged?.Invoke(this, EventArgs.Empty);
            if (previewChanged) PreviewTabChanged?.Invoke(this, EventArgs.Empty);

            return result;
        }

        public IList<IEditorData> AddRange(IEnumerable<IEditorData> tabs)
        {
            var result = new List<IEditorData>();
            var selectedChanged = false;
            var hasPreview = false;

            //only add tabs that don't exist yet
            var count = 0;
            foreach (var tab in tabs)
            {
                if (ContainsId(tab)) result.Add(tab);
                else if (PreviewTab is not null) _tabs.Insert(_tabs.Count - 1, tab);
                else _tabs.Add(tab);

                if (tab.Id == PreviewTab?.Id) hasPreview = true;
                ++count;
            }
            if (count == 0) return result;

            //update selected/preview
            if (SelectedTab is null)
            {
                SelectedTab = _tabs[0];
                selectedChanged = true;
            }
            if (hasPreview)
            {
                PreviewTab!.Modified -= OnPreviewTabModified;
                PreviewTab = null;
            }

            TabsChanged?.Invoke(this, EventArgs.Empty);
            if (selectedChanged) SelectedTabChanged?.Invoke(this, EventArgs.Empty);
            if (hasPreview) PreviewTabChanged?.Invoke(this, EventArgs.Empty);

            return result;
        }

        public IEditorData? Insert(IEditorData tab, int index)
        {
            if (index < 0 || index > _tabs.Count) return null;

            var result = FindId(tab);
            var tabExists = result is not null;
            var isSelected = tabExists && SelectedTab == result;
            var isPreview = tabExists && PreviewTab == result;

            //insert before preview, if inserting at last position
            if (index == _tabs.Count && (tabExists || PreviewTab is not null)) --index;
            _tabs.Insert(index, tab);
            if (tabExists) _tabs.Remove(result!);

            SelectedTab = tab;

            //inserting always makes tab sticky
            if (isPreview)
            {
                PreviewTab!.Modified -= OnPreviewTabModified;
                PreviewTab = null;
            }

            TabsChanged?.Invoke(this, EventArgs.Empty);
            if (!isSelected) SelectedTabChanged?.Invoke(this, EventArgs.Empty);
            if (isPreview) PreviewTabChanged?.Invoke(this, EventArgs.Empty);

            return result == tab ? null : result;
        }

        public bool Replace(IEditorData oldTab, IEditorData newTab)
        {
            var index = IndexOf(oldTab);
            if (index < 0) return false;

            var isSelected = SelectedTab == oldTab;
            var isPreview = PreviewTab == oldTab;

            _tabs.Insert(index, newTab);
            _tabs.Remove(oldTab);

            if (isSelected) SelectedTab = newTab;
            if (isPreview)
            {
                PreviewTab!.Modified -= OnPreviewTabModified;
                PreviewTab = null;
            }

            TabsChanged?.Invoke(this, EventArgs.Empty);
            if (isSelected) SelectedTabChanged?.Invoke(this, EventArgs.Empty);
            if (isPreview) PreviewTabChanged?.Invoke(this, EventArgs.Empty);

            return true;
        }

        public void Select(IEditorData tab)
        {
            if (!_tabs.Contains(tab)) return;

            var isSelected = SelectedTab == tab;
            var isPreview = PreviewTab == tab;

            SelectedTab = tab;

            //selecting an already selected tab makes it sticky
            if (isSelected && isPreview)
            {
                PreviewTab!.Modified -= OnPreviewTabModified;
                PreviewTab = null;
            }

            if (!isSelected) SelectedTabChanged?.Invoke(this, EventArgs.Empty);
            if (isSelected && isPreview) PreviewTabChanged?.Invoke(this, EventArgs.Empty);
        }

        public bool Remove(IEditorData tab)
        {
            var index = _tabs.IndexOf(tab);
            if (index < 0) return false;

            _tabs.RemoveAt(index);

            var isSelected = SelectedTab == tab;
            var isPreview = PreviewTab == tab;

            //select first tab if selected tab was removed
            if (isSelected) SelectedTab = _tabs.Count > 0 ? _tabs[index > 0 ? index - 1 : 0] : null;
            if (isPreview)
            {
                PreviewTab!.Modified -= OnPreviewTabModified;
                PreviewTab = null;
            }

            TabsChanged?.Invoke(this, EventArgs.Empty);
            if (isSelected) SelectedTabChanged?.Invoke(this, EventArgs.Empty);
            if (isPreview) PreviewTabChanged?.Invoke(this, EventArgs.Empty);

            return true;
        }

        public IList<IEditorData> RemoveRange(IEnumerable<IEditorData> tabs)
        {
            var result = new List<IEditorData>();
            var hasSelected = false;
            var hasPreview = false;

            foreach (var tab in tabs)
            {
                if (!_tabs.Remove(tab)) continue;
                if (tab == SelectedTab) hasSelected = true;
                if (tab == PreviewTab) hasPreview = true;
                result.Add(tab);
            }
            if (result.Count == 0) return result;

            //update selected/preview
            if (hasSelected) SelectedTab = _tabs.Count > 0 ? _tabs[0] : null;
            if (hasPreview)
            {
                PreviewTab!.Modified -= OnPreviewTabModified;
                PreviewTab = null;
            }

            TabsChanged?.Invoke(this, EventArgs.Empty);
            if (hasSelected) SelectedTabChanged?.Invoke(this, EventArgs.Empty);
            if (hasPreview) PreviewTabChanged?.Invoke(this, EventArgs.Empty);

            return result;
        }
        #endregion

        #region private methods
        private bool ContainsId(IEditorData tab) => _tabs.Any(t => t.Id == tab.Id);

        private IEditorData? FindId(IEditorData tab) => _tabs.Find(t => t.Id == tab.Id);

        private int IndexOf(IEditorData tab) => _tabs.IndexOf(tab);

        private void OnPreviewTabModified(object? sender, EventArgs args)
        {
            if (PreviewTab is null) return;

            PreviewTab.Modified -= OnPreviewTabModified;
            PreviewTab = null;
            PreviewTabChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        #region IDisposable interface
        public void Dispose()
        {
            _tabs.Clear();
            SelectedTab = null;
            if (PreviewTab is not null) PreviewTab.Modified -= OnPreviewTabModified;
            PreviewTab = null;
        }
        #endregion
    }
    #endregion
}