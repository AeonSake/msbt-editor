﻿namespace MsbtEditor;

internal sealed class EditorContextActionProvider
{
    #region private members
    private readonly TypeDictionary<Func<IEditorData, ContextAction[]>> _actionCache = new();
    #endregion

    #region public methods
    public void Register<T>(Func<T, ContextAction[]> actionBuilder) where T : IEditorData => _actionCache.Add<T>(file => actionBuilder.Invoke((T) file));

    public void Unregister<T>() where T : IEditorData => _actionCache.Remove<T>();

    public bool HasActions<T>(T data) where T : IEditorData => _actionCache.Contains(data.GetType());

    public ContextAction[] GetActions<T>(T data) where T : IEditorData => _actionCache.TryGet(data.GetType(), out var actionBuilder) ? actionBuilder.Invoke(data) : [];
    #endregion
}