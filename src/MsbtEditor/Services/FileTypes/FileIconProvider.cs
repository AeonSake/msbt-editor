﻿namespace MsbtEditor;

internal sealed class FileIconProvider
{
    #region private members
    private readonly TypeDictionary<(IconValue, string)> _iconCache = new();
    private static readonly (IconValue, string) DefaultIcon = (GlobalIcons.UnknownFile, "#c5c5c5");
    #endregion

    #region public methods
    public void Register<T>(IconValue icon, string color) where T : FileData => _iconCache.Add<T>((icon, color));

    public void Unregister<T>() where T : FileData => _iconCache.Remove<T>();

    public (IconValue, string) GetIcon<T>(T file) where T : FileData => _iconCache.GetOrDefault(file.GetType(), DefaultIcon);

    public static (IconValue, string) GetDefaultIcon() => DefaultIcon;
    #endregion
}