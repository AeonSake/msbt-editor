﻿namespace MsbtEditor;

internal sealed class FileDataFactory
{
    #region private members
    private readonly IEnumerable<IFileDataProvider> _fileProviders;
    private readonly IEnumerable<ICompressionProvider> _compressionProviders;
    #endregion

    #region constructor
    public FileDataFactory(IEnumerable<IFileDataProvider> fileProviders, IEnumerable<ICompressionProvider> compressionProviders, FileIconProvider fileIconProvider, FileContextActionProvider fileContextActionProvider, DialogService dialogService)
    {
        _fileProviders = fileProviders;
        _compressionProviders = compressionProviders;

        fileIconProvider.Register<FolderFileData>(GlobalIcons.Folder, "#bf9558");
        fileIconProvider.Register<ErrorFileData>(GlobalIcons.ErrorFile, "#c90e0e");
        fileIconProvider.Register<UnknownFileData>(GlobalIcons.UnknownFile, "#c5c5c5");

        fileContextActionProvider.Register<ErrorFileData>(file =>
        [
            new ContextAction
            {
                Name = "Show Error",
                Icon = GlobalIcons.Error,
                OnClick = () =>
                {
                    dialogService.ShowMessageDialog(file.Exception.Message, StatusType.Error);
                    return Task.CompletedTask;
                }
            }
        ]);
    }
    #endregion

    #region public methods
    public FileData Create(StorageProvider provider, ContainerFileData? parent = null)
    {
        //check if file is a folder
        if (Directory.Exists(provider.FilePath))
        {
            return new FolderFileData(provider.FilePath, this, provider.IsVirtual, parent);
        }

        var stream = provider.GetReadStream();
        FileData? fileData = null;
        try
        {
            //check compression types
            bool isCompressed;
            do
            {
                isCompressed = false;
                foreach (var compressionProvider in _compressionProviders)
                {
                    if (!compressionProvider.CanDecompress(stream)) continue;
                    provider = compressionProvider.GetWrapper(provider);
                    stream.Close();
                    stream = provider.GetReadStream();
                    isCompressed = true;
                }
            }
            while (isCompressed);

            //check file types
            foreach (var fileProvider in _fileProviders)
            {
                if (!fileProvider.CanParse(stream)) continue;
                fileData = fileProvider.Parse(provider, this, parent);
            }

            //fallback
            fileData ??= new UnknownFileData(provider, parent);
        }
        catch (Exception ex)
        {
            fileData = new ErrorFileData(provider, ex, parent);
        }

        stream.Close();
        return fileData;
    }
    #endregion
}