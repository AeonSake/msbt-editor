﻿namespace MsbtEditor;

internal sealed class FileContextActionProvider
{
    #region private members
    private readonly TypeDictionary<Func<FileData, ContextAction[]>> _actionCache = new();
    #endregion

    #region public methods
    public void Register<T>(Func<T, ContextAction[]> actionBuilder) where T : FileData => _actionCache.Add<T>(file => actionBuilder.Invoke((T) file));

    public void Unregister<T>() where T : FileData => _actionCache.Remove<T>();

    public bool HasActions<T>(T file) where T : FileData => _actionCache.Contains(file.GetType());

    public ContextAction[] GetActions<T>(T file) where T : FileData => _actionCache.TryGet(file.GetType(), out var actionBuilder) ? actionBuilder.Invoke(file) : [];
    #endregion
}