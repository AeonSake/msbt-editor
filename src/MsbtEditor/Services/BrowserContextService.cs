﻿using Microsoft.Web.WebView2.WinForms;

namespace MsbtEditor;

internal sealed class BrowserContextService : IDisposable
{
    #region private members
    private static readonly double[] ZoomLevels = [0.25, 0.33, 0.5, 0.66, 0.75, 0.8, 0.9, 1, 1.1, 1.25, 1.5, 1.75, 2, 2.5, 3, 4, 5];
    private const int DefaultZoomIndex = 7;
    private int _currentZoomIndex = DefaultZoomIndex;
    private WebView2? _context;
    #endregion

    #region public properties
    public double CurrentZoom => ZoomLevels[_currentZoomIndex];

    public bool CanZoomIn => _currentZoomIndex + 1 < ZoomLevels.Length;

    public bool CanZoomOut => _currentZoomIndex > 0;
    #endregion

    #region public events
    public event EventHandler? ZoomChanged;
    #endregion

    #region public methods
    public void SetContext(WebView2 context)
    {
        if (_context is not null) _context.ZoomFactorChanged -= OnContextZoomChanged;
        _context = context;
        _context.ZoomFactor = ZoomLevels[_currentZoomIndex];
        _context.ZoomFactorChanged += OnContextZoomChanged;
    }

    public void ZoomIn()
    {
        if (!CanZoomIn) return;

        ++_currentZoomIndex;
        if (_context is not null) _context.ZoomFactor = ZoomLevels[_currentZoomIndex];
        ZoomChanged?.Invoke(this, EventArgs.Empty);
    }

    public void ZoomOut()
    {
        if (!CanZoomOut) return;

        --_currentZoomIndex;
        if (_context is not null) _context.ZoomFactor = ZoomLevels[_currentZoomIndex];
        ZoomChanged?.Invoke(this, EventArgs.Empty);
    }

    public void ResetZoom()
    {
        _currentZoomIndex = DefaultZoomIndex;
        if (_context is not null) _context.ZoomFactor = ZoomLevels[_currentZoomIndex];
        ZoomChanged?.Invoke(this, EventArgs.Empty);
    }

    public void OpenDevTools() => _context?.CoreWebView2.OpenDevToolsWindow();

    public void OpenTaskManager() => _context?.CoreWebView2.OpenTaskManagerWindow();
    #endregion

    #region private methods
    private void OnContextZoomChanged(object? sender, EventArgs args)
    {
        if (_context is null) return;

        //find nearest current zoom level
        for (var i = 0; i < ZoomLevels.Length; ++i)
        {
            if (Math.Abs(_context.ZoomFactor - ZoomLevels[i]) > 0.0001) continue;

            _currentZoomIndex = i;
            ZoomChanged?.Invoke(this, EventArgs.Empty);
            return;
        }
    }
    #endregion

    #region IDisposable interface
    public void Dispose()
    {
        if (_context is null) return;

        _context.ZoomFactorChanged -= OnContextZoomChanged;
    }
    #endregion
}