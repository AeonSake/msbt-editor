﻿using System.Collections;

namespace MsbtEditor;

public interface IFileList : IEnumerable<FileData>
{
    public int Count { get; }

    public IEnumerable<FileData> Files { get; }

    public FileData? SelectedFile { get; }

    public event EventHandler FilesChanged;

    public event EventHandler SelectionChanged;

    public bool Add(FileData file);

    public int AddRange(IEnumerable<FileData> files);

    public bool Remove(FileData file);

    public void Clear();

    public bool Contains(FileData file);

    public void Select(FileData file);

    public void Refresh();
}

internal sealed class FileBrowserService : IFileList
{
    #region private members
    private readonly Dictionary<string, FileData> _files = [];
    #endregion

    #region public properties
    public int Count => _files.Count;

    public IEnumerable<FileData> Files => _files.Values;

    public FileData? SelectedFile { get; private set; }
    #endregion

    #region public events
    public event EventHandler? FilesChanged;

    public event EventHandler? SelectionChanged;
    #endregion

    #region public methods
    public bool Add(FileData file)
    {
        if (!_files.TryAdd(file.InternalFilePath, file)) return false;

        FilesChanged?.Invoke(this, EventArgs.Empty);
        return true;
    }

    public int AddRange(IEnumerable<FileData> files)
    {
        var fileCount = 0;
        foreach (var file in files)
        {
            if (!_files.TryAdd(file.InternalFilePath, file)) continue;
            ++fileCount;
        }
        if (fileCount == 0) return 0;

        FilesChanged?.Invoke(this, EventArgs.Empty);

        return fileCount;
    }

    public bool Remove(FileData file)
    {
        var filesChanged = _files.Remove(file.InternalFilePath);

        var selectionChanged = false;
        if (SelectedFile is not null && (file == SelectedFile || file is ContainerFileData container && container.Contains(SelectedFile)))
        {
            SelectedFile = null;
            selectionChanged = true;
        }

        if (filesChanged) FilesChanged?.Invoke(this, EventArgs.Empty);
        if (selectionChanged) SelectionChanged?.Invoke(this, EventArgs.Empty);

        return filesChanged;
    }

    public void Clear()
    {
        _files.Clear();
        SelectedFile = null;

        FilesChanged?.Invoke(this, EventArgs.Empty);
        SelectionChanged?.Invoke(this, EventArgs.Empty);
    }

    public bool Contains(FileData file) => _files.ContainsKey(file.InternalFilePath);

    public void Select(FileData? file)
    {
        SelectedFile = file;
        SelectionChanged?.Invoke(this, EventArgs.Empty);
    }

    public void Refresh() => FilesChanged?.Invoke(this, EventArgs.Empty);
    #endregion

    #region IEnumerable interface
    public IEnumerator<FileData> GetEnumerator() => _files.Values.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    #endregion
}