﻿namespace MsbtEditor;

public sealed class AsyncLock : IDisposable
{
    private readonly SemaphoreSlim _lock = new(1, 1);

    public async Task LockAsync(Func<Task> work)
    {
        var isTaken = false;
        try
        {
            do
            {
                try
                {
                }
                finally
                {
                    isTaken = await _lock.WaitAsync(TimeSpan.FromSeconds(1));
                }
            }
            while (!isTaken);
            await work();
        }
        finally
        {
            if (isTaken) _lock.Release();
        }
    }

    public async Task<T> LockAsync<T>(Func<Task<T>> work)
    {
        var isTaken = false;
        try
        {
            do
            {
                try
                {
                }
                finally
                {
                    isTaken = await _lock.WaitAsync(TimeSpan.FromSeconds(1));
                }
            }
            while (!isTaken);
            return await work();
        }
        finally
        {
            if (isTaken) _lock.Release();
        }
    }

    public void Dispose() => _lock.Dispose();
}