﻿namespace MsbtEditor;

public sealed class ContextAction
{
    public required string Name { get; init; }

    public IconValue? Icon { get; init; }

    public string IconColor { get; init; } = "currentColor";

    public Func<Task>? OnClick { get; init; }

    public bool Active { get; init; } = true;

    public static readonly ContextAction Separator = new() {Name = "separator"};
}