﻿using System.Text;
using NintendoTools.FileFormats.Msbt;

namespace MsbtEditor;

internal sealed class MsbtFormatProvider : IMsbtFormatProvider
{
    public string FormatMessage(MsbtMessage message, string rawText) => rawText;

    public string FormatTag(MsbtMessage message, string tagName, IEnumerable<MsbtTagArgument> arguments)
    {
        var sb = new StringBuilder();
        sb.Append("{{").Append(tagName);

        foreach (var arg in arguments)
        {
            sb.Append(' ').Append(arg.Name).Append("=\"");
            if (arg.Value is Array arr)
            {
                sb.Append('[');
                for (var i = 0; i < arr.Length; ++i)
                {
                    if (i > 0) sb.Append(',');
                    sb.Append(arr.GetValue(i));
                }
                sb.Append(']');
            }
            else sb.Append(arg.Value);
            sb.Append('\"');
        }

        sb.Append("}}");
        return sb.ToString();
    }
}