﻿using System.Text;
using NintendoTools.FileFormats.Bmg;

namespace MsbtEditor;

internal sealed class BmgFormatProvider : IBmgFormatProvider
{
    public string FormatMessage(BmgMessage message, string rawText) => rawText;

    public string FormatTag(BmgMessage message, string tagName, IEnumerable<BmgTagArgument> arguments)
    {
        var sb = new StringBuilder();
        sb.Append("{{").Append(tagName);

        foreach (var arg in arguments)
        {
            sb.Append(' ').Append(arg.Name).Append("=\"");
            if (arg.Value is Array arr)
            {
                sb.Append('[');
                for (var i = 0; i < arr.Length; ++i)
                {
                    if (i > 0) sb.Append(',');
                    sb.Append(arr.GetValue(i));
                }
                sb.Append(']');
            }
            else sb.Append(arg.Value);
            sb.Append('\"');
        }

        sb.Append("}}");
        return sb.ToString();
    }
}