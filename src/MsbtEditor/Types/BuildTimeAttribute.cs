﻿using System.Globalization;

namespace MsbtEditor;

[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
internal sealed class BuildTimeAttribute(string dateTime) : Attribute
{
    public DateTime Time { get; } = DateTime.TryParse(dateTime, null, DateTimeStyles.AssumeUniversal|DateTimeStyles.AdjustToUniversal, out var value) ? value : DateTime.MinValue;
}