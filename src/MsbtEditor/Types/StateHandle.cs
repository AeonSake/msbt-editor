﻿namespace MsbtEditor;

internal sealed class StateHandle<T>(T initialValue)
{
    private static readonly EqualityComparer<T> Comparer = EqualityComparer<T>.Default;

    public T Value { get; private set; } = initialValue;

    public event EventHandler? ValueChanged;

    public void Set(T value)
    {
        if (Comparer.Equals(Value, value)) return;
        Value = value;
        ValueChanged?.Invoke(this, EventArgs.Empty);
    }
}