﻿namespace MsbtEditor;

[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
internal sealed class GitHashAttribute(string hash) : Attribute
{
    public string Hash { get; } = hash;
}