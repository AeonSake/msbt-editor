﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MsbtEditor;

internal interface IWritableOptions<out T> : IOptionsMonitor<T>
{
    void Update(Action<T> applyChanges);
}

internal sealed class WritableOptions<T> : IWritableOptions<T>
{
    #region private members
    private readonly IOptionsMonitor<T> _options;
    private readonly string _section;
    private readonly string _file;
    #endregion

    #region constructor
    public WritableOptions(IOptionsMonitor<T> options, string section, string file)
    {
        if (string.IsNullOrEmpty(section)) throw new ArgumentNullException(nameof(section));
        if (string.IsNullOrEmpty(file)) throw new ArgumentNullException(nameof(file));

        _options = options ?? throw new ArgumentNullException(nameof(options));
        _section = section;
        _file = file;
    }
    #endregion

    #region IWritableOptions interface
    public T CurrentValue => _options.CurrentValue;

    public T Get(string? name) => _options.Get(name);

    public IDisposable? OnChange(Action<T, string?> listener) => _options.OnChange(listener);
    #endregion

    #region public methods
    public void Update(Action<T> applyChanges)
    {
        var current = _options.CurrentValue;
        if (current is null) return;
        applyChanges.Invoke(current);

        var jObject = JsonConvert.DeserializeObject<JObject>(File.ReadAllText(_file));
        if (jObject is null) return;

        jObject[_section] = JToken.FromObject(current);
        File.WriteAllText(_file, JsonConvert.SerializeObject(jObject, Formatting.Indented));
    }
    #endregion
}