﻿namespace MsbtEditor;

public enum StatusType
{
    Success,
    Info,
    Warning,
    Error,
    Unknown
}