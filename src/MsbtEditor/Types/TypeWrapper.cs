﻿namespace MsbtEditor;

public sealed class TypeWrapper<T>
{
    private TypeWrapper(Type type) => WrappedType = type;

    public Type WrappedType { get; }

    public static TypeWrapper<T> From<TSource>() where TSource : T => new(typeof(TSource));

    public static implicit operator Type(TypeWrapper<T> wrapper) => wrapper.WrappedType;
}