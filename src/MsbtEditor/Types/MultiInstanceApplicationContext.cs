﻿namespace MsbtEditor;

internal class MultiInstanceApplicationContext(SynchronizationContext uiContext, params Form[] forms) : MultiInstanceApplicationContext<Form>(uiContext, forms);

internal class MultiInstanceApplicationContext<T> : ApplicationContext where T : Form
{
    #region private members
    private readonly List<T> _forms = [];
    #endregion

    #region constructor
    public MultiInstanceApplicationContext(SynchronizationContext uiContext, params T[] forms)
    {
        SynchronizationContext = uiContext;
        foreach (var form in forms) AddForm(form);
    }
    #endregion

    #region public properties
    public SynchronizationContext SynchronizationContext { get; }

    public T? LastFocusedForm { get; private set; }
    #endregion

    #region public methods
    public void AddForm(T form)
    {
        lock (_forms)
        {
            form.Activated += OnFormFocused;
            form.GotFocus += OnFormFocused;
            form.FormClosed += OnFormClosed;

            _forms.Add(form);
            SynchronizationContext.Post(_ =>
            {
                form.Show();
                form.TopMost = true;
                form.TopMost = false;
            }, null);
        }
    }

    public void ExecuteInUiThread(Action action) => SynchronizationContext.Post(_ => action(), null);
    #endregion

    #region private methods
    private void OnFormFocused(object? sender, EventArgs args)
    {
        if (sender is not T form) return;

        LastFocusedForm = form;
    }

    private void OnFormClosed(object? sender, FormClosedEventArgs args)
    {
        if (sender is not T form) return;

        lock (_forms)
        {
            _forms.Remove(form);
            if (_forms.Count == 0) ExitThread();
            else if (LastFocusedForm == form) LastFocusedForm = _forms[^1];
        }
    }
    #endregion
}