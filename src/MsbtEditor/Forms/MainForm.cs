using Microsoft.AspNetCore.Components.WebView;
using Microsoft.AspNetCore.Components.WebView.WindowsForms;
using Microsoft.Extensions.DependencyInjection;
using MsbtEditor.Components;

namespace MsbtEditor;

public sealed partial class MainForm : Form
{
    #region private members
    private AppStateService? _appStateService;
    private IEnumerable<string> _pathsToOpen;

    private FormWindowState _lastWindowState = FormWindowState.Normal;
    private bool _isInitialized;
    private bool _isClosing;
    private bool _canClose;
    #endregion

    #region constructor
    public MainForm(IServiceProvider serviceProvider, string[] pathsToOpen)
    {
        _pathsToOpen = pathsToOpen;

        var updateService = serviceProvider.GetRequiredService<UpdateService>();
        serviceProvider.GetRequiredService<ScopeManagerService>().WaitForScope(OnScopeResolved);

        InitializeComponent();
        Text = $"{Text} | Version {updateService.CurrentVersion}";

        blazorWebView.HostPage = "wwwroot\\index.html";
        blazorWebView.Services = serviceProvider;
        blazorWebView.RootComponents.Add<MainLayout>("#app");
        blazorWebView.WebView.DefaultBackgroundColor = Color.FromArgb(0xff, 0x25, 0x25, 0x26);
        blazorWebView.BlazorWebViewInitialized += BlazorWebView_Initialized;
    }
    #endregion

    #region public methods
    public async Task OpenPaths(IEnumerable<string> paths)
    {
        if (_appStateService is null || !_isInitialized)
        {
            _pathsToOpen = paths;
            return;
        }

        var files = new List<string>();
        var folders = new List<string>();
        foreach (var path in paths)
        {
            if (File.Exists(path)) files.Add(path);
            else if (Directory.Exists(path)) folders.Add(path);
        }

        await _appStateService.LoadFiles([..files]);
        foreach (var folder in folders)
        {
            await _appStateService.LoadFolder(folder);
        }

        if (WindowState == FormWindowState.Minimized)
        {
            WindowState = _lastWindowState;
            Activate();
            BringToFront();
            TopMost = true;
            TopMost = false;
        }
    }
    #endregion

    #region private methods
    private void OnScopeResolved(IServiceProvider provider)
    {
        _appStateService = provider.GetRequiredService<AppStateService>();
        provider.GetRequiredService<WindowsInteropService>().SetContext(this);
        provider.GetRequiredService<BrowserContextService>().SetContext(blazorWebView.WebView);
    }
    #endregion

    #region form events
    private async void MainForm_FormClosing(object sender, FormClosingEventArgs args)
    {
        if (args.CloseReason != CloseReason.UserClosing || _canClose || _appStateService is null) return;

        args.Cancel = true;
        if (_isClosing) return;

        _isClosing = true;
        await Task.Yield();
        _canClose = await _appStateService.OnWindowClose();
        _isClosing = false;

        if (_canClose) Close();
    }

    private void MainForm_Resize(object sender, EventArgs args)
    {
        if (WindowState != FormWindowState.Minimized) _lastWindowState = WindowState;
    }

    private async void BlazorWebView_Initialized(object? sender, BlazorWebViewInitializedEventArgs args)
    {
        _isInitialized = true;
        await OpenPaths(_pathsToOpen);
    }
    #endregion
}