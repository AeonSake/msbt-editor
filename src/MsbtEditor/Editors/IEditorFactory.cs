﻿namespace MsbtEditor;

public interface IEditorFactory
{
    public string Name { get; }

    public Task<IEditorData> CreateEditor(FileData file);
}