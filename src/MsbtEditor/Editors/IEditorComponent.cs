﻿using Microsoft.AspNetCore.Components;

namespace MsbtEditor;

public interface IEditorComponent : IComponent
{
    public IEditorData EditorData { get; set; }

    public Task Focus();
}