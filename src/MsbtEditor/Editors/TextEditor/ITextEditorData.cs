﻿using BlazorMonaco.Editor;

namespace MsbtEditor;

internal interface ITextEditorData : IEditorData
{
    public new bool IsModified { get; set; }

    public string Language { get; }

    public int TabSize { get; }

    public TextModel Model { get; }

    public object? State { get; set; }

    public Task<string> ExportText();

    public Task ImportText(string content);
}