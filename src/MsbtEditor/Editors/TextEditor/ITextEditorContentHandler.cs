﻿namespace MsbtEditor;

internal interface ITextEditorContentHandler<in T> where T : FileData
{
    public string Language { get; }

    public string DisplayName { get; }

    public int TabSize => 4;

    public bool IsReadOnly => false;

    public Task<string> Load(T file);

    public Task Save(T file, string content);
}