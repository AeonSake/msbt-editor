﻿using BlazorMonaco.Editor;
using Microsoft.Extensions.Logging;
using MsbtEditor.Components.Editor;

namespace MsbtEditor;

internal sealed class TextEditorProvider : IEditorProvider
{
    #region private members
    private readonly JsRuntimeProvider _runtimeProvider;
    private readonly Dictionary<Type, List<IEditorFactory>> _factories = [];
    #endregion

    #region constructor
    public TextEditorProvider(JsRuntimeProvider runtimeProvider, EditorContextActionProvider editorContextActionProvider, IWritableOptions<EditorSettings> editorSettings, ILogger<TextEditorProvider> logger)
    {
        _runtimeProvider = runtimeProvider;

        editorContextActionProvider.Register<ITextEditorData>(_ =>
        [
            new ContextAction
            {
                Name = "Show Whitespace",
                Icon = editorSettings.CurrentValue.ShowWhitespace ? GlobalIcons.Check : null,
                OnClick = () =>
                {
                    editorSettings.Update(settings => settings.ShowWhitespace = !settings.ShowWhitespace);
                    logger.LogDebug("Editor whitespace was set to {State}", editorSettings.CurrentValue.ShowWhitespace);
                    return Task.CompletedTask;
                }
            },
            new ContextAction
            {
                Name = "Show Newlines",
                Icon = editorSettings.CurrentValue.ShowNewLines ? GlobalIcons.Check : null,
                OnClick = () =>
                {
                    editorSettings.Update(settings => settings.ShowNewLines = !settings.ShowNewLines);
                    logger.LogDebug("Editor new lines was set to {State}", editorSettings.CurrentValue.ShowNewLines);
                    return Task.CompletedTask;
                }
            },
            new ContextAction
            {
                Name = "Word Wrap",
                Icon = editorSettings.CurrentValue.WordWrap ? GlobalIcons.Check : null,
                OnClick = () =>
                {
                    editorSettings.Update(settings => settings.WordWrap = !settings.WordWrap);
                    logger.LogDebug("Editor word wrap was set to {State}", editorSettings.CurrentValue.WordWrap);
                    return Task.CompletedTask;
                }
            }
        ]);
    }
    #endregion

    #region IEditorProvider interface
    public IReadOnlyCollection<IEditorFactory> GetFactories(FileData file) => _factories.TryGetValue(file.GetType(), out var factories) ? factories : [];
    #endregion

    #region public methods
    public bool HasFactory(FileData file) => _factories.ContainsKey(file.GetType());

    public void Register<T>(ITextEditorContentHandler<T> contentHandler) where T : FileData
    {
        var fileType = typeof(T);
        if (!_factories.TryGetValue(fileType, out var factories))
        {
            factories = [];
            _factories.Add(fileType, factories);
        }

        factories.Add(new TextEditorFactory<T>(_runtimeProvider, contentHandler));
    }
    #endregion

    #region helper classes
    private class TextEditorFactory<T>(JsRuntimeProvider runtimeProvider, ITextEditorContentHandler<T> contentHandler) : IEditorFactory where T : FileData
    {
        #region private members
        private readonly Dictionary<FileData, List<ITextEditorData>> _editorCache = [];
        private readonly AsyncLock _lock = new();
        #endregion

        #region IEditorFactory interface
        public string Name => $"Text Editor ({contentHandler.DisplayName})";

        public async Task<IEditorData> CreateEditor(FileData file)
        {
            if (file.GetType() != typeof(T)) throw new InvalidCastException($"Cannot create {Name} for this file type.");

            if (!_editorCache.TryGetValue(file, out var editorDataList))
            {
                editorDataList = [];
                _editorCache.Add(file, editorDataList);
            }

            ITextEditorData data;
            if (editorDataList.Count > 0)
            {
                data = (ITextEditorData) editorDataList[0].Clone();
            }
            else
            {
                var content = await contentHandler.Load((T) file);
                var model = await Global.CreateModel(runtimeProvider.Runtime, content, contentHandler.Language, $"{contentHandler.Language}-{Guid.NewGuid()}-{file.Name}");
                data = new TextEditorData<T>((T) file, this, model, contentHandler, DisposeData);
            }

            editorDataList.Add(data);
            return data;
        }
        #endregion

        #region private methods
        private Task DisposeData(IEditorData editorData) => _lock.LockAsync(async () =>
        {
            if (editorData is not ITextEditorData textEditorData) return;
            if (!_editorCache.TryGetValue(editorData.File, out var editorDataList)) return;
            if (!editorDataList.Remove(textEditorData) || editorDataList.Count > 0) return;

            await textEditorData.Model.DisposeModel();
            _editorCache.Remove(editorData.File);
        });
        #endregion
    }

    private class TextEditorData<T> : ITextEditorData where T : FileData
    {
        #region private members
        private readonly T _file;
        private readonly ITextEditorContentHandler<T> _contentHandler;
        private readonly Func<IEditorData, Task> _disposeData;
        private readonly StateHandle<bool> _modified;
        private bool _readOnly;
        #endregion

        #region constructors
        public TextEditorData(T file, IEditorFactory factory, TextModel model, ITextEditorContentHandler<T> contentHandler, Func<IEditorData, Task> disposeData) : this(file, factory, model, contentHandler, new StateHandle<bool>(false), disposeData)
        { }

        private TextEditorData(T file, IEditorFactory factory, TextModel model, ITextEditorContentHandler<T> contentHandler, StateHandle<bool> modifiedHandle, Func<IEditorData, Task> disposeData)
        {
            _file = file;
            _contentHandler = contentHandler;
            _disposeData = disposeData;
            _modified = modifiedHandle;

            Id = $"text-editor-{contentHandler.Language}-{model.Id}";
            EditorFactory = factory;
            Model = model;
        }
        #endregion

        #region IEditorData interface
        public string Id { get; }

        public TypeWrapper<IEditorComponent> Editor { get; } = TypeWrapper<IEditorComponent>.From<TextEditor>();

        public IEditorFactory EditorFactory { get; }

        public FileData File => _file;

        public bool IsModified
        {
            get => _modified.Value;
            set
            {
                _modified.Set(value);
                _file.IsModified = value;
            }
        }

        public bool IsReadOnly => _file.IsReadOnly || _contentHandler.IsReadOnly || _readOnly;

        public event EventHandler? Modified
        {
            add => _modified.ValueChanged += value;
            remove => _modified.ValueChanged -= value;
        }

        public Task Reload() => Task.Run(async () =>
        {
            try
            {
                var content = await _contentHandler.Load(_file);
                await Model.SetValue(content);
                IsModified = false;
                _readOnly = false;
            }
            catch (Exception ex)
            {
                await Model.SetValue($"Failed to load file.\n{ex.Message}");
                _readOnly = true;
                throw;
            }
        });

        public Task Save() => Task.Run(async () =>
        {
            var content = await Model.GetValue(EndOfLinePreference.LF, false);
            await _contentHandler.Save(_file, content);
            IsModified = false;
        });

        public IEditorData Clone() => new TextEditorData<T>(_file, EditorFactory, Model, _contentHandler, _modified, _disposeData);

        public async ValueTask DisposeAsync() => await _disposeData.Invoke(this);
        #endregion

        #region ITextEditorData interface
        public string Language => _contentHandler.Language;

        public int TabSize => _contentHandler.TabSize;

        public TextModel Model { get; }

        public object? State { get; set; }

        public Task<string> ExportText() => Task.Run(() => _contentHandler.Load(_file));

        public Task ImportText(string content) => Task.Run(async () =>
        {
            await Model.SetValue(content);
            await _contentHandler.Save(_file, content);
            IsModified = false;
            _readOnly = false;
        });
        #endregion
    }
    #endregion
}