﻿namespace MsbtEditor;

public interface IEditorData : IAsyncDisposable
{
    public string Id { get; }

    public TypeWrapper<IEditorComponent> Editor { get; }

    public IEditorFactory EditorFactory { get; }

    public FileData File { get; }

    public bool IsModified { get; }

    public bool IsReadOnly { get; }

    public event EventHandler Modified;

    public Task Reload();

    public Task Save();

    public IEditorData Clone();
}