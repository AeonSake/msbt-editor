﻿namespace MsbtEditor;

internal interface IEditorProvider
{
    public IReadOnlyCollection<IEditorFactory> GetFactories(FileData file);
}