﻿using System.Collections;
using Newtonsoft.Json;

namespace MsbtEditor;

internal sealed class SingleOrArrayConverter : JsonConverter
{
    public override bool CanConvert(Type objectType) => objectType == typeof(SingleOrArray<>);

    public override bool CanRead => true;

    public override bool CanWrite => true;

    public override object? ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
    {
        var valType = objectType.GetGenericArguments()[0];
        var data = Activator.CreateInstance(objectType);
        var addMethod = data?.GetType().GetMethod("Add");

        if (reader.TokenType is JsonToken.StartArray)
        {
            var array = (Array?) serializer.Deserialize(reader, valType.MakeArrayType());
            if (array is null) return existingValue;

            foreach (var item in array) addMethod?.Invoke(data, [item]);
        }
        else
        {
            var item = serializer.Deserialize(reader, valType);
            if (item is null) return existingValue;

            addMethod?.Invoke(data, [item]);
        }

        return data;
    }

    public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
    {
        var list = (ICollection?) value;
        if (list is null)
        {
            writer.WriteStartArray();
            writer.WriteEndArray();
            return;
        }

        if (list.Count == 1)
        {
            foreach (var item in list)
            {
                serializer.Serialize(writer, item);
                break;
            }
        }
        else serializer.Serialize(writer, list.Cast<object>().ToArray());
    }
}