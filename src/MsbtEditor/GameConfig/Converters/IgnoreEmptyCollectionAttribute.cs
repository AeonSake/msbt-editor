﻿namespace MsbtEditor;

[AttributeUsage(AttributeTargets.Property)]
internal sealed class IgnoreEmptyCollectionAttribute : Attribute;