﻿using Newtonsoft.Json.Converters;

namespace MsbtEditor;

internal class LowercaseStringEnumConverter() : StringEnumConverter(LowercaseNamingStrategy.Instance);