﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NintendoTools.FileFormats;

namespace MsbtEditor;

internal sealed class AlignmentTableConverter : JsonConverter<AlignmentTable>
{
    public override bool CanRead => true;

    public override bool CanWrite => true;

    public override AlignmentTable? ReadJson(JsonReader reader, Type objectType, AlignmentTable? existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        if (reader.TokenType is JsonToken.Null) return existingValue;
        if (reader.TokenType is not JsonToken.StartObject) throw new JsonReaderException("Invalid data type for alignment table.");

        var obj = JObject.Load(reader);
        var table = new AlignmentTable();

        if (obj.TryGetValue("default", StringComparison.OrdinalIgnoreCase, out var defaultValue))
        {
            table.Default = defaultValue.ToObject<int>(serializer);
        }
        if (obj.TryGetValue("extensions", StringComparison.OrdinalIgnoreCase, out var extensionToken) && extensionToken is JObject extensionObj)
        {
            foreach (var (extension, value) in extensionObj)
            {
                if (value is null) continue;
                table.Add(extension, value.ToObject<int>(serializer));
            }
        }

        return table;
    }

    public override void WriteJson(JsonWriter writer, AlignmentTable? value, JsonSerializer serializer)
    {
        if (value is null)
        {
            writer.WriteNull();
            return;
        }

        writer.WriteStartObject();
        writer.WritePropertyName("default");
        writer.WriteValue(value.Default);
        if (value.Count > 0)
        {
            writer.WritePropertyName("extensions");
            writer.WriteStartObject();
            foreach (var (extension, alignment) in value)
            {
                writer.WritePropertyName(extension);
                writer.WriteValue(alignment);
            }
            writer.WriteEndObject();
        }
        writer.WriteEndObject();
    }
}