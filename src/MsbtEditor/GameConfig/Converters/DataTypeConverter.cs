﻿using System.Globalization;
using Newtonsoft.Json;
using NintendoTools.FileFormats.Msbt;

namespace MsbtEditor;

internal sealed class DataTypeConverter : JsonConverter<DataType>
{
    private static readonly IReadOnlyDictionary<string, DataType> Types = DataTypes.GetTypeList();

    public override bool CanRead => true;

    public override bool CanWrite => true;

    public override DataType? ReadJson(JsonReader reader, Type objectType, DataType? existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        if (reader.TokenType is JsonToken.Null) return existingValue;
        if (reader.TokenType is not JsonToken.String) throw new JsonReaderException("Invalid value for data type.");

        var typeValue = (string?) reader.Value;

        if (typeValue is null)
        {
            if (hasExistingValue) return existingValue;
            throw new JsonReaderException("Missing value for data type.");
        }

        if (Types.TryGetValue(typeValue, out var dataType)) return dataType;
        if (typeValue.StartsWith("0x") && ulong.TryParse(typeValue[2..], NumberStyles.AllowHexSpecifier, null, out _)) return DataTypes.GetPadding(typeValue);

        throw new JsonReaderException($"Unknown data type: {typeValue}");
    }

    public override void WriteJson(JsonWriter writer, DataType? value, JsonSerializer serializer)
    {
        if (value is null) writer.WriteNull();
        else writer.WriteValue(value.Name);
    }
}