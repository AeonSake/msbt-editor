﻿using Newtonsoft.Json;

namespace MsbtEditor;

internal sealed class FilePathFilterConverter : JsonConverter<FilePathFilter>
{
    public override bool CanRead => true;

    public override bool CanWrite => true;

    public override FilePathFilter? ReadJson(JsonReader reader, Type objectType, FilePathFilter? existingValue, bool hasExistingValue, JsonSerializer serializer) => reader.TokenType switch
    {
        JsonToken.Null   => existingValue,
        JsonToken.String => new FilePathFilter((string?) reader.Value),
        _                => throw new JsonReaderException("Invalid value type for file filter.")
    };

    public override void WriteJson(JsonWriter writer, FilePathFilter? value, JsonSerializer serializer)
    {
        if (value is null) writer.WriteNull();
        else writer.WriteValue(value.ToString());
    }
}