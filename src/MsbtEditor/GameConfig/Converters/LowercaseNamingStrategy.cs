﻿using Newtonsoft.Json.Serialization;

namespace MsbtEditor;

internal class LowercaseNamingStrategy : NamingStrategy
{
    public static readonly LowercaseNamingStrategy Instance = new();

    protected override string ResolvePropertyName(string name) => name.ToLowerInvariant();
}