﻿namespace MsbtEditor;

[AttributeUsage(AttributeTargets.Property)]
internal sealed class IgnoreEmptyObjectAttribute : Attribute;