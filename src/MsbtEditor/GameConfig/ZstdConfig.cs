﻿using Newtonsoft.Json;

namespace MsbtEditor;

public class ZstdConfig : FileFilterConfig, IConfig
{
    #region public properties
    [JsonProperty("compressionLevel")]
    public int CompressionLevel { get; set; } = 19;

    [JsonProperty("dict")]
    public string? DictPath { get; set; }

    [JsonIgnore]
    public byte[]? Dict { get; set; }
    #endregion

    #region public methods
    public void Validate()
    {
        if (DictPath is not null)
        {
            if (!Path.IsPathRooted(DictPath)) DictPath = Path.Combine(CommonPaths.ConfigFolder, DictPath);
            DictPath = DictPath.Replace('\\', '/');
            if (!File.Exists(DictPath)) throw new FileNotFoundException("File specified for the ZSTD dictionary does not exist.", DictPath);
            Dict = File.ReadAllBytes(DictPath);
        }
    }
    #endregion
}