﻿using Newtonsoft.Json;

namespace MsbtEditor;

[JsonConverter(typeof(ValueInfoConverter))]
public class ValueInfo
{
    [JsonProperty("name")]
    public required string Name { get; set; }

    [JsonProperty("description")]
    public string Description { get; set; } = string.Empty;
}