﻿using System.ComponentModel;
using Newtonsoft.Json;
using NintendoTools.FileFormats.Msbt;

namespace MsbtEditor;

public class ArgumentInfo
{
    [JsonProperty("name", DefaultValueHandling = DefaultValueHandling.Ignore), DefaultValue("")]
    public string Name { get; set; } = string.Empty;

    [JsonProperty("description", DefaultValueHandling = DefaultValueHandling.Ignore), DefaultValue("")]
    public string Description { get; set; } = string.Empty;

    [JsonProperty("dataType"), JsonConverter(typeof(DataTypeConverter))]
    public DataType DataType { get; set; } = DataTypes.UInt16;

    [JsonProperty("valueMap"), IgnoreEmptyCollection]
    public Dictionary<string, ValueInfo> ValueMap { get; set; } = [];

    [JsonProperty("arrayLength", DefaultValueHandling = DefaultValueHandling.Ignore)]
    public int ArrayLength { get; set; }

    /*
    [JsonProperty("defaultValue", DefaultValueHandling = DefaultValueHandling.Ignore), DefaultValue("")]
    public string DefaultValue { get; set; } = string.Empty;
    */
}