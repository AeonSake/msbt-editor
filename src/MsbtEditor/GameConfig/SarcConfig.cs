﻿using Newtonsoft.Json;
using NintendoTools.FileFormats;

namespace MsbtEditor;

public class SarcConfig : FileFilterConfig, IConfig
{
    #region public properties
    [JsonProperty("alignment"), JsonConverter(typeof(AlignmentTableConverter))]
    public AlignmentTable Alignment { get; set; } = [];

    [JsonProperty("checkAlignment", DefaultValueHandling = DefaultValueHandling.Ignore)]
    public bool CheckAlignment { get; set; }
    #endregion

    #region public methods
    public void Validate()
    { }
    #endregion
}