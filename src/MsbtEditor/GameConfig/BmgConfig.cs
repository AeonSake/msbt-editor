﻿using System.ComponentModel;
using Newtonsoft.Json;
using NintendoTools.FileFormats;
using NintendoTools.FileFormats.Bmg;
using NintendoTools.FileFormats.Msbt;

namespace MsbtEditor;

public class BmgConfig : IConfig
{
    #region public properties
    [JsonProperty("newline", DefaultValueHandling = DefaultValueHandling.Ignore), JsonConverter(typeof(LowercaseStringEnumConverter)), DefaultValue(NewlineType.LF)]
    public NewlineType Newline { get; set; } = NewlineType.LF;

    [JsonProperty("tags"), IgnoreEmptyCollection]
    public List<TagInfo> Tags { get; set; } = [];

    [JsonIgnore]
    public BmgDynamicTagMap TagMap { get; set; } = new();
    #endregion

    #region public methods
    public void Validate()
    {
        var map = new BmgDynamicTagMap();
        foreach (var tag in Tags)
        {
            if (tag.Group is < 0 or > byte.MaxValue) throw new IndexOutOfRangeException("Tag group ID out of range (must be between 0 and 255).");
            if (tag.Type > ushort.MaxValue) throw new IndexOutOfRangeException("Tag type ID out of range (must be between 0 and 65535).");

            var typeList = new ushort[tag.Types.Length];
            for (var i = 0; i < typeList.Length; ++i)
            {
                var value = tag.Types[i];
                if (value is < 0 or > ushort.MaxValue) throw new IndexOutOfRangeException("Tag type ID out of range (must be between 0 and 65535).");
                typeList[i] = (ushort) value;
            }

            var typeMap = new List<MsbtValueInfo>(tag.TypeMap.Count);
            foreach (var (key, info) in tag.TypeMap)
            {
                typeMap.Add(new MsbtValueInfo
                {
                    Value = key,
                    Name = info.Name,
                    Description = info.Description
                });
            }

            var arguments = new List<MsbtArgumentInfo>(tag.Arguments.Count);
            foreach (var argument in tag.Arguments)
            {
                var valueMap = new List<MsbtValueInfo>(argument.ValueMap.Count);
                foreach (var (key, info) in argument.ValueMap)
                {
                    valueMap.Add(new MsbtValueInfo
                    {
                        Value = key,
                        Name = info.Name,
                        Description = info.Description
                    });
                }

                arguments.Add(new MsbtArgumentInfo
                {
                    DataType = argument.DataType,
                    ValueMap = MsbtValueMap.Create(valueMap, argument.DataType),
                    ArrayLength = argument.ArrayLength < 0 ? 0 : argument.ArrayLength,
                    Name = string.IsNullOrEmpty(argument.Name) ? $"arg{arguments.Count + 1}" : argument.Name,
                    Description = argument.Description
                });
            }

            map.AddInfo(new BmgTagInfo
            {
                Group = (byte) tag.Group,
                Type = tag.Type > -1 ? (ushort) tag.Type : null,
                TypeList = typeList,
                TypeMap = MsbtValueMap.Create(typeMap, DataTypes.UInt16),
                HasDiscard = tag.Discard,
                Name = tag.Name,
                Description = tag.Description,
                Arguments = arguments
            });
        }
        TagMap = map;
    }
    #endregion
}