﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace MsbtEditor;

public class GameConfig : IConfig
{
    #region general data
    [JsonProperty("game", DefaultValueHandling = DefaultValueHandling.Ignore), DefaultValue("")]
    public string Game { get; set; } = string.Empty;
    #endregion

    #region file-specific configs
    [JsonProperty("zstd"), IgnoreEmptyCollection]
    public SingleOrArray<ZstdConfig> Zstd { get; set; } = [];

    [JsonProperty("darc"), IgnoreEmptyCollection]
    public SingleOrArray<DarcConfig> Darc { get; set; } = [];

    [JsonProperty("sarc"), IgnoreEmptyCollection]
    public SingleOrArray<SarcConfig> Sarc { get; set; } = [];

    [JsonProperty("bmg")]
    public BmgConfig Bmg { get; set; } = new();

    [JsonProperty("msbt")]
    public MsbtConfig Msbt { get; set; } = new();

    [JsonProperty("bcsv")]
    public BcsvConfig Bcsv { get; set; } = new();

    [JsonProperty("byml")]
    public BymlConfig Byml { get; set; } = new();
    #endregion

    #region public methods
    public void Validate()
    {
        Zstd.Validate();
        Darc.Validate();
        Sarc.Validate();
        Bmg.Validate();
        Msbt.Validate();
        Bcsv.Validate();
        Byml.Validate();
    }
    #endregion
}