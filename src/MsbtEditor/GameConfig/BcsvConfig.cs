﻿using Newtonsoft.Json;

namespace MsbtEditor;

public class BcsvConfig : IConfig
{
    [JsonProperty("headers"), IgnoreEmptyCollection]
    public List<BcsvHeader> Headers { get; set; } = [];

    [JsonProperty("fieldHashes", DefaultValueHandling = DefaultValueHandling.Ignore)]
    public FieldHashes FieldHashes { get; set; } = new();

    public void Validate()
    {
        // allow field hashes to load the cache
        FieldHashes.Validate();
    }
}