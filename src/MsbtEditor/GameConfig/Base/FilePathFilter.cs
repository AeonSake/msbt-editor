﻿using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace MsbtEditor;

[JsonConverter(typeof(FilePathFilterConverter))]
public sealed class FilePathFilter
{
    #region private members
    private readonly string _filter;
    #endregion

    #region constructor
    public FilePathFilter(string? filter)
    {
        _filter = string.IsNullOrEmpty(filter) ? "*" : filter.Replace('\\', '/');
        Value = new Regex("^" + Regex.Escape(_filter).Replace("\\*", ".*") + "$", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
    }
    #endregion

    #region public properties
    public Regex Value { get; }
    #endregion

    #region public methods
    public override string ToString() => _filter;
    #endregion
}