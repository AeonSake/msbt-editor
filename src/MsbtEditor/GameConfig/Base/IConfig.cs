﻿namespace MsbtEditor;

internal interface IConfig
{
    public void Validate();
}