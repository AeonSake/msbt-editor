﻿using System.Collections;
using Newtonsoft.Json;

namespace MsbtEditor;

[JsonConverter(typeof(SingleOrArrayConverter))]
public sealed class SingleOrArray<T> : ICollection<T>, ICollection
{
    #region private members
    private readonly List<T> _list = [];
    #endregion

    #region constructors
    public SingleOrArray() { }

    public SingleOrArray(T item) => _list.Add(item);

    public SingleOrArray(IEnumerable<T> items) => _list.AddRange(items);

    public SingleOrArray(params T[] items) => _list.AddRange(items);
    #endregion

    #region ICollection interface
    public bool IsReadOnly => false;

    public int Count => _list.Count;

    public bool IsSynchronized => false;

    public object SyncRoot => this;

    public T this[int index] => _list[index];

    public void Add(T item)
    {
        if (item is null || _list.Contains(item)) return;
        _list.Add(item);
    }

    public void AddRange(IEnumerable<T> items)
    {
        foreach (var item in items) Add(item);
    }

    public bool Remove(T item) => _list.Remove(item);

    public void Clear() => _list.Clear();

    public bool Contains(T item) => _list.Contains(item);

    public void CopyTo(T[] array, int arrayIndex) => _list.CopyTo(array, arrayIndex);

    public void CopyTo(Array array, int index) => ((ICollection) _list).CopyTo(array, index);

    public IEnumerator<T> GetEnumerator() => _list.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    #endregion
}