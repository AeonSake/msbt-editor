﻿using Newtonsoft.Json;

namespace MsbtEditor;

public abstract class FileFilterConfig
{
    [JsonProperty("filter", Order = 99), IgnoreEmptyCollection]
    public SingleOrArray<FilePathFilter> Filter { get; set; } = [];

    public bool IsMatch(string filePath) => Filter.Count == 0 || Filter.Any(filter => filter.Value.IsMatch(filePath));
}