﻿namespace MsbtEditor;

internal static class GameConfigExtensions
{
    private static readonly Dictionary<Type, object> DefaultConfigs = [];

    public static T FindMatch<T>(this SingleOrArray<T> configs, string filePath) where T : FileFilterConfig
    {
        foreach (var config in configs)
        {
            if (config.IsMatch(filePath)) return config;
        }

        return GetDefaultMatch(configs);
    }

    public static T GetDefaultMatch<T>(this SingleOrArray<T> configs) where T : FileFilterConfig
    {
        var type = typeof(T);

        //cache default configs
        if (!DefaultConfigs.TryGetValue(type, out var defaultConfig))
        {
            defaultConfig = Activator.CreateInstance<T>();
            DefaultConfigs.Add(type, defaultConfig);
        }

        return (T) defaultConfig;
    }

    public static void Validate<T>(this SingleOrArray<T> configs) where T : IConfig
    {
        foreach (var config in configs) config.Validate();
    }
}