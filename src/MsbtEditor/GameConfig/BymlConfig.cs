﻿using Newtonsoft.Json;
using NintendoTools.Hashing;

namespace MsbtEditor;

public class BymlConfig : IConfig
{
    [JsonProperty("hashes"), IgnoreEmptyCollection]
    public List<string> Hashes { get; set; } = [];

    [JsonIgnore]
    public Dictionary<string, string> HashValues { get; set; } = [];

    public void Validate()
    {
        var crc32 = new Crc32Hash();
        var hashValues = new Dictionary<string, string>(Hashes.Count);
        foreach (var label in Hashes)
        {
            var hash = crc32.Compute(label);
            hashValues.TryAdd(hash, label);
        }

        HashValues = hashValues;
    }
}