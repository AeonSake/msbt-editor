﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace MsbtEditor;

public class BcsvHeader
{
    [JsonProperty("hash", DefaultValueHandling = DefaultValueHandling.Ignore), DefaultValue("")]
    public string Hash { get; set; } = string.Empty;

    [JsonProperty("name", DefaultValueHandling = DefaultValueHandling.Ignore), DefaultValue("")]
    public string Name { get; set; } = string.Empty;

    [JsonProperty("type", DefaultValueHandling = DefaultValueHandling.Ignore), DefaultValue("")]
    public string DataType { get; set; } = string.Empty;
}