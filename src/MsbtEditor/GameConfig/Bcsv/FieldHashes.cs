﻿using Newtonsoft.Json;
using NintendoTools.Hashing;

namespace MsbtEditor;

public class FieldHashes : IConfig
{
    [JsonProperty("crc32"), IgnoreEmptyCollection]
    public List<string> CrcHashes { get; set; } = [];


    [JsonProperty("mmh3"), IgnoreEmptyCollection]
    public List<string> Mmh3Hashes { get; set; } = [];

    /// <summary>
    /// Gets a dictionary of CRC32 hashes for enum values.
    /// </summary>
    [JsonIgnore]
    public Dictionary<string, string> Crc32Values { get; set; } = [];

    /// <summary>
    /// Gets a dictionary of MMH3 hashes for enum values.
    /// </summary>
    [JsonIgnore]
    public Dictionary<string, string> Mmh3Values { get; set; } = [];

    #region public methods
    public void Validate()
    {
        var crc32 = new Crc32Hash();
        var crc32Values = new Dictionary<string, string>(CrcHashes.Count);
        foreach (var label in CrcHashes)
        {
            var hash = crc32.Compute(label);
            crc32Values.TryAdd(hash, label);
        }

        var mmh3 = new Mmh3Hash();
        var mmh3Values = new Dictionary<string, string>(Mmh3Hashes.Count);
        foreach(var label in Mmh3Hashes)
        {
            var hash = mmh3.Compute(label);
            mmh3Values.TryAdd(hash, label);
        }

        Crc32Values = crc32Values;
        Mmh3Values = mmh3Values;
    }
    #endregion
}