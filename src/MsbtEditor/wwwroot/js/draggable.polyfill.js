﻿(function() {
    const moveThreshold = 5;
    let isDragging = false;
    let dragElement = null;
    let ghostElement = null;
    let currentElement = null;
    const startPosition = {x: 0, y: 0};

    document.addEventListener('mousedown', function (e) {
        if (e.buttons !== 1) return;

        //find draggable element
        let element = e.target;
        while (element && element !== document.body) {
            if (element.getAttribute('draggable') === 'true') {
                dragElement = element;
                break;
            }
            element = element.parentElement;
        }
        if (!dragElement) return;

        dragElement.setAttribute('draggable', 'false');
        startPosition.x = e.clientX;
        startPosition.y = e.clientY;
    });

    document.addEventListener('mousemove', function (e) {
        if (!dragElement || e.buttons !== 1) return;

        const x = e.clientX - startPosition.x;
        const y = e.clientY - startPosition.y;

        //only start dragging if threshold was reached
        if (!isDragging && (Math.abs(x) > moveThreshold || Math.abs(y) > moveThreshold)) {
            isDragging = true;

            //fire dragstart event
            dragElement.dispatchEvent(createEvent('dragstart', {
                bubbles: true,
                cancelable: true,
                clientX: e.clientX,
                clientY: e.clientY
            }));

            //build ghost element
            ghostElement = dragElement.cloneNode(true);
            const computedStyles = window.getComputedStyle(dragElement);
            for (let prop of computedStyles) ghostElement.style[prop] = computedStyles[prop];

            ghostElement.style.position = 'fixed';
            ghostElement.style.left = `${e.clientX}px`;
            ghostElement.style.top = `${e.clientY}px`;
            ghostElement.style.width = `${dragElement.width}px`;
            ghostElement.style.height = `${dragElement.height}px`;
            ghostElement.style.opacity = '0.7';
            ghostElement.style.zIndex = '1000';
            ghostElement.style.pointerEvents = 'none';

            document.body.appendChild(ghostElement);
        }
        if (!isDragging) return;

        //move ghost element
        ghostElement.style.transform = `translate(${x}px, ${y}px)`;

        //fire drag event
        dragElement.dispatchEvent(createEvent('drag', {
            bubbles: true,
            cancelable: true,
            clientX: e.clientX,
            clientY: e.clientY
        }));

        //find current element below
        ghostElement.style.display = 'none';
        let element = document.elementFromPoint(e.clientX, e.clientY);
        ghostElement.style.display = '';
        if (!element) return;

        //fire dragenter/dragleave events
        if (element !== currentElement) {
            element.dispatchEvent(createEvent('dragenter', {
                bubbles: true,
                cancelable: true,
                clientX: e.clientX,
                clientY: e.clientY
            }));

            if (currentElement) {
                currentElement.dispatchEvent(createEvent('dragleave', {
                    bubbles: true,
                    cancelable: false,
                    clientX: e.clientX,
                    clientY: e.clientY
                }));
            }

            currentElement = element;
        }

        //fire dragover event
        element.dispatchEvent(createEvent('dragover', {
            bubbles: true,
            cancelable: true,
            clientX: e.clientX,
            clientY: e.clientY
        }));
    });

    document.addEventListener('mouseup', function (e) {
        if (!isDragging) {
            if (dragElement) dragElement.setAttribute('draggable', 'true');
            dragElement = null;
            return;
        }

        //fire drop event
        if (currentElement) {
            currentElement.dispatchEvent(createEvent('drop', {
                bubbles: true,
                cancelable: true,
                clientX: e.clientX,
                clientY: e.clientY
            }));
            /*currentElement.dispatchEvent(createEvent('dragleave', {
                bubbles: true,
                cancelable: false,
                clientX: e.clientX,
                clientY: e.clientY
            }));*/
        }

        //fire dragend event
        dragElement.dispatchEvent(createEvent('dragend', {
            bubbles: true,
            cancelable: false,
            clientX: e.clientX,
            clientY: e.clientY
        }));

        isDragging = false;
        dragElement.setAttribute('draggable', 'true');
        document.body.removeChild(ghostElement);
        dragElement = null;
        ghostElement = null;
        currentElement = null;
    });

    function createEvent(type, options) {
        let event = new Event(type, options);
        event.dataTransfer = new MockDataTransfer();
        return event;
    }

    function MockDataTransfer() {
        this.dropEffect = 'move';
        this.effectAllowed = 'all';
        this.files = [];
        this.items = [];
        this.types = [];
        this.setData = function (format, data) {
            this.items.push({ format, data });
            this.types.push(format);
        };
        this.getData = function (format) {
            let item = this.items.find(i => i.format === format);
            return item ? item.data : '';
        };
        this.clearData = function (format) {
            this.items = this.items.filter(i => i.format !== format);
            this.types = this.types.filter(t => t !== format);
        };
    }
})();