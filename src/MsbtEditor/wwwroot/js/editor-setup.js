﻿window.onload = function () {
    monaco.editor.defineTheme('msbt-editor', {
        base: 'vs-dark',
        inherit: true,
        rules: [
            {token: 'delimiter',       foreground: '#808080'},
            {token: 'keyword',         foreground: '#569cd6'},
            {token: 'attribute.name',  foreground: '#9cdcfe'},
            {token: 'attribute.value', foreground: '#ce9178'},
            {token: 'comment',         foreground: '#008000'},
            {token: 'type.yaml',       foreground: '#569cd6'}
        ],
        colors: {}
    });

    //support for MSBT syntax
    monaco.languages.register({
        id: 'msbt',
        extensions: ['.msbt', '.bmg']
    });
    monaco.languages.setMonarchTokensProvider('msbt', {
        defaultToken: '',
        ignoreCase: true,
        tokenizer: {
            root: [
                [/^%%%$/, 'delimiter', '@header'],
                [/^---$/, 'delimiter', '@message'],
                [/^(\{\{)(---)(\}\})$/, ['delimiter', '', 'delimiter']],
                [/^(\{\{)(\{\{---\}\})(\}\})$/, ['delimiter', '', 'delimiter']],
                [/(\{\{)(\s*)([A-Za-z0-9_]+:[A-Za-z0-9_]+|[A-Za-z0-9_]+)/, ['delimiter', '', {token: 'keyword', next: '@function'}]]
            ],
            header: [
                [/(\s*[A-Za-z0-9_]+\s*:)(.*)/, ['delimiter', 'keyword']],
                [/^%%%$/, 'delimiter', '@pop']
            ],
            message: [
                [/(\s*[A-Za-z0-9_]+\s*:)(.*)/, ['delimiter', 'keyword']],
                [/^---$/, 'delimiter', '@pop']
            ],
            function: [
                [/([A-Za-z0-9_]+)(=)("[^"]*")/, ['attribute.name', 'delimiter', 'attribute.value']],
                [/\}\}/, 'delimiter', '@pop']
            ]
        }
    });
    monaco.languages.setLanguageConfiguration('msbt', {
        autoClosingPairs: [
            {open: '{{', close: '}}'},
            {open: '[', close: ']'}
        ]
    });

    //support for MSBP syntax
    monaco.languages.register({
        id: 'msbp',
        extensions: ['.msbp']
    });
    monaco.languages.setMonarchTokensProvider('msbp', {
        defaultToken: '',
        ignoreCase: true,
        tokenizer: {
            root: [
                [/^(\S+:)(.*)$/, ['attribute.name', 'attribute.value']],
                [/^(=+)$/, {token: 'comment', next: '@section'}],
                [/^(.+)$/, 'comment'],
            ],
            section: [
                [/^(\S+:)(.*)$/, ['attribute.name', 'attribute.value']],
                [/^(\s*\w+\s)(\[)(\w+)(\])$/, ['attribute.name', 'delimiter', 'keyword', 'delimiter']],
                [/^(\s*\w+\s)(\[)(\w+)(\])(\s\{)/, ['attribute.name', 'delimiter', 'keyword', 'delimiter', {token: 'delimiter', next: '@enumList'}]],
                [/^(\s*\[)(\d+)(\]\s)(\w+)$/, ['delimiter', 'attribute.value', 'delimiter', 'attribute.name']],
                [/^(\s*\[)(\d+)(:)(\d+)(\]\s)(\w+)$/, ['delimiter', 'attribute.value', 'delimiter', 'attribute.value', 'delimiter', 'attribute.name']],
                [/^(.+)$/, 'attribute.name'],
                [/^$/, {token: '', next: '@pop'}]
            ],
            enumList: [
                [/(\w)/, 'attribute.value'],
                [/(,)/, 'delimiter'],
                [/(\})/, {token: 'delimiter', next: '@pop'}]
            ]
        }
    });

    //support for MFM syntax
    monaco.languages.register({
        id: 'mfm',
        extensions: ['.mfm']
    });
    monaco.languages.setMonarchTokensProvider('mfm', {
        defaultToken: '',
        ignoreCase: true,
        tokenizer: {
            root: [
                [/^\s*#.*$/, 'comment'],
                [/^(\[\s*)(\d+)(\s*,\s*)(\d+|\d+\s*-\s*\d+|_|\(\s*\d+(?:\s*-\s*\d+)?(?:\s*,\s*\d+(?:\s*-\s*\d+)?)*\s*\)|\{[A-Za-z0-9_]+\})(\s*\]\s*)$/, ['delimiter', 'keyword', 'delimiter', 'keyword', {token: 'delimiter', next: '@function'}]],
                [/^(\[\s*)(\d+)(\s*,\s*)(\d+|\d+\s*-\s*\d+|_|\(\s*\d+(?:\s*-\s*\d+)?(?:\s*,\s*\d+(?:\s*-\s*\d+)?)*\s*\)|\{[A-Za-z0-9_]+\})(\s*\])(\s+[A-Za-z0-9_]+\s*)$/, ['delimiter', 'keyword', 'delimiter', 'keyword', 'delimiter', {token: 'attribute.name', next: '@function'}]],
                [/^(\[\s*)(\d+)(\s*,\s*)(\d+|\d+\s*-\s*\d+|_|\(\s*\d+(?:\s*-\s*\d+)?(?:\s*,\s*\d+(?:\s*-\s*\d+)?)*\s*\)|\{[A-Za-z0-9_]+\})(\s*\]\s*)((?:\s+[A-Za-z0-9_]+\s*)?)((?:\s+#.*)?)$/, ['delimiter', 'keyword', 'delimiter', 'keyword', 'delimiter', 'attribute.name', {token: 'comment', next: '@function'}]],
                [/^(map)(\s+[A-Za-z0-9_]+\s*)$/, ['delimiter', {token: 'attribute.name', next: '@map'}]],
                [/^(map)(\s+[A-Za-z0-9_]+)(\s+[A-Za-z0-9_]+\s*)$/, ['delimiter', 'attribute.name', {token: 'keyword', next: '@map'}]],
                [/^(map)(\s+[A-Za-z0-9_]+)((?:\s+[A-Za-z0-9_]+\s*)?)((?:\s+#.*)?)$/, ['delimiter', 'attribute.name', 'keyword', {token: 'comment', next: '@map'}]]
            ],
            function: [
                [/^\s*#.*$/, 'comment'],
                [/^(\s{2,}(?:[A-Za-z0-9_]+|\{[A-Za-z0-9_]+\})(?:\[\d+\])?\s*)((?:\s+[A-Za-z0-9_]+\s*)?)((?:\s+#.*)?)$/, ['keyword', 'attribute.name', 'comment']],
                [/^[^\s]+|\s[^\s].*$/, {token: '@rematch', next: '@pop'}]
            ],
            map: [
                [/^\s*#.*$/, 'comment'],
                [/^(\s{2,}-?[A-Za-z0-9_]+\s*)((?:\s+[A-Za-z0-9_]+\s*)?)((?:\s+#.*)?)$/, ['keyword', 'attribute.name', 'comment']],
                [/^[^\s]+|\s[^\s].*$/, {token: '@rematch', next: '@pop'}]
            ]
        }
    });
    monaco.languages.setLanguageConfiguration('mfm', {
        autoClosingPairs: [
            {open: '[', close: ']'},
            {open: '{', close: '}'}
        ]
    });

    //support for NL text syntax
    monaco.languages.register({
        id: 'nltxt'
    });
    monaco.languages.setMonarchTokensProvider('nltxt', {
        defaultToken: '',
        ignoreCase: true,
        tokenizer: {
            root: [
                [/^---\s+\d+\s+---$/, 'delimiter'],
                [/(\{\{)([^\{\}]*)(\}\})/, ['delimiter', 'attribute.name', 'delimiter']]
            ]
        }
    });
    monaco.languages.setLanguageConfiguration('nltxt', {
        autoClosingPairs: [
            {open: '{{', close: '}}'}
        ]
    });

    //support for BTXT syntax
    monaco.languages.register({
        id: 'btxt'
    });
    monaco.languages.setMonarchTokensProvider('btxt', {
        defaultToken: '',
        ignoreCase: true,
        tokenizer: {
            root: [
                [/^(version: )(\d+\.\d+)$/, ['delimiter', 'keyword']],
                [/^(---\s+)([!-~]+)(\s+---)$/, ['delimiter', 'keyword', 'delimiter']],
                [/(\{)([^\{\}]*)(\})/, ['delimiter', 'attribute.name', 'delimiter']]
            ]
        }
    });

    //support for BCSV text syntax
    monaco.languages.register({
        id: 'bcsv',
        extensions: ['.bcsv']
    });
    monaco.languages.setMonarchTokensProvider('bcsv', {
        ignoreCase: true,
        defaultToken: '',
        tokenizer: {
            root: [
                [/^%%%$/, 'delimiter', '@header'],
                [/^\s*$/, 'white'],
                [/^--.*/, {token: 'comment', next: '@entry'}],
                [/./, 'invalid']
            ],
            header: [
                [/(\s*[A-Za-z0-9_]+\s*:)(.*)/, ['delimiter', 'keyword']],
                [/^%%%$/, 'delimiter', '@pop']
            ],
            entry: [
                // Check for tab
                [/^\t\w+/, {
                    token: '@rematch',
                    next: '@fieldLine'
                }],

                // Check for space
                [/^ \w+/, {
                    token: '@rematch',
                    next: '@fieldLine'
                }],

                [/^\s*$/, {token: '', next: '@pop'}],

                [/^--/, 'invalid'],
                [/./, 'invalid']
            ],
            fieldLine: [
                [/(\w+)(\s*:)/, ['attribute.name', 'delimiter']],

                // TODO: Add better string detection
                // TODO: Add float detection
                //[/\b\d+\b/, {token: 'number', next: '@pop'}],

                [/[+-]?([0-9]*[.])?[0-9]+/, {token: 'number', next: '@pop'}],

                [/"[^"]*"/, {token: 'string', next: '@pop'}],

                [/^\s*$/, {token: '@rematch', next: '@pop'}],

                [/./, 'invalid'],
            ]
        }
    });
}