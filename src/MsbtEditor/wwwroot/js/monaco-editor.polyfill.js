﻿blazorMonaco.editor.saveViewState = function(id) {
    const editor = blazorMonaco.editor.getEditor(id);
    return editor.saveViewState();
}

blazorMonaco.editor.restoreViewState = function(id, state) {
    const editor = blazorMonaco.editor.getEditor(id);
    editor.restoreViewState(state);
}