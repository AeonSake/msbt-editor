﻿namespace MsbtEditor;

internal class EditorSettings
{
    public bool ShowWhitespace { get; set; }

    public bool ShowNewLines { get; set; }

    public bool WordWrap { get; set; }

    public int MaxTokenizationLineLength { get; set; } = 20000;
}