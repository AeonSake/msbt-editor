﻿namespace MsbtEditor;

internal class FileBrowserSettings
{
    public int VirtualizationThreshold { get; set; } = 100;
}