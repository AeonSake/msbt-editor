﻿export function setup(container, options) {
    const panels = container.querySelectorAll(':scope > .split-panel');

    options.expandToMin = true;
    options.onDragStart = function (p1, p2, gutter) {
        gutter.classList.add('active');
    };
    options.onDragEnd = function (p1, p2, gutter) {
        gutter.classList.remove('active');
    };

    dispose(container);
    container.splitData = {
        instance: Split(panels, options),
        options: options
    };

    for (let i = 0; i < options.panelStates.length; ++i) {
        if (options.panelStates[i] == 2) disable(container, i);
        else if (options.panelStates[i] == 1) collapse(container, i);
    }
}

export function dispose(container) {
    if (!container.splitData) return;

    container.splitData.instance.destroy();
    delete container.splitData;
}

export function collapse(container, index) {
    if (!container.splitData) return;

    collapsePanel(container.splitData, index);
}

export function expand(container, index) {
    if (!container.splitData) return;

    expandPanel(container.splitData, index);
}

export function toggle(container, index) {
    if (!container.splitData) return;

    const data = container.splitData;
    const minSize = getRelativeMinSize(data, index);
    const currentSizes = data.instance.getSizes();

    if (currentSizes[index] - minSize > 0.1) collapsePanel(data, index);
    else {
        const size = data.options.sizes[index];

        let total = 0;
        for (let i = 0; i < currentSizes.length; ++i) total += currentSizes[i];
        total += size - minSize;

        const newSizes = [];
        for (let i = 0; i < currentSizes.length; ++i) {
            if (i == index) newSizes.push(size * 100 / total);
            else newSizes.push(currentSizes[i] * 100 / total);
        }

        data.instance.setSizes(newSizes);
    }
}

export function disable(container, index) {
    if (!container.splitData) return;

    const data = container.splitData;
    collapsePanel(data, index);
    data.instance.pairs[index == 0 ? 0 : index - 1].gutter.classList.add('disabled');
}

export function enable(container, index) {
    if (!container.splitData) return;

    const data = container.splitData;
    expandPanel(data, index);
    data.instance.pairs[index == 0 ? 0 : index - 1].gutter.classList.remove('disabled');
}

function collapsePanel(data, index) {
    data.instance.collapse(index);
}

function expandPanel(data, index) {
    const size = data.options.sizes[index];
    const minSize = getRelativeMinSize(data, index);
    const currentSizes = data.instance.getSizes();

    let total = 0;
    for (let i = 0; i < currentSizes.length; ++i) total += currentSizes[i];
    total += size - minSize;

    const newSizes = [];
    for (let i = 0; i < currentSizes.length; ++i) {
        if (i == index) newSizes.push(size * 100 / total);
        else newSizes.push(currentSizes[i] * 100 / total);
    }

    data.instance.setSizes(newSizes);
}

function getRelativeMinSize(data, index) {
    const maxSize = data.options.direction == 'vertical' ? data.instance.parent.clientHeight : data.instance.parent.clientWidth;
    return data.options.minSize[index] * 100 / maxSize;
}