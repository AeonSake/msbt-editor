using Microsoft.AspNetCore.Components;

namespace MsbtEditor;

public sealed class IconValue(string value)
{
    public MarkupString Value { get; } = new(value);

    public override string ToString() => value;
}