﻿namespace MsbtEditor;

internal static class GlobalIcons
{
    //most icons are sourced from Lucide Icons: https://lucide.dev/

    #region general
    public static readonly IconValue AppIcon = new("""<path d="M23.04,4.84L23.04,19.16C23.04,19.712 22.592,20.16 22.04,20.16L1.96,20.16C1.408,20.16 0.96,19.712 0.96,19.16L0.96,4.84C0.96,4.288 1.408,3.84 1.96,3.84L22.04,3.84C22.592,3.84 23.04,4.288 23.04,4.84Z" style="fill:rgb(30,30,30);"/><path d="M23.04,4.84L23.04,5.76L0.96,5.76L0.96,4.84C0.96,4.288 1.408,3.84 1.96,3.84L22.04,3.84C22.592,3.84 23.04,4.288 23.04,4.84Z" style="fill:rgb(51,51,51);"/><path d="M6.72,5.76L6.72,20.16L1.96,20.16C1.408,20.16 0.96,19.712 0.96,19.16L0.96,5.76L6.72,5.76Z" style="fill:rgb(37,37,38);"/><rect x="1.44" y="7.68" width="4.8" height="0.576" style="fill:rgb(170,170,170);"/><rect x="1.44" y="13.44" width="2.88" height="0.576" style="fill:rgb(170,170,170);"/><rect x="1.44" y="12" width="3.84" height="0.576" style="fill:rgb(170,170,170);"/><rect x="1.44" y="10.56" width="4.32" height="0.576" style="fill:rgb(170,170,170);"/><rect x="1.44" y="9.12" width="3.36" height="0.576" style="fill:rgb(170,170,170);"/><path d="M11.831,9.236L11.831,14.764L10.655,14.764L10.655,11.032L10.185,14.764L9.351,14.764L8.856,11.117L8.856,14.764L7.68,14.764L7.68,9.236L9.422,9.236C9.473,9.568 9.527,9.96 9.585,10.411L9.77,11.817L10.077,9.236L11.831,9.236Z" style="fill:rgb(19,124,201);fill-rule:nonzero;"/><path d="M15.222,10.909L13.972,10.909L13.972,10.499C13.972,10.308 13.956,10.186 13.924,10.134C13.893,10.082 13.839,10.056 13.765,10.056C13.684,10.056 13.622,10.091 13.579,10.161C13.539,10.232 13.519,10.339 13.519,10.482C13.519,10.667 13.542,10.806 13.589,10.899C13.634,10.992 13.76,11.105 13.969,11.237C14.568,11.617 14.945,11.929 15.101,12.172C15.256,12.416 15.334,12.809 15.334,13.35C15.334,13.744 15.29,14.034 15.203,14.221C15.118,14.408 14.951,14.565 14.704,14.692C14.457,14.817 14.169,14.88 13.841,14.88C13.481,14.88 13.173,14.807 12.918,14.661C12.664,14.516 12.498,14.33 12.419,14.105C12.34,13.88 12.301,13.56 12.301,13.146L12.301,12.784L13.551,12.784L13.551,13.456C13.551,13.663 13.568,13.797 13.602,13.856C13.638,13.915 13.701,13.944 13.79,13.944C13.88,13.944 13.946,13.907 13.988,13.832C14.033,13.757 14.055,13.645 14.055,13.497C14.055,13.172 14.014,12.959 13.931,12.859C13.846,12.759 13.636,12.591 13.301,12.357C12.967,12.12 12.745,11.948 12.637,11.841C12.528,11.734 12.437,11.586 12.365,11.397C12.295,11.208 12.259,10.967 12.259,10.674C12.259,10.25 12.31,9.941 12.41,9.745C12.512,9.549 12.676,9.397 12.902,9.287C13.128,9.176 13.4,9.12 13.72,9.12C14.069,9.12 14.367,9.18 14.612,9.301C14.859,9.422 15.022,9.574 15.101,9.758C15.181,9.941 15.222,10.251 15.222,10.691L15.222,10.909Z" style="fill:rgb(19,124,201);fill-rule:nonzero;"/><path d="M15.756,9.236L17.098,9.236C17.522,9.236 17.842,9.271 18.06,9.342C18.279,9.412 18.456,9.556 18.59,9.772C18.724,9.986 18.792,10.332 18.792,10.81C18.792,11.133 18.744,11.359 18.648,11.486C18.554,11.614 18.368,11.711 18.089,11.78C18.4,11.855 18.61,11.98 18.721,12.155C18.832,12.328 18.887,12.595 18.887,12.954L18.887,13.466C18.887,13.84 18.847,14.116 18.766,14.296C18.687,14.476 18.56,14.599 18.386,14.665C18.211,14.731 17.853,14.764 17.312,14.764L15.756,14.764L15.756,9.236ZM17.101,10.182L17.101,11.411C17.159,11.409 17.203,11.408 17.235,11.408C17.367,11.408 17.452,11.373 17.488,11.305C17.524,11.235 17.542,11.035 17.542,10.708C17.542,10.535 17.527,10.414 17.497,10.346C17.467,10.275 17.428,10.231 17.379,10.213C17.332,10.194 17.24,10.184 17.101,10.182ZM17.101,12.271L17.101,13.818C17.291,13.811 17.411,13.779 17.462,13.723C17.515,13.666 17.542,13.526 17.542,13.303L17.542,12.787C17.542,12.55 17.519,12.407 17.472,12.357C17.425,12.307 17.301,12.278 17.101,12.271Z" style="fill:rgb(19,124,201);fill-rule:nonzero;"/><path d="M22.08,9.236L22.08,10.342L21.281,10.342L21.281,14.764L19.936,14.764L19.936,10.342L19.14,10.342L19.14,9.236L22.08,9.236Z" style="fill:rgb(19,124,201);fill-rule:nonzero;"/><circle cx="1.92" cy="4.8" r="0.36" style="fill:rgb(252,96,91);"/><circle cx="3.12" cy="4.8" r="0.36" style="fill:rgb(252,187,64);"/><circle cx="4.32" cy="4.8" r="0.36" style="fill:rgb(50,195,71);"/>""");

    public static readonly IconValue Plus = new("""<path d="M5 12h14"/><path d="M12 5v14"/>""");

    public static readonly IconValue Minus = new("""<path d="M5 12h14"/>""");

    public static readonly IconValue X = new("""<path d="M18 6 6 18"/><path d="m6 6 12 12"/>""");

    public static readonly IconValue Check = new("""<path d="M20 6 9 17l-5-5"/>""");

    public static readonly IconValue Ellipsis = new("""<circle cx="12" cy="12" r="1"/><circle cx="19" cy="12" r="1"/><circle cx="5" cy="12" r="1"/>""");

    public static readonly IconValue Settings = new("""<path d="M12.22 2h-.44a2 2 0 0 0-2 2v.18a2 2 0 0 1-1 1.73l-.43.25a2 2 0 0 1-2 0l-.15-.08a2 2 0 0 0-2.73.73l-.22.38a2 2 0 0 0 .73 2.73l.15.1a2 2 0 0 1 1 1.72v.51a2 2 0 0 1-1 1.74l-.15.09a2 2 0 0 0-.73 2.73l.22.38a2 2 0 0 0 2.73.73l.15-.08a2 2 0 0 1 2 0l.43.25a2 2 0 0 1 1 1.73V20a2 2 0 0 0 2 2h.44a2 2 0 0 0 2-2v-.18a2 2 0 0 1 1-1.73l.43-.25a2 2 0 0 1 2 0l.15.08a2 2 0 0 0 2.73-.73l.22-.39a2 2 0 0 0-.73-2.73l-.15-.08a2 2 0 0 1-1-1.74v-.5a2 2 0 0 1 1-1.74l.15-.09a2 2 0 0 0 .73-2.73l-.22-.38a2 2 0 0 0-2.73-.73l-.15.08a2 2 0 0 1-2 0l-.43-.25a2 2 0 0 1-1-1.73V4a2 2 0 0 0-2-2z"/><circle cx="12" cy="12" r="3"/>""");

    public static readonly IconValue Package = new("""<path d="m7.5 4.27 9 5.15"/><path d="M21 8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16Z"/><path d="m3.3 7 8.7 5 8.7-5"/><path d="M12 22V12"/>""");

    public static readonly IconValue Glossary = new("""<path d="m3 10 2.5-2.5L3 5"/><path d="m3 19 2.5-2.5L3 14"/><path d="M10 6h11"/><path d="M10 12h11"/><path d="M10 18h11"/>""");

    public static readonly IconValue Binary = new("""<rect x="14" y="14" width="4" height="6" rx="2"/><rect x="6" y="4" width="4" height="6" rx="2"/><path d="M6 20h4"/><path d="M14 10h4"/><path d="M6 14h2v6"/><path d="M14 4h2v6"/>""");
    #endregion

    #region arrows
    public static readonly IconValue ChevronUp = new("""<path d="m18 15-6-6-6 6"/>""");

    public static readonly IconValue ChevronDown = new("""<path d="m6 9 6 6 6-6"/>""");

    public static readonly IconValue ChevronLeft = new("""<path d="m15 18-6-6 6-6"/>""");

    public static readonly IconValue ChevronRight = new("""<path d="m9 18 6-6-6-6"/>""");

    public static readonly IconValue ArrowUp = new("""<path d="m5 12 7-7 7 7"/><path d="M12 19V5"/>""");

    public static readonly IconValue ArrowDown = new("""<path d="M12 5v14"/><path d="m19 12-7 7-7-7"/>""");

    public static readonly IconValue ArrowLeft = new("""<path d="m12 19-7-7 7-7"/><path d="M19 12H5"/>""");

    public static readonly IconValue ArrowRight = new("""<path d="M5 12h14"/><path d="m12 5 7 7-7 7"/>""");

    public static readonly IconValue ArrowUpDown = new("""<path d="m21 16-4 4-4-4"/><path d="M17 20V4"/><path d="m3 8 4-4 4 4"/><path d="M7 4v16"/>""");

    public static readonly IconValue ArrowDownUp = new("""<path d="m3 16 4 4 4-4"/><path d="M7 20V4"/><path d="m21 8-4-4-4 4"/><path d="M17 4v16"/>""");

    public static readonly IconValue ArrowLeftRight = new("""<path d="M8 3 4 7l4 4"/><path d="M4 7h16"/><path d="m16 21 4-4-4-4"/><path d="M20 17H4"/>""");

    public static readonly IconValue ArrowRightLeft = new("""<path d="m16 3 4 4-4 4"/><path d="M20 7H4"/><path d="m8 21-4-4 4-4"/><path d="M4 17h16"/>""");

    public static readonly IconValue SplitArrowHorizontal = new("""<path d="M16 12h6"/><path d="M8 12H2"/><path d="M12 2v2"/><path d="M12 8v2"/><path d="M12 14v2"/><path d="M12 20v2"/><path d="m19 15 3-3-3-3"/><path d="m5 9-3 3 3 3"/>""");

    public static readonly IconValue SplitArrowVertical = new("""<path d="M12 22v-6"/><path d="M12 8V2"/><path d="M4 12H2"/><path d="M10 12H8"/><path d="M16 12h-2"/><path d="M22 12h-2"/><path d="m15 19-3 3-3-3"/><path d="m15 5-3-3-3 3"/>""");
    #endregion

    #region status
    public static readonly IconValue Success = new("""<circle cx="12" cy="12" r="10"/><path d="m9 12 2 2 4-4"/>""");

    public static readonly IconValue Info = new("""<circle cx="12" cy="12" r="10"/><path d="M12 16v-4"/><path d="M12 8h.01"/>""");

    public static readonly IconValue Warning = new("""<path d="m21.73 18-8-14a2 2 0 0 0-3.48 0l-8 14A2 2 0 0 0 4 21h16a2 2 0 0 0 1.73-3"/><path d="M12 9v4"/><path d="M12 17h.01"/>""");

    public static readonly IconValue Error = new("""<path d="m15 9-6 6"/><path d="M2.586 16.726A2 2 0 0 1 2 15.312V8.688a2 2 0 0 1 .586-1.414l4.688-4.688A2 2 0 0 1 8.688 2h6.624a2 2 0 0 1 1.414.586l4.688 4.688A2 2 0 0 1 22 8.688v6.624a2 2 0 0 1-.586 1.414l-4.688 4.688a2 2 0 0 1-1.414.586H8.688a2 2 0 0 1-1.414-.586z"/><path d="m9 9 6 6"/>""");

    public static readonly IconValue Unknown = new("""<circle cx="12" cy="12" r="10"/><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"/><path d="M12 17h.01"/>""");
    #endregion

    #region operations
    public static readonly IconValue Open = new("""<path d="M21 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h6"/><path d="m21 3-9 9"/><path d="M15 3h6v6"/>""");

    public static readonly IconValue Save = new("""<path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"/><polyline points="17 21 17 13 7 13 7 21"/><polyline points="7 3 7 8 15 8"/>""");

    public static readonly IconValue SaveAll = new("""<path d="M6 4a2 2 0 0 1 2-2h10l4 4v10.2a2 2 0 0 1-2 1.8H8a2 2 0 0 1-2-2Z"/><path d="M10 2v4h6"/><path d="M18 18v-7h-8v7"/><path d="M18 22H4a2 2 0 0 1-2-2V6"/>""");

    public static readonly IconValue Edit = new("""<path d="M17 3a2.85 2.83 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5Z"/><path d="m15 5 4 4"/>""");

    public static readonly IconValue Copy = new("""<rect width="14" height="14" x="8" y="8" rx="2" ry="2"/><path d="M4 16c-1.1 0-2-.9-2-2V4c0-1.1.9-2 2-2h10c1.1 0 2 .9 2 2"/>""");

    public static readonly IconValue Undo = new("""<path d="M9 14 4 9l5-5"/><path d="M4 9h10.5a5.5 5.5 0 0 1 5.5 5.5v0a5.5 5.5 0 0 1-5.5 5.5H11"/>""");

    public static readonly IconValue Reload = new("""<path d="M3 12a9 9 0 0 1 9-9 9.75 9.75 0 0 1 6.74 2.74L21 8"/><path d="M21 3v5h-5"/><path d="M21 12a9 9 0 0 1-9 9 9.75 9.75 0 0 1-6.74-2.74L3 16"/><path d="M8 16H3v5"/>""");

    public static readonly IconValue Delete = new("""<path d="M3 6h18"/><path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"/><path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"/><line x1="10" x2="10" y1="11" y2="17"/><line x1="14" x2="14" y1="11" y2="17"/>""");

    public static readonly IconValue ImportFile = new("""<path d="M4 22h14a2 2 0 0 0 2-2V7l-5-5H6a2 2 0 0 0-2 2v4"/><path d="M14 2v4a2 2 0 0 0 2 2h4"/><path d="M2 15h10"/><path d="m9 18 3-3-3-3"/>""");

    public static readonly IconValue ExportFile = new("""<path d="M14 2v4a2 2 0 0 0 2 2h4"/><path d="M4 7V4a2 2 0 0 1 2-2 2 2 0 0 0-2 2"/><path d="M4.063 20.999a2 2 0 0 0 2 1L18 22a2 2 0 0 0 2-2V7l-5-5H6"/><path d="m5 11-3 3"/><path d="m5 17-3-3h10"/>""");

    public static readonly IconValue ImportFolder = new("""<path d="M2 9V5a2 2 0 0 1 2-2h3.9a2 2 0 0 1 1.69.9l.81 1.2a2 2 0 0 0 1.67.9H20a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2v-1"/><path d="M2 13h10"/><path d="m9 16 3-3-3-3"/>""");

    public static readonly IconValue ExportFolder = new("""<path d="M2 7.5V5a2 2 0 0 1 2-2h3.9a2 2 0 0 1 1.69.9l.81 1.2a2 2 0 0 0 1.67.9H20a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H4a2 2 0 0 1-2-1.5"/><path d="M2 13h10"/><path d="m5 10-3 3 3 3"/>""");
    #endregion

    #region file types
    public static readonly IconValue File = new("""<path d="M15 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7Z"/><path d="M14 2v4a2 2 0 0 0 2 2h4"/>""");

    public static readonly IconValue Files = new("""<path d="M20 7h-3a2 2 0 0 1-2-2V2"/><path d="M9 18a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h7l4 4v10a2 2 0 0 1-2 2Z"/><path d="M3 7.6v12.8A1.6 1.6 0 0 0 4.6 22h9.8"/>""");

    public static readonly IconValue Folder = new("""<path d="M20 20a2 2 0 0 0 2-2V8a2 2 0 0 0-2-2h-7.9a2 2 0 0 1-1.69-.9L9.6 3.9A2 2 0 0 0 7.93 3H4a2 2 0 0 0-2 2v13a2 2 0 0 0 2 2Z"/>""");

    public static readonly IconValue Folders = new("""<path d="M20 17a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-3.9a2 2 0 0 1-1.69-.9l-.81-1.2a2 2 0 0 0-1.67-.9H8a2 2 0 0 0-2 2v9a2 2 0 0 0 2 2Z"/><path d="M2 8v11a2 2 0 0 0 2 2h14"/>""");

    public static readonly IconValue Archive = new("""<circle cx="15" cy="19" r="2"/><path d="M20.9 19.8A2 2 0 0 0 22 18V8a2 2 0 0 0-2-2h-7.9a2 2 0 0 1-1.69-.9L9.6 3.9A2 2 0 0 0 7.93 3H4a2 2 0 0 0-2 2v13a2 2 0 0 0 2 2h5.1"/><path d="M15 11v-1"/><path d="M15 17v-2"/>""");

    public static readonly IconValue TextFile = new("""<path d="M15 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7Z"/><path d="M14 2v4a2 2 0 0 0 2 2h4"/><path d="M10 9H8"/><path d="M16 13H8"/><path d="M16 17H8"/>""");

    public static readonly IconValue ConfigFile = new("""<path d="M14 2v4a2 2 0 0 0 2 2h4"/><path d="m3.2 12.9-.9-.4"/><path d="m3.2 15.1-.9.4"/><path d="M4.677 21.5a2 2 0 0 0 1.313.5H18a2 2 0 0 0 2-2V7l-5-5H6a2 2 0 0 0-2 2v2.5"/><path d="m4.9 11.2-.4-.9"/><path d="m4.9 16.8-.4.9"/><path d="m7.5 10.3-.4.9"/><path d="m7.5 17.7-.4-.9"/><path d="m9.7 12.5-.9.4"/><path d="m9.7 15.5-.9-.4"/><circle cx="6" cy="14" r="3"/>""");

    public static readonly IconValue BinaryFile = new("""<path d="M4 22h14a2 2 0 0 0 2-2V7l-5-5H6a2 2 0 0 0-2 2v4"/><path d="M14 2v4a2 2 0 0 0 2 2h4"/><rect width="4" height="6" x="2" y="12" rx="2"/><path d="M10 12h2v6"/><path d="M10 18h4"/>""");

    public static readonly IconValue ErrorFile = new("""<path d="M15 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7Z"/><path d="M14 2v4a2 2 0 0 0 2 2h4"/><path d="m14.5 12.5-5 5"/><path d="m9.5 12.5 5 5"/>""");

    public static readonly IconValue UnknownFile = new("""<path d="M14.5 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7.5L14.5 2z"/><path d="M10 10.3c.2-.4.5-.8.9-1a2.1 2.1 0 0 1 2.6.4c.3.4.5.8.5 1.3 0 1.3-2 2-2 2"/><path d="M12 17h.01"/>""");
    #endregion
}