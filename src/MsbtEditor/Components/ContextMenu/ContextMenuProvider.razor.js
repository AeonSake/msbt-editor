﻿const positions = ['bottom-start', 'bottom', 'bottom-end', 'top-start', 'top', 'top-end'];
let _dotNetReference = null;
let _containerReference = null;
let _targetReference = null;
let _popperInstance = null;

const _virtualElement = {
    lastPosX: 0,
    lastPosY: 0,
    getBoundingClientRect: () => ({
        width: 0,
        height: 0,
        top: _virtualElement.lastPosY,
        bottom: _virtualElement.lastPosY,
        left: _virtualElement.lastPosX,
        right: _virtualElement.lastPosX
    })
};

export function setup(dotNetReference) {
    _dotNetReference = dotNetReference;

    document.addEventListener('mousedown', onMouseDown, true);
    document.addEventListener('touchstart', onMouseDown, true);
    document.addEventListener('scroll', onScroll, true);
}

function onMouseDown(e) {
    if (_containerReference && _containerReference.contains(e.target)) return;

    if (_dotNetReference && _popperInstance) _dotNetReference.invokeMethodAsync('OnClose');
    _virtualElement.lastPosX = e.clientX;
    _virtualElement.lastPosY = e.clientY;
}

function onScroll(e) {
    if (!_popperInstance || _containerReference && _containerReference.contains(e.target)) return;

    _dotNetReference.invokeMethodAsync('OnClose');
}

export function open(container, target, position, targetAsPosition = true) {
    if (target instanceof HTMLElement) {
        _targetReference = target;
        target.classList.add('context-menu');
    } else target = _virtualElement;

    _containerReference = container;

    setTimeout(function() {
        _popperInstance = Popper.createPopper(targetAsPosition ? target : _virtualElement, container, {
            placement: position < 0 || position > positions.length ? positions[0] : positions[position]
        });
        _containerReference.style.display = 'block';
    }, 1);
}

export function close() {
    if (!_popperInstance) return;

    _containerReference.style.display = 'none';
    _containerReference = null;

    if (_targetReference) _targetReference.classList.remove('context-menu');
    _targetReference = null;

    _popperInstance.destroy();
    _popperInstance = null;
}