﻿export function setup(element) {
    if (!(element instanceof HTMLElement)) return;

    element.removeEventListener('wheel', handleScroll);
    element.addEventListener('wheel', handleScroll);
}

function handleScroll(e) {
    if (!e.deltaY) return;

    e.currentTarget.scrollLeft += e.deltaY + e.deltaX;
    e.preventDefault();
}