﻿export function scrollTo(element) {
    if (!(element instanceof HTMLElement)) return;

    element.scrollIntoView();
}