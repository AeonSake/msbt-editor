﻿namespace MsbtEditor;

internal static class CommonPaths
{
    #region private members
    private static readonly object Lock = new();
    private static int _tempIndex = -1;
    #endregion

    #region public fields
    public static readonly string AppRoot = Path.GetDirectoryName(Application.ExecutablePath)!;

    public static readonly string MapFolder = Path.Combine(AppRoot, "maps");

    public static readonly string ConfigFolder = Path.Combine(AppRoot, "configs");

    public static readonly string TempFolder = Path.Combine(AppRoot, "temp");

    public static readonly string LogsFolder = Path.Combine(AppRoot, "logs");

    public static readonly string UpdateFolder = Path.Combine(AppRoot, "update");

    public static readonly string SettingsFile = Path.Combine(AppRoot, "settings.json");
    #endregion

    #region public methods
    public static DirectoryInfo CreateTempDir()
    {
        lock (Lock)
        {
            string dirName;
            do
            {
                dirName = Path.Combine(TempFolder, $"{++_tempIndex:D3}");
            }
            while (Directory.Exists(dirName));

            return Directory.CreateDirectory(dirName);
        }
    }
    #endregion
}