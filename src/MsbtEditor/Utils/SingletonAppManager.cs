﻿using System.Diagnostics;
using System.IO.Pipes;
using System.Text;

namespace MsbtEditor;

internal sealed class SingletonAppManager(Action<string[]> handleData)
{
    #region private members
    private static readonly Process CurrentProcess = Process.GetCurrentProcess();
    private static readonly CancellationTokenSource MessageServerTokenSource = new();
    #endregion

    #region public methods
    public void Run(Action<string[]> mainLoop, string[] args)
    {
        if (CheckProcess()) //already running
        {
            SendMessage(args);
        }
        else
        {
            StartMessageServer();
            mainLoop.Invoke(args);
            MessageServerTokenSource.Cancel();
        }
    }
    #endregion

    #region private methods
    private static bool CheckProcess()
    {
        foreach (var process in Process.GetProcesses())
        {
            if (process.Id == CurrentProcess.Id) continue;
            if (process.ProcessName.Equals(CurrentProcess.ProcessName, StringComparison.OrdinalIgnoreCase)) return true;
        }
        return false;
    }

    private static void SendMessage(string[] args)
    {
        try
        {
            var data = Encoding.UTF8.GetBytes(string.Join('\0', args));
            var clientPipe = new NamedPipeClientStream(CurrentProcess.ProcessName);
            clientPipe.Connect(1000);
            clientPipe.Write(data);
            clientPipe.Close();
        }
        catch
        {
            //ignored
        }
    }

    private void StartMessageServer()
    {
        var token = MessageServerTokenSource.Token;
        Task.Run(async () =>
        {
            while (!token.IsCancellationRequested)
            {
                try
                {
                    await using var serverPipe = new NamedPipeServerStream(CurrentProcess.ProcessName);
                    await serverPipe.WaitForConnectionAsync(token);

                    await using var messageStream = new MemoryStream();
                    await serverPipe.CopyToAsync(messageStream, token);
                    var message = messageStream.ToArray();
                    serverPipe.Disconnect();
                    serverPipe.Close();

                    var args = message.Length > 0 ? Encoding.UTF8.GetString(message).Split('\0') : [];
                    handleData.Invoke(args);
                }
                catch
                {
                    //ignored
                }
            }
        }, token);
    }
    #endregion
}