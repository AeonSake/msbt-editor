namespace MsbtEditor;

internal static class FolderBuilder
{
    public static List<FileData> Wrap(IEnumerable<FileData> files, string rootDir)
    {
        rootDir = rootDir.Replace('\\', '/');

        var folders = new Dictionary<string, FolderFileData>();
        var rootFiles = new List<FileData>();
        foreach (var file in files)
        {
            var fileDir = Path.GetDirectoryName(file.InternalFilePath)!.Replace('\\', '/');

            FolderFileData? lastFolder = null;
            var index = rootDir.Length;
            while (index < fileDir.Length)
            {
                index = fileDir.IndexOf('/', index + 1);
                if (index < 0) index = fileDir.Length;

                var subDir = fileDir[..index];

                if (folders.TryGetValue(subDir, out var foundFolder))
                {
                    lastFolder = foundFolder;
                    continue;
                }

                var folder = new FolderFileData(subDir, [], true);
                folders[subDir] = folder;
                lastFolder?.Add(folder);
                lastFolder = folder;
            }

            if (lastFolder is null) rootFiles.Add(file);
            else lastFolder.Add(file);
        }

        var wrappedFiles = new List<FileData>();
        foreach (var (path, folder) in folders)
        {
            if (path[(rootDir.Length + 1)..].Contains('/')) continue;
            wrappedFiles.Add(folder);
        }
        wrappedFiles.AddRange(rootFiles);
        return wrappedFiles;
    }

    public static Dictionary<string, FileData> BuildMap(IEnumerable<FileData> files, bool containerAsFolder = false)
    {
        var map = new Dictionary<string, FileData>();
        MapFiles(files);
        return map;

        void MapFiles(IEnumerable<FileData> childFiles)
        {
            foreach (var childFile in childFiles)
            {
                if (childFile is FolderFileData folder) MapFiles(folder.Children);
                else if (containerAsFolder && childFile is ContainerFileData container) MapFiles(container.Children);
                else map.Add(childFile.InternalFilePath, childFile);
            }
        }
    }

    public static Dictionary<string, FileData> MatchPaths(ContainerFileData container, string basePath, IEnumerable<string> filePaths, Func<FileData, string, bool> fileMatch, bool containerAsFolder = false)
    {
        var map = new Dictionary<string, FileData>();
        var filePathArr = filePaths.ToArray();
        MapFiles(container);
        return map;

        void MapFiles(IEnumerable<FileData> childFiles)
        {
            foreach (var childFile in childFiles)
            {
                if (childFile is FolderFileData folder) MapFiles(folder.Children);
                else if (containerAsFolder && childFile is ContainerFileData container1) MapFiles(container1.Children);
                else
                {
                    var relativePath = Path.GetRelativePath(container.GetFilePath(), childFile.GetFilePath());
                    var relativeFolder = Path.GetDirectoryName(relativePath) ?? string.Empty;
                    foreach (var filePath in filePathArr)
                    {
                        var relativeSourcePath = Path.GetRelativePath(basePath, filePath);
                        var mappedFolder = Path.GetDirectoryName(relativeSourcePath);

                        if (!Equals(mappedFolder, relativeFolder) || !fileMatch(childFile, Path.GetFileName(relativeSourcePath))) continue;
                        map.Add(filePath, childFile);
                        break;
                    }
                }
            }
        }
    }
}