﻿using System.Text;
using System.Text.RegularExpressions;
using NintendoTools.FileFormats;
using NintendoTools.FileFormats.Bmg;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.Utils;

namespace MsbtEditor;

internal static partial class TextConverter
{
    #region private members
    private static readonly IMsbtFormatProvider MsbtFormatProvider = new MsbtFormatProvider();
    private static readonly IBmgFormatProvider BmgFormatProvider = new BmgFormatProvider();
    private static readonly HeaderParameter<MsbtFile>[] MsbtHeaderParams = GetMsbtHeaderParams();
    private static readonly HeaderParameter<BmgFile>[] BmgHeaderParams = GetBmgHeaderParams();
    private static readonly HeaderParameter<MsbtMessage>[] MsbtMessageHeaderParams = GetMsbtMessageHeaderParams();
    private static readonly HeaderParameter<BmgMessage>[] BmgMessageHeaderParams = GetBmgMessageHeaderParams();
    #endregion

    #region public methods
    public static string SerializeMsbt(MsbtFile file, MsbtDynamicTagMap map)
    {
        var content = new StringBuilder();

        //msbt header
        content.AppendLine("%%%");
        content.Append("bigEndian: ").AppendLine(file.BigEndian.ToString().ToLowerInvariant());
        content.Append("version: ").AppendLine(file.Version.ToString());
        content.Append("encoding: ").AppendLine(file.EncodingType.SerializeEncoding());
        content.Append("hasNLI1: ").AppendLine(file.HasNli1.ToString().ToLowerInvariant());
        content.Append("hasLBL1: ").AppendLine(file.HasLbl1.ToString().ToLowerInvariant());
        if (file.HasLbl1) content.Append("labelGroups: ").AppendLine(file.LabelGroups.ToString());
        content.Append("hasATR1: ").AppendLine(file.HasAtr1.ToString().ToLowerInvariant());
        if (file.AdditionalAttributeData.Length > 0) content.Append("additionalAttributeData: ").AppendLine(file.AdditionalAttributeData.ToHexString(true));
        content.Append("hasATO1: ").AppendLine(file.HasAto1.ToString().ToLowerInvariant());
        if (file.HasAto1) content.Append("ATO1Data: ").AppendLine(file.Ato1Data.ToHexString(true));
        content.Append("hasTSY1: ").AppendLine(file.HasTsy1.ToString().ToLowerInvariant());
        content.Append("hasTXTW: ").AppendLine(file.HasTxtW.ToString().ToLowerInvariant());
        content.AppendLine("%%%");

        foreach (var message in file.Messages)
        {
            //message header
            content.AppendLine();
            content.AppendLine("---");
            if (file.HasNli1) content.Append("id: ").AppendLine(message.Id.ToString());
            if (file.HasLbl1) content.Append("label: ").AppendLine(message.Label);
            if (file.HasAtr1)
            {
                if (message.AttributeText is not null) content.Append("attributeText: ").AppendLine(message.AttributeText);
                else content.Append("attribute: ").AppendLine(message.Attribute.ToHexString(true));
            }
            if (file.HasTsy1) content.Append("styleIndex: ").AppendLine(message.StyleIndex.ToString());
            content.AppendLine("---");

            //message content
            foreach (var line in message.ToCompiledString(map, MsbtFormatProvider, file.BigEndian, file.Encoding).Split('\n'))
            {
                if (line == "---") content.AppendLine("{{---}}");
                else if (line == "{{---}}") content.AppendLine("{{{{---}}}}");
                else content.AppendLine(line);
            }
        }

        return content.ToString();
    }

    public static MsbtFile DeserializeMsbt(string content, MsbtDynamicTagMap map)
    {
        var file = new MsbtFile {Version = 3};

        var lines = content.Split('\n');
        var i = 0;

        //parse file header
        ParseHeaderData(lines, ref i, MsbtHeaderParams, file, HeaderDelimiterRegex());

        //parse messages
        for (; i < lines.Length; ++i)
        {
            var message = new MsbtMessage();
            ParseHeaderData(lines, ref i, MsbtMessageHeaderParams, message, MessageDelimiterRegex());
            if (i >= lines.Length) break;
            if (file.HasLbl1 && string.IsNullOrEmpty(message.Label)) throw new FileFormatException($"Message is missing label value on line {i+1}.");

            ParseMsbtMessageBody(lines, ref i, message, file.BigEndian, file.Encoding, map);
            file.Messages.Add(message);
        }

        return file;
    }

    public static string SerializeBmg(BmgFile file, BmgDynamicTagMap map)
    {
        var content = new StringBuilder();

        //bmg header
        content.AppendLine("%%%");
        content.Append("bigEndian: ").AppendLine(file.BigEndian.ToString().ToLowerInvariant());
        content.Append("bigEndianLabels: ").AppendLine(file.BigEndianLabels.ToString().ToLowerInvariant());
        content.Append("encoding: ").AppendLine(file.EncodingType.SerializeEncoding());
        content.Append("fileId: ").AppendLine(file.FileId.ToString());
        content.Append("defaultColor: ").AppendLine(file.DefaultColor.ToString());
        content.Append("hasMID1: ").AppendLine(file.HasMid1.ToString().ToLowerInvariant());
        if (file.HasMid1) content.Append("MID1Format: ").AppendLine(file.Mid1Format.ToHexString(true));
        content.Append("hasSTR1: ").AppendLine(file.HasStr1.ToString().ToLowerInvariant());
        content.Append("hasFLW1: ").AppendLine(file.HasFlw1.ToString().ToLowerInvariant());
        if (file.HasFlw1)
        {
            content.AppendLine("flowData:");
            content.Append("  nodes: [").AppendJoin(',', file.FlowData!.Nodes.Select(data => data.ToHexString(true))).AppendLine("]");
            content.Append("  labels: [").AppendJoin(',', file.FlowData.Labels.Select(data => data.ToHexString(true))).AppendLine("]");
            content.Append("  indices: [").AppendJoin(',', file.FlowData.Indices.Select(data => data.ToHexString(true))).AppendLine("]");
        }
        content.AppendLine("%%%");

        foreach (var message in file.Messages)
        {
            //message header
            content.AppendLine();
            content.AppendLine("---");
            if (file.HasMid1) content.Append("id: ").AppendLine(message.Id.ToString());
            if (file.HasStr1) content.Append("label: ").AppendLine(message.Label);
            content.Append("attribute: ").AppendLine(message.Attribute.ToHexString(true));
            content.AppendLine("---");

            //message content
            foreach (var line in message.ToCompiledString(map, BmgFormatProvider, file.BigEndian, file.Encoding).Split('\n'))
            {
                if (line == "---") content.AppendLine("{{---}}");
                else if (line == "{{---}}") content.AppendLine("{{{{---}}}}");
                else content.AppendLine(line);
            }
        }

        return content.ToString();
    }

    public static BmgFile DeserializeBmg(string content, BmgDynamicTagMap map)
    {
        var file = new BmgFile();

        var lines = content.Split('\n');
        var i = 0;

        //parse file header
        ParseHeaderData(lines, ref i, BmgHeaderParams, file, HeaderDelimiterRegex());

        //parse messages
        for (; i < lines.Length; ++i)
        {
            var message = new BmgMessage();
            ParseHeaderData(lines, ref i, BmgMessageHeaderParams, message, MessageDelimiterRegex());
            if (i >= lines.Length) break;
            if (file.HasStr1 && string.IsNullOrEmpty(message.Label)) throw new FileFormatException($"Message is missing label value on line {i+1}.");

            ParseBmgMessageBody(lines, ref i, message, file.BigEndian, file.Encoding, map);
            file.Messages.Add(message);
        }

        return file;
    }
    #endregion

    #region private methods
    private static HeaderParameter<MsbtFile>[] GetMsbtHeaderParams() =>
    [
        new()
        {
            Name = "bigEndian",
            Parser = (file, value, name, line) => file.BigEndian = ParseBool(value, name, line)
        },
        new()
        {
            Name = "version",
            Parser = (file, value, name, line) => file.Version = ParseInt(value, name, line)
        },
        new()
        {
            Name = "encoding",
            Parser = (file, value, name, line) => file.EncodingType = ParseEncoding(value, name, line)
        },
        new()
        {
            Name = "hasNLI1",
            Parser = (file, value, name, line) => file.HasNli1 = ParseBool(value, name, line)
        },
        new()
        {
            Name = "hasLBL1",
            Parser = (file, value, name, line) => file.HasLbl1 = ParseBool(value, name, line)
        },
        new()
        {
            Name = "labelGroups",
            Parser = (file, value, name, line) => file.LabelGroups = ParseInt(value, name, line)
        },
        new()
        {
            Name = "hasATR1",
            Parser = (file, value, name, line) => file.HasAtr1 = ParseBool(value, name, line)
        },
        new()
        {
            Name = "additionalAttributeData",
            Parser = (file, value, name, line) => file.AdditionalAttributeData = ParseHexString(value, name, line)
        },
        new()
        {
            Name = "hasATO1",
            Parser = (file, value, name, line) => file.HasAto1 = ParseBool(value, name, line)
        },
        new()
        {
            Name = "ATO1Data",
            Parser = (file, value, name, line) => file.Ato1Data = ParseHexString(value, name, line)
        },
        new()
        {
            Name = "hasTSY1",
            Parser = (file, value, name, line) => file.HasTsy1 = ParseBool(value, name, line)
        },
        new()
        {
            Name = "hasTXTW",
            Parser = (file, value, name, line) => file.HasTxtW = ParseBool(value, name, line)
        }
    ];

    private static HeaderParameter<BmgFile>[] GetBmgHeaderParams() =>
    [
        new()
        {
            Name = "bigEndian",
            Parser = (file, value, name, line) => file.BigEndian = ParseBool(value, name, line)
        },
        new()
        {
            Name = "bigEndianLabels",
            Parser = (file, value, name, line) => file.BigEndianLabels = ParseBool(value, name, line)
        },
        new()
        {
            Name = "encoding",
            Parser = (file, value, name, line) => file.EncodingType = ParseEncoding(value, name, line)
        },
        new()
        {
            Name = "fileId",
            Parser = (file, value, name, line) => file.FileId = ParseInt(value, name, line)
        },
        new()
        {
            Name = "defaultColor",
            Parser = (file, value, name, line) => file.DefaultColor = ParseInt(value, name, line)
        },
        new()
        {
            Name = "hasMID1",
            Parser = (file, value, name, line) => file.HasMid1 = ParseBool(value, name, line)
        },
        new()
        {
            Name = "MID1Format",
            Parser = (file, value, name, line) => file.Mid1Format = ParseHexString(value, name, line)
        },
        new()
        {
            Name = "hasSTR1",
            Parser = (file, value, name, line) => file.HasStr1 = ParseBool(value, name, line)
        },
        new()
        {
            Name = "hasFLW1",
            Parser = (file, value, name, line) => file.HasFlw1 = ParseBool(value, name, line)
        },
        new()
        {
            Name = "flowData",
            Parser = (file, _, _, _) => file.FlowData = new BmgFlowData(),
            Children = [
                new HeaderParameter<BmgFile>
                {
                    Name = "nodes",
                    Parser = (file, value, name, line) => file.FlowData!.Nodes = ParseHexStringArray(value, name, line)
                },
                new HeaderParameter<BmgFile>
                {
                    Name = "labels",
                    Parser = (file, value, name, line) => file.FlowData!.Labels = ParseHexStringArray(value, name, line)
                },
                new HeaderParameter<BmgFile>
                {
                    Name = "indices",
                    Parser = (file, value, name, line) => file.FlowData!.Indices = ParseHexStringArray(value, name, line)
                }
            ]
        }
    ];

    private static HeaderParameter<MsbtMessage>[] GetMsbtMessageHeaderParams() =>
    [
        new()
        {
            Name = "id",
            Parser = (message, value, name, line) => message.Id = ParseUInt(value, name, line)
        },
        new()
        {
            Name = "label",
            Parser = (message, value, _, _) => message.Label = value
        },
        new()
        {
            Name = "attribute",
            Parser = (message, value, name, line) => message.Attribute = ParseHexString(value, name, line)
        },
        new()
        {
            Name = "attributeText",
            Parser = (message, value, _, _) => message.AttributeText = value
        },
        new()
        {
            Name = "styleIndex",
            Parser = (message, value, name, line) => message.StyleIndex = ParseUInt(value, name, line)
        }
    ];

    private static HeaderParameter<BmgMessage>[] GetBmgMessageHeaderParams() =>
    [
        new()
        {
            Name = "id",
            Parser = (message, value, name, line) => message.Id = ParseUInt(value, name, line)
        },
        new()
        {
            Name = "label",
            Parser = (message, value, _, _) => message.Label = value
        },
        new()
        {
            Name = "attribute",
            Parser = (message, value, name, line) => message.Attribute = ParseHexString(value, name, line)
        }
    ];

    private static string SerializeEncoding(this EncodingType encoding) => encoding switch
    {
        EncodingType.ASCII    => "ascii",
        EncodingType.Latin1   => "latin-1",
        EncodingType.ShiftJIS => "shift-jis",
        EncodingType.UTF8     => "utf-8",
        EncodingType.UTF16    => "utf-16",
        EncodingType.UTF32    => "utf-32",
        _                     => encoding.ToString()
    };

    private static EncodingType ParseEncoding(string value, string parameter, int line) => value.ToLower() switch
    {
        "ascii"                                                            => EncodingType.ASCII,
        "latin-1" or "windows-1252"                                        => EncodingType.Latin1,
        "shift-jis" or "shift_jis"                                         => EncodingType.ShiftJIS,
        "utf-8"                                                            => EncodingType.UTF8,
        "utf-16" or "utf-16le" or "utf-16be" or "unicode" or "unicodefeff" => EncodingType.UTF16,
        "utf-32"                                                           => EncodingType.UTF32,
        _                                                                  => throw new FileFormatException($"Invalid encoding value for \"{parameter}\" found on line {line + 1}.")
    };

    private static void ParseHeaderData<T>(string[] lines, ref int i, HeaderParameter<T>[] parameters, T instance, Regex headerDelimiter)
    {
        var headerStarted = false;
        HeaderParameter<T>? blockParam = null;

        for (; i < lines.Length; ++i)
        {
            var line = lines[i].TrimEnd('\r');

            if (headerDelimiter.IsMatch(line))
            {
                if (headerStarted)
                {
                    ++i;
                    break;
                }
                headerStarted = true;
                continue;
            }

            if (!headerStarted) continue;

            var match = HeaderParameterRegex().Match(line);
            if (!match.Success) continue;

            var paramName = match.Groups[1].Value;
            if (line.StartsWith("  ") && blockParam is not null)
            {
                foreach (var parameter in blockParam.Children)
                {
                    if (!paramName.Equals(parameter.Name, StringComparison.OrdinalIgnoreCase)) continue;
                    parameter.Parser(instance, match.Groups[2].Value, parameter.Name, i);
                    break;
                }
            }
            foreach (var parameter in parameters)
            {
                if (!paramName.Equals(parameter.Name, StringComparison.OrdinalIgnoreCase)) continue;
                parameter.Parser(instance, match.Groups[2].Value, parameter.Name, i);
                blockParam = parameter.Children.Length > 0 ? parameter : null;
                break;
            }
        }
    }

    private static bool ParseBool(string value, string parameter, int line)
    {
        if (bool.TryParse(value, out var val)) return val;
        throw new FileFormatException($"Invalid boolean value for \"{parameter}\" found on line {line+1}.");
    }

    private static int ParseInt(string value, string parameter, int line)
    {
        if (int.TryParse(value, out var val)) return val;
        throw new FileFormatException($"Invalid integer value for \"{parameter}\" found on line {line+1}.");
    }

    private static uint ParseUInt(string value, string parameter, int line)
    {
        if (uint.TryParse(value, out var val)) return val;
        throw new FileFormatException($"Invalid unsigned integer value for \"{parameter}\" found on line {line+1}.");
    }

    private static byte[] ParseHexString(string value, string parameter, int line)
    {
        try
        {
            return DataTypes.HexString.Serialize(value, !BitConverter.IsLittleEndian, Encoding.UTF8);
        }
        catch
        {
            throw new FileFormatException($"Invalid hex string value for \"{parameter}\" found on line {line+1}.");
        }
    }

    private static T[] ParseArray<T>(string value, Func<string, T> itemParser, string parameter, int line)
    {
        if (!value.StartsWith('[') || !value.EndsWith(']')) throw new FileFormatException($"Invalid array value for \"{parameter}\" found on line {line+1}.");

        var values = value.TrimStart('[').TrimEnd(']').Split(',');
        var items = new T[values.Length];
        for (var i = 0; i < values.Length; ++i)
        {
            try
            {
                items[i] = itemParser(values[i].Trim());
            }
            catch
            {
                throw new FileFormatException($"Invalid array item value for \"{parameter}\" found on line {line+1}.");
            }
        }

        return items;
    }

    private static byte[][] ParseHexStringArray(string value, string parameter, int line) => ParseArray(value, str => DataTypes.HexString.Serialize(str, !BitConverter.IsLittleEndian, Encoding.UTF8), parameter, line);

    private static void ParseMsbtMessageBody(string[] lines, ref int i, MsbtMessage message, bool bigEndian, Encoding encoding, MsbtDynamicTagMap map)
    {
        var encodingWidth = encoding.GetByteCount("\0");
        var nullStringPadding = new byte[encodingWidth];

        for (; i < lines.Length; ++i)
        {
            var text = lines[i].TrimEnd('\r');

            if (MessageDelimiterRegex().IsMatch(text))
            {
                --i;
                break;
            }

            //handle escaped text
            if (text == "{{---}}") text = "---";
            else if (text == "{{{{---}}}}") text = "{{---}}";
            else
            {
                foreach (Match tagMatch in TagRegex().Matches(text))
                {
                    var tag = new MsbtTag();

                    var tagName = tagMatch.Groups[1].Value;
                    var argsFound = !string.IsNullOrWhiteSpace(tagMatch.Groups[2].Value);
                    if (map.TryGetInfo(tagName, out var tagInfo))
                    {
                        tag.Group = tagInfo.Group;
                        tag.Type = tagInfo.Type ?? 0;

                        if (tagInfo.HasDiscard || tagInfo.TypeList.Count > 0 || tagInfo.TypeMap.Count > 0)
                        {
                            var parts = tagName.Split(':');
                            if (parts.Length != 2) throw new FileFormatException($"Invalid tag name format on line {i+1}. Tag requires type value.");

                            if (tagInfo.HasDiscard)
                            {
                                if (ushort.TryParse(parts[1], out var type)) tag.Type = type;
                                else throw new FileFormatException($"Invalid tag name format on line {i+1}. Tag is defined as discard type but the type value could not be parsed as u16.");
                            }
                            else if (tagInfo.TypeList.Count > 0)
                            {
                                if (ushort.TryParse(parts[1], out var type)) tag.Type = type;
                                else throw new FileFormatException($"Invalid tag name format on line {i+1}. Tag is defined with a type-range but the type value could not be parsed as u16.");
                            }
                            else
                            {
                                if (tagInfo.TypeMap.TryGetValue(parts[1], out var valueInfo)) tag.Type = ushort.Parse(valueInfo.Value);
                                else throw new FileFormatException($"Invalid tag name format on line {i+1}. \"{parts[1]}\" is not a valid value in the defined value map.");
                            }
                        }

                        if (argsFound)
                        {
                            var argLength = 0;
                            var args = new List<byte[]>();

                            var parsedArgs = ParseTagArguments(tagMatch.Groups[2].Value);
                            var handledArgs = new bool[parsedArgs.Length];
                            DataType? lastType = null;
                            foreach (var argInfo in tagInfo.Arguments)
                            {
                                //argument padding
                                if (argInfo.IsPadding)
                                {
                                    var bytes = ((PaddingDataType) argInfo.DataType).PaddingValue;

                                    if (argInfo.ArrayLength > 0)
                                    {
                                        var arrBytes = new byte[bytes.Length * argInfo.ArrayLength];

                                        for (var j = 0; j < argInfo.ArrayLength; ++j)
                                        {
                                            Buffer.BlockCopy(bytes, 0, arrBytes, j * bytes.Length, bytes.Length);
                                        }

                                        argLength += arrBytes.Length;
                                        args.Add(arrBytes);
                                    }
                                    else
                                    {
                                        argLength += bytes.Length;
                                        args.Add(bytes);
                                    }

                                    lastType = argInfo.DataType;
                                    continue;
                                }

                                //find parsed argument
                                string? argValue = null;
                                for (var j = 0; j < parsedArgs.Length; ++j)
                                {
                                    if (argInfo.Name.Equals(parsedArgs[j].Item1, StringComparison.OrdinalIgnoreCase))
                                    {
                                        argValue = parsedArgs[j].Item2;
                                        handledArgs[j] = true;
                                        break;
                                    }
                                }
                                if (argValue is null) throw new FileFormatException($"Missing argument value for {argInfo.Name} on line {i+1}.");

                                //add null-string padding
                                if (lastType == DataTypes.NullString)
                                {
                                    argLength += encodingWidth;
                                    args.Add(nullStringPadding);
                                }

                                //add encoding padding
                                if (argInfo.DataType == DataTypes.String)
                                {
                                    var paddingLength = (-argLength % encodingWidth + encodingWidth) % encodingWidth;
                                    if (paddingLength > 0)
                                    {
                                        var bytes = new byte[paddingLength];
                                        Array.Fill(bytes, (byte) 0xCD);

                                        argLength += paddingLength;
                                        args.Add(bytes);
                                    }
                                }

                                //handle arrays
                                if (argInfo.ArrayLength > 0)
                                {
                                    var entries = argValue.TrimStart('[').TrimEnd(']').Split(',');
                                    if (argInfo.ArrayLength != entries.Length) throw new FileFormatException($"Argument array lengths do not match on line {i+1}. Expected an array of length {argInfo.ArrayLength} but found an array of length {entries.Length}.");

                                    foreach (var arrValue in entries)
                                    {
                                        var bytes = ParseKnownArgumentValue(arrValue.Trim(), argInfo, bigEndian, encoding, i);
                                        argLength += bytes.Length;
                                        args.Add(bytes);
                                    }
                                }
                                else
                                {
                                    var bytes = ParseKnownArgumentValue(argValue, argInfo, bigEndian, encoding, i);
                                    argLength += bytes.Length;
                                    args.Add(bytes);
                                }

                                lastType = argInfo.DataType;
                            }

                            //append all remaining undefined arguments
                            for (var j = 0; j < parsedArgs.Length; ++j)
                            {
                                if (handledArgs[j]) continue;
                                var bytes = ParseArgumentValue(parsedArgs[j].Item2, DataTypes.HexString, bigEndian, Encoding.UTF8, parsedArgs[j].Item1, i);
                                argLength += bytes.Length;
                                args.Add(bytes);
                            }

                            //combine all arguments into single byte array
                            var argBytes = new byte[argLength];
                            var argIndex = 0;
                            foreach (var arg in args)
                            {
                                Buffer.BlockCopy(arg, 0, argBytes, argIndex, arg.Length);
                                argIndex += arg.Length;
                            }

                            tag.Args = argBytes;
                        }
                    }
                    else if (tagName.IndexOf(':') > -1)
                    {
                        var tagParts = tagName.Split(':');
                        if (!ushort.TryParse(tagParts[0], out var group)) throw new FileFormatException($"Invalid tag format value for group index found on line {i+1}.");
                        if (!ushort.TryParse(tagParts[1], out var type)) throw new FileFormatException($"Invalid tag format value for type index found on line {i+1}.");
                        tag.Group = group;
                        tag.Type = type;

                        if (argsFound)
                        {
                            var argParts = ParseTagArguments(tagMatch.Groups[2].Value);
                            if (argParts.Length > 1) throw new FileFormatException($"Undefined tags can only have a single argument (error on line {i+1}).");
                            tag.Args = ParseHexString(argParts[0].Item2, argParts[0].Item1, i);
                        }
                    }
                    else throw new FileFormatException($"Unknown tag \"{tagName}\" found on line {i+1}. If the tag is not listed in the used tag map, the tag has to follow the format of \"<groupIndex>:<typeIndex>\", example: \"1:2\"");

                    var firstIndex = text.IndexOf(tagMatch.Groups[0].Value, StringComparison.InvariantCulture);
                    text = text[..firstIndex] + "{{" + message.Tags.Count + "}}" + text[(firstIndex + tagMatch.Groups[0].Value.Length)..];
                    message.Tags.Add(tag);
                }
            }

            message.Text += text + "\n";
        }

        if (message.Text.Length == 0) return;
        message.Text = message.Text.EndsWith("\n\n") ? message.Text[..^2] : message.Text[..^1];
    }

    private static void ParseBmgMessageBody(string[] lines, ref int i, BmgMessage message, bool bigEndian, Encoding encoding, BmgDynamicTagMap map)
    {
        var encodingWidth = encoding.GetByteCount("\0");
        var nullStringPadding = new byte[encodingWidth];

        for (; i < lines.Length; ++i)
        {
            var text = lines[i].TrimEnd('\r');

            if (MessageDelimiterRegex().IsMatch(text))
            {
                --i;
                break;
            }

            //handle escaped text
            if (text == "{{---}}") text = "---";
            else if (text == "{{{{---}}}}") text = "{{---}}";
            else
            {
                foreach (Match tagMatch in TagRegex().Matches(text))
                {
                    var tag = new MsbtTag();

                    var tagName = tagMatch.Groups[1].Value;
                    var argsFound = !string.IsNullOrWhiteSpace(tagMatch.Groups[2].Value);
                    if (map.TryGetInfo(tagName, out var tagInfo))
                    {
                        tag.Group = tagInfo.Group;
                        tag.Type = tagInfo.Type ?? 0;

                        if (tagInfo.HasDiscard || tagInfo.TypeList.Count > 0 || tagInfo.TypeMap.Count > 0)
                        {
                            var parts = tagName.Split(':');
                            if (parts.Length != 2) throw new FileFormatException($"Invalid tag name format on line {i+1}. Tag requires type value.");

                            if (tagInfo.HasDiscard)
                            {
                                if (ushort.TryParse(parts[1], out var type)) tag.Type = type;
                                else throw new FileFormatException($"Invalid tag name format on line {i+1}. Tag is defined as discard type but the type value could not be parsed as u16.");
                            }
                            else if (tagInfo.TypeList.Count > 0)
                            {
                                if (ushort.TryParse(parts[1], out var type)) tag.Type = type;
                                else throw new FileFormatException($"Invalid tag name format on line {i+1}. Tag is defined with a type-range but the type value could not be parsed as u16.");
                            }
                            else
                            {
                                if (tagInfo.TypeMap.TryGetValue(parts[1], out var valueInfo)) tag.Type = ushort.Parse(valueInfo.Value);
                                else throw new FileFormatException($"Invalid tag name format on line {i+1}. \"{parts[1]}\" is not a valid value in the defined value map.");
                            }
                        }

                        if (argsFound)
                        {
                            var argLength = 0;
                            var args = new List<byte[]>();

                            var parsedArgs = ParseTagArguments(tagMatch.Groups[2].Value);
                            var handledArgs = new bool[parsedArgs.Length];
                            DataType? lastType = null;
                            foreach (var argInfo in tagInfo.Arguments)
                            {
                                //argument padding
                                if (argInfo.IsPadding)
                                {
                                    var bytes = ((PaddingDataType) argInfo.DataType).PaddingValue;

                                    if (argInfo.ArrayLength > 0)
                                    {
                                        var arrBytes = new byte[bytes.Length * argInfo.ArrayLength];

                                        for (var j = 0; j < argInfo.ArrayLength; ++j)
                                        {
                                            Buffer.BlockCopy(bytes, 0, arrBytes, j * bytes.Length, bytes.Length);
                                        }

                                        argLength += arrBytes.Length;
                                        args.Add(arrBytes);
                                    }
                                    else
                                    {
                                        argLength += bytes.Length;
                                        args.Add(bytes);
                                    }

                                    lastType = argInfo.DataType;
                                    continue;
                                }

                                //find parsed argument
                                string? argValue = null;
                                for (var j = 0; j < parsedArgs.Length; ++j)
                                {
                                    if (argInfo.Name.Equals(parsedArgs[j].Item1, StringComparison.OrdinalIgnoreCase))
                                    {
                                        argValue = parsedArgs[j].Item2;
                                        handledArgs[j] = true;
                                        break;
                                    }
                                }
                                if (argValue is null) throw new FileFormatException($"Missing argument value for {argInfo.Name} on line {i+1}.");

                                //add null-string padding
                                if (lastType == DataTypes.NullString)
                                {
                                    argLength += encodingWidth;
                                    args.Add(nullStringPadding);
                                }

                                //add encoding padding
                                if (argInfo.DataType == DataTypes.String)
                                {
                                    var paddingLength = (-argLength % encodingWidth + encodingWidth) % encodingWidth;
                                    if (paddingLength > 0)
                                    {
                                        argLength += paddingLength;
                                        args.Add(new byte[paddingLength]);
                                    }
                                }

                                //handle arrays
                                if (argInfo.ArrayLength > 0)
                                {
                                    var entries = argValue.TrimStart('[').TrimEnd(']').Split(',');
                                    if (argInfo.ArrayLength != entries.Length) throw new FileFormatException($"Argument array lengths do not match on line {i+1}. Expected an array of length {argInfo.ArrayLength} but found an array of length {entries.Length}.");

                                    foreach (var arrValue in entries)
                                    {
                                        var bytes = ParseKnownArgumentValue(arrValue.Trim(), argInfo, bigEndian, encoding, i);
                                        argLength += bytes.Length;
                                        args.Add(bytes);
                                    }
                                }
                                else
                                {
                                    var bytes = ParseKnownArgumentValue(argValue, argInfo, bigEndian, encoding, i);
                                    argLength += bytes.Length;
                                    args.Add(bytes);
                                }

                                lastType = argInfo.DataType;
                            }

                            //append all remaining undefined arguments
                            for (var j = 0; j < parsedArgs.Length; ++j)
                            {
                                if (handledArgs[j]) continue;
                                var bytes = ParseArgumentValue(parsedArgs[j].Item2, DataTypes.HexString, bigEndian, Encoding.UTF8, parsedArgs[j].Item1, i);
                                argLength += bytes.Length;
                                args.Add(bytes);
                            }

                            //combine all arguments into single byte array
                            var argBytes = new byte[argLength];
                            var argIndex = 0;
                            foreach (var arg in args)
                            {
                                Buffer.BlockCopy(arg, 0, argBytes, argIndex, arg.Length);
                                argIndex += arg.Length;
                            }

                            tag.Args = argBytes;
                        }
                    }
                    else if (tagName.IndexOf(':') > -1)
                    {
                        var tagParts = tagName.Split(':');
                        if (!ushort.TryParse(tagParts[0], out var group)) throw new FileFormatException($"Invalid tag format value for group index found on line {i+1}.");
                        if (!ushort.TryParse(tagParts[1], out var type)) throw new FileFormatException($"Invalid tag format value for type index found on line {i+1}.");
                        tag.Group = group;
                        tag.Type = type;

                        if (argsFound)
                        {
                            var argParts = ParseTagArguments(tagMatch.Groups[2].Value);
                            if (argParts.Length > 1) throw new FileFormatException($"Undefined tags can only have a single argument (error on line {i+1}).");
                            tag.Args = ParseHexString(argParts[0].Item2, argParts[0].Item1, i);
                        }
                    }
                    else throw new FileFormatException($"Unknown tag \"{tagName}\" found on line {i+1}. If the tag is not listed in the used tag map, the tag has to follow the format of \"<groupIndex>:<typeIndex>\", example: \"1:2\"");

                    var firstIndex = text.IndexOf(tagMatch.Groups[0].Value, StringComparison.InvariantCulture);
                    text = text[..firstIndex] + "{{" + message.Tags.Count + "}}" + text[(firstIndex + tagMatch.Groups[0].Value.Length)..];
                    message.Tags.Add(tag);
                }
            }

            message.Text += text + "\n";
        }

        if (message.Text.Length == 0) return;
        message.Text = message.Text.EndsWith("\n\n") ? message.Text[..^2] : message.Text[..^1];
    }

    private static (string, string)[] ParseTagArguments(string value)
    {
        var args = new List<(string, string)>();
        foreach (Match match in TagArgumentRegex().Matches(value))
        {
            args.Add((match.Groups[1].Value, match.Groups[2].Value));
        }
        return [..args];
    }

    private static byte[] ParseKnownArgumentValue(string value, MsbtArgumentInfo info, bool isBigEndian, Encoding encoding, int line)
    {
        if (info.ValueMap.Count == 0) return ParseArgumentValue(value, info.DataType, isBigEndian, encoding, info.Name, line);
        if (info.ValueMap.TryGetValue(value, out var valueInfo)) return info.DataType.Serialize(valueInfo.Value, isBigEndian, encoding);
        throw new FileFormatException($"Failed to find value \"{value}\" of argument \"{info.Name}\" in defined value map (line {line+1}).");
    }

    private static byte[] ParseArgumentValue(string value, DataType dataType, bool isBigEndian, Encoding encoding, string name, int line)
    {
        try
        {
            return dataType.Serialize(value, isBigEndian, encoding);
        }
        catch
        {
            throw new FileFormatException($"Failed to convert tag argument \"{name}\" to \"{dataType.Name}\" (line {line+1}).");
        }
    }
    #endregion

    #region compiled regex
    [GeneratedRegex("^%%%$")]
    private static partial Regex HeaderDelimiterRegex();

    [GeneratedRegex(@"^\s*([A-Za-z0-9_]+)\s*:\s*(.*)$")]
    private static partial Regex HeaderParameterRegex();

    [GeneratedRegex("^---$")]
    private static partial Regex MessageDelimiterRegex();

    [GeneratedRegex("""\{\{\s*([A-Za-z0-9_]+:[A-Za-z0-9_]+|[A-Za-z0-9_]+)\s*((?:[A-Za-z0-9_]+=\"[^"]*\"\s*)*)\}\}""")]
    private static partial Regex TagRegex();

    [GeneratedRegex("""([A-Za-z0-9_]+)=\"([^"]*)\"\s*""")]
    private static partial Regex TagArgumentRegex();
    #endregion

    #region helper class
    private class HeaderParameter<T>
    {
        public required string Name { get; init; }

        public required Action<T, string, string, int> Parser { get; init; }

        public HeaderParameter<T>[] Children { get; init; } = [];
    }
    #endregion
}