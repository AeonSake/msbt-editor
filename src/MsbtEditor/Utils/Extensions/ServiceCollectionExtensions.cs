﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MsbtEditor;

internal static class ServiceCollectionExtensions
{
    public static IServiceCollection ConfigureWritable<TOptions>(this IServiceCollection services, IConfigurationSection section, string file) where TOptions : class, new()
    {
        services.Configure<TOptions>(section);
        return services.AddTransient<IWritableOptions<TOptions>>(provider =>
        {
            var options = provider.GetRequiredService<IOptionsMonitor<TOptions>>();
            return new WritableOptions<TOptions>(options, section.Key, file);
        });
    }

    public static IServiceCollection AddTransientWithInterfaces<TService>(this IServiceCollection services) where TService : class
    {
        services.AddTransient<TService>();
        AddInterfaces<TService>(services.AddTransient);
        return services;
    }

    public static IServiceCollection AddScopedWithInterfaces<TService>(this IServiceCollection services) where TService : class
    {
        services.AddScoped<TService>();
        AddInterfaces<TService>(services.AddScoped);
        return services;
    }

    public static IServiceCollection AddSingletonWithInterfaces<TService>(this IServiceCollection services) where TService : class
    {
        services.AddSingleton<TService>();
        AddInterfaces<TService>(services.AddSingleton);
        return services;
    }

    private static void AddInterfaces<TService>(Func<Type, Func<IServiceProvider, object>, IServiceCollection> addAction) where TService : class
    {
        foreach (var serviceInterface in typeof(TService).GetInterfaces())
        {
            addAction(serviceInterface, provider => provider.GetRequiredService<TService>());
        }
    }
}