﻿using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Components;

namespace MsbtEditor;

internal static class ElementReferenceExtensions
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsInitialized(this ElementReference element) => !string.IsNullOrEmpty(element.Id);
}