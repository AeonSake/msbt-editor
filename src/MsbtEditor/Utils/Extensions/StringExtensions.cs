﻿using System.Runtime.CompilerServices;

namespace MsbtEditor;

internal static class StringExtensions
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Capitalize(this string str) => str.Length == 0 ? string.Empty : char.ToUpper(str[0]) + str[1..];
}