﻿using System.Diagnostics.CodeAnalysis;

namespace MsbtEditor;

internal sealed class TypeDictionary<TValue>
{
    private readonly Dictionary<Type, TValue> _dictionary = new();

    public void Add<T>(TValue value) => _dictionary[typeof(T)] = value;

    public void Add(Type type, TValue value) => _dictionary[type] = value;

    public bool Remove<T>() => _dictionary.Remove(typeof(T));

    public bool Remove(Type type) => _dictionary.Remove(type);

    public bool Contains<T>() => TryGet(typeof(T), out _);

    public bool Contains(Type type) => TryGet(type, out _);

    public bool TryGet<T>([MaybeNullWhen(false)] out TValue value) => TryGet(typeof(T), out value);

    public bool TryGet(Type type, [MaybeNullWhen(false)] out TValue value)
    {
        value = default;

        //check for exact match
        if (_dictionary.TryGetValue(type, out value)) return true;

        //check all interfaces
        foreach (var typeInterface in type.GetInterfaces())
        {
            if (_dictionary.TryGetValue(typeInterface, out value)) return true;
        }

        //check all base types
        for (var baseType = type.BaseType; baseType is not null; baseType = baseType.BaseType)
        {
            if (_dictionary.TryGetValue(baseType, out value)) return true;
        }

        return false;
    }

    public TValue GetOrDefault<T>(TValue defaultValue) => TryGet<T>(out var value) ? value : defaultValue;

    public TValue GetOrDefault(Type type, TValue defaultValue) => TryGet(type, out var value) ? value : defaultValue;
}