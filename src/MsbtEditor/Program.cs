using System.Globalization;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Serilog.Events;

namespace MsbtEditor;

internal static class Program
{
    #region private members
    private static MultiInstanceApplicationContext<MainForm>? _appContext;
    private static ServiceProvider? _serviceProvider;
    private static ConfigurationManager? _configManager;
    #endregion

    #region private methods
    [STAThread]
    private static void Main(string[] args)
    {
        var app = new SingletonAppManager(CreateInstance);
        app.Run(InternalMain, args);
    }

    private static void InternalMain(string[] args)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        ApplicationConfiguration.Initialize();

        _ = new Control(); //required to initialize UI thread/context
        _appContext = new MultiInstanceApplicationContext<MainForm>(SynchronizationContext.Current!);
        InitServices();

        //temp cleanup
        if (Directory.Exists(CommonPaths.TempFolder))
        {
            try
            {
                Directory.Delete(CommonPaths.TempFolder, true);
            }
            catch
            {
                //ignored
            }
        }

        CreateInstance(args);
        Application.Run(_appContext);

        if (_serviceProvider is null) return;
        Task.Run(_serviceProvider.DisposeAsync).Wait();
        _configManager?.Dispose();
    }

    private static void CreateInstance(string[] args)
    {
        if (_appContext is null || _serviceProvider is null) return;

        //only open new window if all windows are closed/minimized or no args were sent
        if (_appContext.LastFocusedForm is not null && args.Length > 0)
        {
            _ = _appContext.LastFocusedForm.OpenPaths(args);
            return;
        }

        _appContext.ExecuteInUiThread(() => _appContext.AddForm(new MainForm(_serviceProvider, args)));
    }

    private static void InitServices()
    {
        var services = new ServiceCollection();

        //compression providers
        services.AddScoped<ICompressionProvider, ZstdCompressionProvider>();
        services.AddScoped<ICompressionProvider, Yaz0CompressionProvider>();
        services.AddScoped<ICompressionProvider, Yay0CompressionProvider>();
        services.AddScoped<ICompressionProvider, Lz10CompressionProvider>();
        services.AddScoped<ICompressionProvider, Lz11CompressionProvider>();
        services.AddScoped<ICompressionProvider, Lz77CompressionProvider>();
        services.AddScoped<ICompressionProvider, ZLibCompressionProvider>();

        //file providers
        services.AddScoped<FileDataFactory>();
        services.AddScoped<IFileDataProvider, ArcFileDataProvider>();
        services.AddScoped<IFileDataProvider, DarcFileDataProvider>();
        services.AddScoped<IFileDataProvider, NarcFileDataProvider>();
        services.AddScoped<IFileDataProvider, RarcFileDataProvider>();
        services.AddScoped<IFileDataProvider, SarcFileDataProvider>();
        services.AddScoped<IFileDataProvider, ZipFileDataProvider>();
        services.AddScoped<IFileDataProvider, UmsbtFileDataProvider>();
        services.AddScoped<IFileDataProvider, MsbtFileDataProvider>();
        services.AddScoped<IFileDataProvider, MsbpFileDataProvider>();
        services.AddScoped<IFileDataProvider, BmgFileDataProvider>();
        services.AddScoped<IFileDataProvider, NLTextFileDataProvider>();
        services.AddScoped<IFileDataProvider, BcsvFileDataProvider>();
        services.AddScoped<IFileDataProvider, BymlFileDataProvider>();
        services.AddScoped<IFileDataProvider, BtxtFileDataProvider>();
        services.AddScoped<IFileDataProvider, GameConfigFileDataProvider>();

        //editor providers
        services.AddScopedWithInterfaces<TextEditorProvider>();

        //file handling
        services.AddScoped<FileBrowserService>();
        services.AddScoped<GameConfigService>();
        services.AddScoped<EditorService>();

        //UI
        services.AddScoped<FileIconProvider>();
        services.AddScoped<StatusService>();
        services.AddScoped<ContextMenuService>();
        services.AddScoped<WindowsInteropService>();
        services.AddScoped<ISystemInteropService>(provider => provider.GetRequiredService<WindowsInteropService>());
        services.AddScoped<DialogService>();
        services.AddScoped<FileDragDropService>();
        services.AddScoped<BrowserContextService>();
        services.AddScoped<FileContextActionProvider>();
        services.AddScoped<EditorContextActionProvider>();
        services.AddScoped<ContextActionProvider>();

        //other
        services.AddScoped<AppStateService>();
        services.AddSingleton<UpdateService>();
        services.AddScoped<ScopeHandleService>();
        services.AddSingleton<ScopeManagerService>();
        services.AddScoped<JsRuntimeProvider>();

        //core
        services.AddWindowsFormsBlazorWebView();

        //prepare settings
        if (File.Exists(CommonPaths.SettingsFile))
        {
            //merge current settings with default settings
            try
            {
                using var readStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("MsbtEditor.Settings.settings.json")!;
                using var reader = new StreamReader(readStream);
                var defaultSettings = JsonConvert.DeserializeObject<JObject>(reader.ReadToEnd())!;

                var currentSettings = JsonConvert.DeserializeObject<JObject>(File.ReadAllText(CommonPaths.SettingsFile));
                defaultSettings.Merge(currentSettings, new JsonMergeSettings
                {
                    PropertyNameComparison = StringComparison.OrdinalIgnoreCase,
                    MergeArrayHandling = MergeArrayHandling.Merge,
                    MergeNullValueHandling = MergeNullValueHandling.Ignore
                });

                File.WriteAllText(CommonPaths.SettingsFile, JsonConvert.SerializeObject(currentSettings, Formatting.Indented));
            }
            catch
            {
                ExportDefaultSettings();
            }
        }
        else ExportDefaultSettings();

        //settings
        var configManager = new ConfigurationManager();
        configManager.AddJsonFile(CommonPaths.SettingsFile, false, true);
        services.ConfigureWritable<FileBrowserSettings>(configManager.GetSection("FileBrowser"), CommonPaths.SettingsFile);
        services.ConfigureWritable<EditorSettings>(configManager.GetSection("Editor"), CommonPaths.SettingsFile);
        services.ConfigureWritable<UpdateSettings>(configManager.GetSection("Update"), CommonPaths.SettingsFile);
        services.ConfigureWritable<GameConfigSettings>(configManager.GetSection("GameConfig"), CommonPaths.SettingsFile);
        services.ConfigureWritable<DebugSettings>(configManager.GetSection("Debug"), CommonPaths.SettingsFile);

        //logging
        services.AddLogging(builder =>
        {
            if (!bool.TryParse(configManager.GetSection("Debug:Enabled").Value, out var isDebug)) isDebug = false;

            var loggerConfig = new LoggerConfiguration();
            loggerConfig.ReadFrom.Configuration(configManager);
            loggerConfig.WriteTo.File(Path.Combine(CommonPaths.LogsFolder, "log_.txt"),
                                      isDebug ? LogEventLevel.Debug : LogEventLevel.Information,
                                      fileSizeLimitBytes: 10485760,
                                      rollingInterval: RollingInterval.Day,
                                      retainedFileCountLimit: 31,
                                      rollOnFileSizeLimit: true,
                                      outputTemplate: "{Timestamp:HH:mm} [{Level:u3}] {Message}{NewLine}{Exception}",
                                      shared: true);

            builder.AddSerilog(loggerConfig.CreateLogger());
        });

        _serviceProvider = services.BuildServiceProvider();
        _configManager = configManager;
        return;

        static void ExportDefaultSettings()
        {
            Directory.CreateDirectory(Path.GetDirectoryName(CommonPaths.SettingsFile)!);
            using var readStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("MsbtEditor.Settings.settings.json")!;
            using var writeStream = File.Create(CommonPaths.SettingsFile);
            readStream.CopyTo(writeStream);
        }
    }
    #endregion
}