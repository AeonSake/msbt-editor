﻿using NintendoTools.Compression.Lz77;

namespace MsbtEditor;

internal sealed class Lz77CompressionProvider : ICompressionProvider
{
    #region ICompressionProvider interface
    public bool CanDecompress(Stream stream) => Lz77Decompressor.CanDecompressStatic(stream);

    public CompressionWrapper GetWrapper(StorageProvider provider) => new Lz77CompressionWrapper(provider);
    #endregion

    #region helper class
    private class Lz77CompressionWrapper(StorageProvider provider) : CompressionWrapper(provider)
    {
        private bool _isChunked;

        public override Stream GetReadStream()
        {
            using var readStream = Provider.GetReadStream();

            var isChunked = _isChunked;
            if (readStream.Length > 4)
            {
                readStream.Seek(4, SeekOrigin.Begin);
                isChunked = readStream.ReadByte() == 0xf7;
                readStream.Seek(0, SeekOrigin.Begin);
            }

            var decompressor = new Lz77Decompressor();
            var resultStream = new MemoryStream();
            decompressor.Decompress(readStream, resultStream);

            resultStream.Seek(0, SeekOrigin.Begin);
            _isChunked = isChunked;
            return resultStream;
        }

        public override Stream GetWriteStream() => new WrappedStream<MemoryStream>(new MemoryStream(), dataStream =>
        {
            if (dataStream.Length == 0) return;

            using var writeStream = Provider.GetWriteStream();

            var compressor = new Lz77Compressor {Chunked = _isChunked};
            compressor.Compress(dataStream, writeStream);
        });
    }
    #endregion
}