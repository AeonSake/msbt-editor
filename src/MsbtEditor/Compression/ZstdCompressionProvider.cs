﻿using NintendoTools.Compression.Zstd;
using Instances = (NintendoTools.Compression.Zstd.ZstdCompressor Compressor, NintendoTools.Compression.Zstd.ZstdDecompressor Decompressor);

namespace MsbtEditor;

internal sealed class ZstdCompressionProvider : ICompressionProvider, IDisposable
{
    #region private members
    private readonly GameConfigService _gameConfigService;
    private Dictionary<ZstdConfig, Instances> _cache = new();
    #endregion

    #region constructor
    public ZstdCompressionProvider(GameConfigService gameConfigService)
    {
        _gameConfigService = gameConfigService;
        _gameConfigService.CurrentConfigChanged += OnConfigChanged;
        OnConfigChanged(null, EventArgs.Empty);
    }
    #endregion

    #region ICompressionProvider interface
    public bool CanDecompress(Stream stream) => ZstdDecompressor.CanDecompressStatic(stream);

    public CompressionWrapper GetWrapper(StorageProvider provider) => new ZstdCompressionWrapper(provider, this);
    #endregion

    #region private methods
    private void OnConfigChanged(object? sender, EventArgs args)
    {
        //cache compressor/decompressor instances to reduce load (especially if using dicts)
        var newMap = new Dictionary<ZstdConfig, Instances>();
        foreach (var config in _gameConfigService.CurrentConfig.Zstd) AddInstances(config);
        var defaultConfig = _gameConfigService.CurrentConfig.Zstd.GetDefaultMatch();
        AddInstances(defaultConfig);
        _cache = newMap;
        return;

        void AddInstances(ZstdConfig config)
        {
            var compressor = new ZstdCompressor {CompressionLevel = config.CompressionLevel, Dict = config.Dict};
            var decompressor = new ZstdDecompressor {Dict = config.Dict};
            newMap.Add(config, (compressor, decompressor));
        }
    }
    #endregion

    #region IDisposable interface
    public void Dispose() => _gameConfigService.CurrentConfigChanged -= OnConfigChanged;
    #endregion

    #region helper class
    private class ZstdCompressionWrapper(StorageProvider provider, ZstdCompressionProvider compressionProvider) : CompressionWrapper(provider)
    {
        public override Stream GetReadStream()
        {
            using var readStream = Provider.GetReadStream();

            var config = compressionProvider._gameConfigService.CurrentConfig.Zstd.FindMatch(FilePath);
            var decompressor = compressionProvider._cache.TryGetValue(config, out var instances) ? instances.Decompressor : new ZstdDecompressor {Dict = config.Dict};

            var resultStream = new MemoryStream();
            decompressor.Decompress(readStream, resultStream);

            resultStream.Seek(0, SeekOrigin.Begin);
            return resultStream;
        }

        public override Stream GetWriteStream() => new WrappedStream<MemoryStream>(new MemoryStream(), dataStream =>
        {
            if (dataStream.Length == 0) return;

            using var writeStream = Provider.GetWriteStream();

            var config = compressionProvider._gameConfigService.CurrentConfig.Zstd.FindMatch(FilePath);
            var compressor = compressionProvider._cache.TryGetValue(config, out var instances) ? instances.Compressor : new ZstdCompressor {CompressionLevel = config.CompressionLevel, Dict = config.Dict};

            compressor.Compress(dataStream, writeStream);
        });
    }
    #endregion
}