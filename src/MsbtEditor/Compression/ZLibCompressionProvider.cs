﻿using NintendoTools.Compression.ZLib;

namespace MsbtEditor;

internal sealed class ZLibCompressionProvider : ICompressionProvider
{
    #region ICompressionProvider interface
    public bool CanDecompress(Stream stream) => ZLibDecompressor.CanDecompressStatic(stream);

    public CompressionWrapper GetWrapper(StorageProvider provider) => new ZLibCompressionWrapper(provider);
    #endregion

    #region helper class
    private class ZLibCompressionWrapper(StorageProvider provider) : CompressionWrapper(provider)
    {
        public override Stream GetReadStream()
        {
            using var readStream = Provider.GetReadStream();

            var decompressor = new ZLibDecompressor();
            var resultStream = new MemoryStream();
            decompressor.Decompress(readStream, resultStream);

            resultStream.Seek(0, SeekOrigin.Begin);
            return resultStream;
        }

        public override Stream GetWriteStream() => new WrappedStream<MemoryStream>(new MemoryStream(), dataStream =>
        {
            if (dataStream.Length == 0) return;

            using var writeStream = Provider.GetWriteStream();

            var compressor = new ZLibCompressor();
            compressor.Compress(dataStream, writeStream);
        });
    }
    #endregion
}