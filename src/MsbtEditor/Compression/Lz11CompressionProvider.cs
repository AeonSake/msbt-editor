﻿using NintendoTools.Compression.Lz11;

namespace MsbtEditor;

internal sealed class Lz11CompressionProvider : ICompressionProvider
{
    #region ICompressionProvider interface
    public bool CanDecompress(Stream stream) => Lz11Decompressor.CanDecompressStatic(stream);

    public CompressionWrapper GetWrapper(StorageProvider provider) => new Lz11CompressionWrapper(provider);
    #endregion

    #region helper class
    private class Lz11CompressionWrapper(StorageProvider provider) : CompressionWrapper(provider)
    {
        public override Stream GetReadStream()
        {
            using var readStream = Provider.GetReadStream();

            var decompressor = new Lz11Decompressor();
            var resultStream = new MemoryStream();
            decompressor.Decompress(readStream, resultStream);

            resultStream.Seek(0, SeekOrigin.Begin);
            return resultStream;
        }

        public override Stream GetWriteStream() => new WrappedStream<MemoryStream>(new MemoryStream(), dataStream =>
        {
            if (dataStream.Length == 0) return;

            using var writeStream = Provider.GetWriteStream();

            var compressor = new Lz11Compressor();
            compressor.Compress(dataStream, writeStream);
        });
    }
    #endregion
}