﻿using NintendoTools.Compression.Lz10;

namespace MsbtEditor;

internal sealed class Lz10CompressionProvider : ICompressionProvider
{
    #region ICompressionProvider interface
    public bool CanDecompress(Stream stream) => Lz10Decompressor.CanDecompressStatic(stream);

    public CompressionWrapper GetWrapper(StorageProvider provider) => new Lz10CompressionWrapper(provider);
    #endregion

    #region helper class
    private class Lz10CompressionWrapper(StorageProvider provider) : CompressionWrapper(provider)
    {
        public override Stream GetReadStream()
        {
            using var readStream = Provider.GetReadStream();

            var decompressor = new Lz10Decompressor();
            var resultStream = new MemoryStream();
            decompressor.Decompress(readStream, resultStream);

            resultStream.Seek(0, SeekOrigin.Begin);
            return resultStream;
        }

        public override Stream GetWriteStream() => new WrappedStream<MemoryStream>(new MemoryStream(), dataStream =>
        {
            if (dataStream.Length == 0) return;

            using var writeStream = Provider.GetWriteStream();

            var compressor = new Lz10Compressor();
            compressor.Compress(dataStream, writeStream);
        });
    }
    #endregion
}