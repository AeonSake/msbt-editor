﻿namespace MsbtEditor;

internal interface ICompressionProvider
{
    public bool CanDecompress(Stream stream);

    public CompressionWrapper GetWrapper(StorageProvider provider);
}