﻿using NintendoTools.Compression.Yay0;

namespace MsbtEditor;

internal sealed class Yay0CompressionProvider : ICompressionProvider
{
    #region ICompressionProvider interface
    public bool CanDecompress(Stream stream) => Yay0Decompressor.CanDecompressStatic(stream);

    public CompressionWrapper GetWrapper(StorageProvider provider) => new Yay0CompressionWrapper(provider);
    #endregion

    #region helper class
    private class Yay0CompressionWrapper(StorageProvider provider) : CompressionWrapper(provider)
    {
        public override Stream GetReadStream()
        {
            using var readStream = Provider.GetReadStream();

            var decompressor = new Yay0Decompressor();
            var resultStream = new MemoryStream();
            decompressor.Decompress(readStream, resultStream);

            resultStream.Seek(0, SeekOrigin.Begin);
            return resultStream;
        }

        public override Stream GetWriteStream() => new WrappedStream<MemoryStream>(new MemoryStream(), dataStream =>
        {
            if (dataStream.Length == 0) return;

            using var writeStream = Provider.GetWriteStream();

            var compressor = new Yay0Compressor();
            compressor.Compress(dataStream, writeStream);
        });
    }
    #endregion
}