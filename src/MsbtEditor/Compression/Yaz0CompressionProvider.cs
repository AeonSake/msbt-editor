﻿using NintendoTools.Compression.Yaz0;

namespace MsbtEditor;

internal sealed class Yaz0CompressionProvider : ICompressionProvider
{
    #region ICompressionProvider interface
    public bool CanDecompress(Stream stream) => Yaz0Decompressor.CanDecompressStatic(stream);

    public CompressionWrapper GetWrapper(StorageProvider provider) => new Yaz0CompressionWrapper(provider);
    #endregion

    #region helper class
    private class Yaz0CompressionWrapper(StorageProvider provider) : CompressionWrapper(provider)
    {
        public override Stream GetReadStream()
        {
            using var readStream = Provider.GetReadStream();

            var decompressor = new Yaz0Decompressor();
            var resultStream = new MemoryStream();
            decompressor.Decompress(readStream, resultStream);

            resultStream.Seek(0, SeekOrigin.Begin);
            return resultStream;
        }

        public override Stream GetWriteStream() => new WrappedStream<MemoryStream>(new MemoryStream(), dataStream =>
        {
            if (dataStream.Length == 0) return;

            using var writeStream = Provider.GetWriteStream();

            var compressor = new Yaz0Compressor();
            compressor.Compress(dataStream, writeStream);
        });
    }
    #endregion
}