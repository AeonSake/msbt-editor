﻿namespace MsbtEditor;

internal sealed class ErrorFileData(StorageProvider storageProvider, Exception exception, ContainerFileData? parent = null) : FileData(storageProvider, parent)
{
    public override bool IsReadOnly => true;

    public Exception Exception => exception;
}