﻿namespace MsbtEditor;

internal sealed class FolderFileData : ContainerFileData
{
    #region private members
    private readonly FileDataFactory? _fileFactory;
    #endregion

    #region constructors
    public FolderFileData(DirectoryInfo directoryInfo, FileDataFactory fileFactory, bool isVirtual, ContainerFileData? parent = null) : base(new FolderStorageProvider(directoryInfo, isVirtual), parent)
    {
        _fileFactory = fileFactory;
    }

    public FolderFileData(string directoryPath, FileDataFactory fileFactory, bool isVirtual, ContainerFileData? parent = null) : base(new FolderStorageProvider(directoryPath, isVirtual), parent)
    {
        _fileFactory = fileFactory;
    }

    public FolderFileData(DirectoryInfo directoryInfo, IEnumerable<FileData> children, bool isVirtual, ContainerFileData? parent = null) : base(new FolderStorageProvider(directoryInfo, isVirtual), children, parent)
    { }

    public FolderFileData(string directoryPath, IEnumerable<FileData> children, bool isVirtual, ContainerFileData? parent = null) : base(new FolderStorageProvider(directoryPath, isVirtual), children, parent)
    { }
    #endregion

    #region public properties
    public override bool IsReadOnly => Parent?.IsReadOnly ?? false;
    #endregion

    #region protected properties
    protected override bool DisposeChildrenOnReload => _fileFactory is not null;

    protected override bool AutoResetModifiedState => true;
    #endregion

    #region public methods
    public override bool CanAdd(FileData file) => !IsVirtual || Parent is null || Parent.CanAdd(file);
    #endregion

    #region protected methods
    protected override Task<List<FileData>> LoadChildren() => Task.Run(() =>
    {
        if (_fileFactory is null) return [..Children];

        var directories = Directory.GetDirectories(InternalFilePath);
        var files = Directory.GetFiles(InternalFilePath);
        var children = new FileData[directories.Length + files.Length];

        Parallel.For(0, directories.Length, i => children[i] = new FolderFileData(directories[i], _fileFactory, IsVirtual, this));
        Parallel.For(0, files.Length, i =>
        {
            var provider = new FileStorageProvider(new FileInfo(files[i]), IsVirtual);
            children[directories.Length + i] = _fileFactory.Create(provider, this);
        });

        return children.ToList();
    });

    protected override Task InternalSave() => Parallel.ForEachAsync(Children, (file, _) => new ValueTask(file.Save()));
    #endregion
}