﻿namespace MsbtEditor;

internal sealed class UnknownFileData(StorageProvider storageProvider, ContainerFileData? parent = null) : FileData(storageProvider, parent)
{
    public override bool IsReadOnly => true;
}