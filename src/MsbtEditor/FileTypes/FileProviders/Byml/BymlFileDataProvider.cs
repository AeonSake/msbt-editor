﻿using NintendoTools.FileFormats;
using NintendoTools.FileFormats.Byml;

namespace MsbtEditor;

internal sealed class BymlFileDataProvider : IFileDataProvider
{
    #region private members
    private static readonly IconValue Icon = new("""<path d="M15.5 14l0 8l5 0"/><path d="M3.5 22l0 -8l4 4l4 -4l0 8"/><path d="M13.5 2l3 4l3 -4"/><path d="M16.5 6l0 4"/><path d="M7.5 10l-3 0l0 -8l3 0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M7.5 6l-3 0"/>""");
    private readonly GameConfigService _gameConfigService;
    #endregion

    #region constructor
    public BymlFileDataProvider(GameConfigService gameConfigService, FileIconProvider iconProvider, TextEditorProvider textEditorProvider)
    {
        _gameConfigService = gameConfigService;

        iconProvider.Register<BymlFileData>(Icon, "#88f257");
        textEditorProvider.Register(new BymlYamlEditorContentHandler());
        textEditorProvider.Register(new BymlJsonEditorContentHandler());
    }
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream) => BymlFileParser.CanParseStatic(stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new BymlFileData(provider, _gameConfigService, parent);
    #endregion

    #region helper classes
    private class BymlYamlEditorContentHandler : ITextEditorContentHandler<BymlFileData>
    {
        private static readonly BymlYamlSerializer Serializer = new();

        public string Language => "yaml";

        public string DisplayName => "YAML";

        public int TabSize => 2;

        public bool IsReadOnly => true;

        public async Task<string> Load(BymlFileData file)
        {
            var model = await file.LoadModel();
            return Serializer.Serialize(model);
        }

        public Task Save(BymlFileData file, string content) => throw new NotImplementedException("Saving BYML is not yet supported.");
    }

    private class BymlJsonEditorContentHandler : ITextEditorContentHandler<BymlFileData>
    {
        private static readonly BymlJsonSerializer Serializer = new();

        public string Language => "json";

        public string DisplayName => "JSON";

        public int TabSize => 2;

        public bool IsReadOnly => true;

        public async Task<string> Load(BymlFileData file)
        {
            var model = await file.LoadModel();
            return Serializer.Serialize(model);
        }

        public Task Save(BymlFileData file, string content) => throw new NotImplementedException("Saving BYML is not yet supported.");
    }
    #endregion
}