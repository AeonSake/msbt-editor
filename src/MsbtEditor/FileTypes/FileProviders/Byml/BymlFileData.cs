﻿using NintendoTools.FileFormats.Byml;

namespace MsbtEditor;

internal sealed class BymlFileData(StorageProvider storageProvider, GameConfigService gameConfigService, ContainerFileData? parent) : FileData(storageProvider, parent)
{
    #region private members
    private static readonly BymlFileParser Parser = new();
    #endregion

    #region public properties
    public override bool IsReadOnly => true;
    #endregion

    #region public methods
    public Task<BymlFile> LoadModel() => Read(stream =>
    {
        var model = Parser.Parse(stream);
        Replace(model.RootNode, gameConfigService.CurrentConfig.Byml.HashValues);
        return model;
    });

    public Task SaveModel(BymlFile file) => throw new NotImplementedException("Saving BYML is not yet supported.");
    #endregion

    #region private methods
    //this is temporary until BYML parsing is improved in NintendoTools
    public static void Replace(INode node, Dictionary<string, string> hashValues)
    {
        if (node is DictionaryNode dict)
        {
            var rename = new Dictionary<string, string>();
            foreach (var (key, value) in dict)
            {
                if (IsHash(key))
                {
                    var hash = key.PadLeft(8, '0').ToUpperInvariant();
                    if (hashValues.TryGetValue(hash, out var label)) rename.Add(key, label);
                }
                Replace(value, hashValues);
            }

            foreach (var (hash, name) in rename) dict.Rename(hash, name);
        }
        else if (node is ArrayNode array)
        {
            foreach (var childNode in array) Replace(childNode, hashValues);
        }
        return;

        static bool IsHash(string value)
        {
            if (value.Length > 8) return false;

            foreach (var c in value)
            {
                if (c is >= '0' and <= '9' or >= 'a' and <= 'f' or >= 'A' and <= 'F') continue;
                return false;
            }

            return true;
        }
    }
    #endregion
}