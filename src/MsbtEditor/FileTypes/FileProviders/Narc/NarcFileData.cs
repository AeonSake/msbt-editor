﻿using NintendoTools.FileFormats.Narc;

namespace MsbtEditor;

internal sealed class NarcFileData(StorageProvider storageProvider, FileDataFactory fileFactory, ContainerFileData? parent = null) : ContainerFileData(storageProvider, parent)
{
    #region private members
    private static readonly NarcFileParser Parser = new();
    private static readonly NarcFileCompiler Compiler = new();
    private NarcFile? _fileData;
    #endregion

    #region protected methods
    protected override async Task<List<FileData>> LoadChildren()
    {
        await using var readStream = GetReadStream();
        var model = Parser.Parse(readStream);

        var children = await BuildFiles(model.Content, GetTempDir());

        _fileData = model;
        _fileData.Content = []; //release memory

        return children;

        async Task<List<FileData>> BuildFiles(IList<INarcNode> nodes, string path)
        {
            var files = new FileData[nodes.Count];

            await Parallel.ForAsync(0, files.Length, async (i, _) =>
            {
                var node = nodes[i];
                var filePath = Path.Combine(path, model.HasFileNames ? node.Name : $"{i:D4}");

                if (node is NarcFileNode fileNode)
                {
                    var provider = new FileStorageProvider(filePath, true);
                    await using var writeStream = provider.GetWriteStream();
                    writeStream.Write(fileNode.Data);
                    writeStream.Close();

                    files[i] = fileFactory.Create(provider);
                }
                else if (node is NarcDirectoryNode dirNode)
                {
                    Directory.CreateDirectory(filePath);
                    var childFiles = await BuildFiles(dirNode.Children, filePath);

                    files[i] = new FolderFileData(filePath, childFiles, true);
                }
            });

            return [..files];
        }
    }

    protected override async Task InternalSave()
    {
        if (_fileData is null) return;

        var model = new NarcFile
        {
            BigEndian = _fileData.BigEndian,
            Version = _fileData.Version,
            HasFileNames = _fileData.HasFileNames,
            Content = await BuildNodes(Children)
        };

        await using var writeStream = GetWriteStream();
        Compiler.Compile(model, writeStream);
        return;

        async Task<List<INarcNode>> BuildNodes(IReadOnlyList<FileData> files)
        {
            var nodes = new INarcNode[files.Count];

            await Parallel.ForAsync(0, files.Count, async (i, token) =>
            {
                var file = files[i];

                if (file is FolderFileData folder)
                {
                    nodes[i] = new NarcDirectoryNode
                    {
                        Name = _fileData.HasFileNames ? file.Name : string.Empty,
                        Children = await BuildNodes(folder.Children)
                    };
                }
                else
                {
                    await file.Save();
                    nodes[i] = new NarcFileNode
                    {
                        Name = _fileData.HasFileNames ? file.Name : string.Empty,
                        Data = await File.ReadAllBytesAsync(file.InternalFilePath, token)
                    };
                }
            });

            return [..nodes];
        }
    }
    #endregion
}