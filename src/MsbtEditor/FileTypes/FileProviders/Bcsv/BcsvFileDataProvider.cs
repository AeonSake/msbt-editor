﻿using System.Text;
using NintendoTools.FileFormats.Bcsv;

namespace MsbtEditor;

internal class BcsvFileDataProvider : IFileDataProvider
{
    #region private members
    private static readonly IconValue Icon = new("""<path d="M13 14l3.5 8l3.5 -8"/><path d="M4 20c0 1.104 1.12 2 2.5 2c1.38 0 2.5 -0.896 2.5 -2c0 -1.104 -0.831 -1.657 -2.5 -2c-1.927 -0.397 -2.5 -0.896 -2.5 -2c0 -1.104 1.12 -2 2.5 -2c1.38 0 2.5 0.896 2.5 2"/><path d="M19.5 9.464c-0.588 0.341 -1.272 0.536 -2 0.536c-2.208 0 -4 -1.792 -4 -4c0 -2.208 1.792 -4 4 -4c0.728 0 1.412 0.195 2 0.536"/><path d="M7.5 10l-3 0l-0 -8l3 0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M7.5 6l-3 0"/>""");
    private readonly GameConfigService _gameConfigService;
    #endregion

    #region constructor
    public BcsvFileDataProvider(GameConfigService gameConfigService, FileIconProvider iconProvider, TextEditorProvider textEditorProvider)
    {
        _gameConfigService = gameConfigService;

        iconProvider.Register<BcsvFileData>(Icon, "#3cd6b7");
        textEditorProvider.Register(new BcsvEditorContentHandler(gameConfigService));
    }
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream) => BcsvFileParser.CanParseStatic(stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new BcsvFileData(provider, _gameConfigService);
    #endregion

    #region helper classes
    private class BcsvEditorContentHandler(GameConfigService gameConfigService) : ITextEditorContentHandler<BcsvFileData>
    {
        public string Language => "bcsv";

        public string DisplayName => "BCSV";

        public int TabSize => 1;

        public bool IsReadOnly => true;

        public async Task<string> Load(BcsvFileData file)
        {
            var data = await file.LoadModel();

            StringBuilder sb = new();

            sb.AppendLine("%%%");
            string[] searchTarget = ["Label", "Name"];
            var labelColumn = -1;
            for (var i = 0; i < data.HeaderInfo.Length; ++i)
            {
                var headerInfo = data.HeaderInfo[i];
                sb.AppendLine($"{headerInfo.NewHeaderName}: {headerInfo.DataType}");

                // search for 'description' names to use instead of rowId
                if (labelColumn == -1 && searchTarget.Contains(headerInfo.NewHeaderName)) labelColumn = i;

            }
            sb.AppendLine("%%%");

            for (var rowId = 0; rowId < data.Rows; ++rowId)
            {
                sb.AppendLine();

                object? entryName = null;
                if (labelColumn >= 0) entryName = data[rowId, labelColumn];
                // name here doesn't really matter, is just for readability
                sb.AppendLine($"-- {entryName ?? $"Entry #{rowId}"}");

                for (var columnId = 0; columnId < data.Columns; ++columnId)
                {
                    var columnInfo = data.HeaderInfo[columnId];
                    var columnValue = data[rowId, columnId];

                    // handle different formats
                    var value = columnValue switch
                    {
                        string stringValue => $"\"{stringValue}\"",
                        _                  => columnValue?.ToString() ?? string.Empty
                    };

                    var config = gameConfigService.CurrentConfig.Bcsv.FieldHashes;
                    if (columnValue is string hashValue)
                    {
                        if (columnInfo.DataType == BcsvDataType.Crc32 && config.Crc32Values.TryGetValue(hashValue, out var crc)) value = $"\"{crc}\"";
                        else if (columnInfo.DataType == BcsvDataType.Mmh3 && config.Mmh3Values.TryGetValue(hashValue, out var mmh)) value = $"\"{mmh}\"";
                    }

                    sb.AppendLine($" {columnInfo.NewHeaderName}: {value}");
                }
            }

            return sb.ToString();
        }

        public Task Save(BcsvFileData file, string content) => Task.CompletedTask;
    }
    #endregion
}