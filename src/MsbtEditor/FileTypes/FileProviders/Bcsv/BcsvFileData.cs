﻿using NintendoTools.FileFormats.Bcsv;
using NintendoTools.Hashing;

namespace MsbtEditor;

internal sealed class BcsvFileData(StorageProvider storageProvider, GameConfigService gameConfigService) : FileData(storageProvider)
{
    #region public properties
    public override bool IsReadOnly => true;
    #endregion

    #region public methods
    public Task<BcsvFile> LoadModel() => Read(stream =>
    {
        var crc32 = new Crc32Hash();
        var headerInfo = gameConfigService.CurrentConfig.Bcsv.Headers.Select(x =>
        {
            BcsvDataType? bcsvDataType = null;

            // check if we override a type
            if (!string.IsNullOrEmpty(x.DataType)) bcsvDataType = GetTypeByName(x.DataType);

            if (!string.IsNullOrEmpty(x.Name))
            {
                var hash = crc32.Compute(x.Name);

                var newName = x.Name;
                string typeName = string.Empty;
                if (newName.Contains('.'))
                {
                    var splits = newName.Split('.');
                    newName = splits[0];
                    typeName = splits[1];
                }
                else if (newName.Contains(' '))
                {
                    var splits = newName.Split(" ");
                    newName = splits[0];
                    typeName = splits[1];
                }

                if (bcsvDataType.HasValue) return new BcsvHeaderInfo(hash, newName, bcsvDataType.Value); // type override
                return new BcsvHeaderInfo(hash, newName, GetTypeByName(newName));
            }

            if (bcsvDataType.HasValue) return new BcsvHeaderInfo(x.Hash, dataType: bcsvDataType.Value);
            return new BcsvHeaderInfo(x.Hash); // should never occur since we only store hashes when we know the type
        });

        var parser = new BcsvFileParser(headerInfo);
        return parser.Parse(stream);
    });
    #endregion

    #region private methods
    private static BcsvDataType GetTypeByName(string type)
    {
        // string64, string128, etc...
        if (type.StartsWith("string")) return BcsvDataType.Str;

        return type switch
        {
            "str"           => BcsvDataType.Str,
            "s8"            => BcsvDataType.S8,
            "u8"            => BcsvDataType.U8,
            "s16"           => BcsvDataType.S16,
            "u16"           => BcsvDataType.U16,
            "s32"           => BcsvDataType.S32,
            "u32"           => BcsvDataType.U32,
            "f32"           => BcsvDataType.F32,
            "f64"           => BcsvDataType.F64,
            "crc32"         => BcsvDataType.Crc32,
            "hshCstringRef" => BcsvDataType.Crc32,
            "mmh3"          => BcsvDataType.Mmh3,

            "u8[]" => BcsvDataType.U8,
            "s8[]" => BcsvDataType.S8,

            _               => BcsvDataType.Default
        };
    }
    #endregion
}