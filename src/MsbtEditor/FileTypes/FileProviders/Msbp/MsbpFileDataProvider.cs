﻿using System.Text;
using Microsoft.Extensions.Options;
using NintendoTools.FileFormats.Msbp;
using ValueType = NintendoTools.FileFormats.Msbp.ValueType;

namespace MsbtEditor;

internal sealed class MsbpFileDataProvider : IFileDataProvider
{
    #region private members
    private static readonly IconValue Icon = new("""<path d="M8 22l-3 -0l0 -8l3 -0c1.104 -0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 -0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M8 18l-3 -0"/><path d="M15.5 8c0 1.104 1.12 2 2.5 2c1.38 0 2.5 -0.896 2.5 -2c0 -1.104 -0.831 -1.657 -2.5 -2c-1.927 -0.397 -2.5 -0.896 -2.5 -2c0 -1.104 1.12 -2 2.5 -2c1.38 0 2.5 0.896 2.5 2"/><path d="M3.5 10l0 -8l4 4l4 -4l0 8"/><path d="M14 22l0 -8"/><path d="M14 18l3 0c1.104 0 2 -0.896 2 -2c0 -1.104 -0.896 -2 -2 -2l-3 0"/>""");
    #endregion

    #region constructor
    public MsbpFileDataProvider(FileIconProvider iconProvider, TextEditorProvider textEditorProvider, FileContextActionProvider fileContextActionProvider, GameConfigService gameConfigService, IOptionsMonitor<DebugSettings> settings)
    {
        iconProvider.Register<MsbpFileData>(Icon, "#862fbd");
        textEditorProvider.Register(new MsbpEditorContentHandler());
        fileContextActionProvider.Register<MsbpFileData>(file => settings.CurrentValue.Enabled ?
        [
            new ContextAction
            {
                Name = "Convert To Game Config",
                Icon = GlobalIcons.Copy,
                OnClick = () => gameConfigService.Create(file)
            }
        ] : []);
    }
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream) => MsbpFileParser.CanParseStatic(stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new MsbpFileData(provider, parent);
    #endregion

    #region helper classes
    private class MsbpEditorContentHandler : ITextEditorContentHandler<MsbpFileData>
    {
        public string Language => "msbp";

        public string DisplayName => "MSBP";

        public int TabSize => 2;

        public bool IsReadOnly => true;

        public async Task<string> Load(MsbpFileData file)
        {
            var model = await file.LoadModel();
            var builder = new StringBuilder();

            builder.AppendLine($"BigEndian: {model.BigEndian.ToString().ToLowerInvariant()}");
            builder.AppendLine($"Version: {model.Version}");
            builder.AppendLine($"Encoding: {model.Encoding.WebName}");

            if (model.Colors.Length > 0)
            {
                builder.AppendLine();
                builder.AppendLine("Color Definitions (CLR1/CLB1)");
                builder.AppendLine("=============================");
                foreach (var color in model.Colors)
                {
                    if (string.IsNullOrEmpty(color.Name)) builder.AppendLine($"#{color.Red:X2}{color.Green:X2}{color.Blue:X2}{color.Alpha:X2}");
                    else builder.AppendLine($"{color.Name}: #{color.Red:X2}{color.Green:X2}{color.Blue:X2}{color.Alpha:X2}");
                }
            }

            if (model.Attributes.Length > 0)
            {
                builder.AppendLine();
                builder.AppendLine("Attribute Definitions (ATI1/ALB1/ALI1)");
                builder.AppendLine("======================================");
                foreach (var attribute in model.Attributes)
                {
                    builder.Append($"{attribute.Name} [{attribute.Type}]");
                    if (attribute.Type == ValueType.Enum) builder.Append($" {{{string.Join(",", attribute.EnumValues)}}}");
                    builder.AppendLine();
                }
            }

            if (model.TagGroups.Length > 0)
            {
                builder.AppendLine();
                builder.AppendLine("Tag Definitions (TGG2/TAG2/TGP2/TGL2)");
                builder.AppendLine("=====================================");
                for (var i = 0; i < model.TagGroups.Length; ++i)
                {
                    var tagGroup = model.TagGroups[i];
                    builder.AppendLine($"[{i}] {tagGroup.Name}");

                    for (var j = 0; j < tagGroup.Tags.Length; ++j)
                    {
                        var tag = tagGroup.Tags[j];
                        builder.AppendLine($"  [{i}:{j}] {tag.Name}");

                        foreach (var parameter in tag.Parameters)
                        {
                            builder.Append($"    {parameter.Name} [{parameter.Type}]");
                            if (parameter.Type == ValueType.Enum) builder.Append($" {{{string.Join(",", parameter.EnumValues)}}}");
                            builder.AppendLine();
                        }
                    }
                }
            }

            if (model.Styles.Length > 0)
            {
                builder.AppendLine();
                builder.AppendLine("Style Definitions (SYL3/SLB1)");
                builder.AppendLine("=============================");
                foreach (var style in model.Styles)
                {
                    builder.AppendLine($"{style.Name}: RegionWidth={style.RegionWidth}, FontIndex={style.FontIndex}, BaseColorIndex={style.BaseColorIndex}, LineNumber={style.LineNumber}");
                }
            }

            if (model.SourceFiles.Length > 0)
            {
                builder.AppendLine();
                builder.AppendLine("Source Files (CTI1)");
                builder.AppendLine("===================");
                foreach (var sourceFile in model.SourceFiles)
                {
                    builder.AppendLine(sourceFile);
                }
            }

            return builder.ToString();
        }

        public Task Save(MsbpFileData file, string content) => Task.CompletedTask;
    }
    #endregion
}