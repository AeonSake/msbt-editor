﻿using NintendoTools.FileFormats.Msbp;

namespace MsbtEditor;

internal sealed class MsbpFileData(StorageProvider storageProvider, ContainerFileData? parent = null) : FileData(storageProvider, parent)
{
    #region private members
    private static readonly MsbpFileParser Parser = new();
    #endregion

    #region public properties
    public override bool IsReadOnly => true;
    #endregion

    #region public methods
    public Task<MsbpFile> LoadModel() => Read(Parser.Parse);
    #endregion
}