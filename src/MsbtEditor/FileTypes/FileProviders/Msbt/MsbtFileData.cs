﻿using NintendoTools.FileFormats.Msbt;

namespace MsbtEditor;

internal sealed class MsbtFileData(StorageProvider storageProvider, GameConfigService gameConfigService, ContainerFileData? parent = null) : FileData(storageProvider, parent)
{
    #region private members
    private static readonly MsbtFileParser Parser = new();
    #endregion

    #region public methods
    public Task<MsbtFile> LoadModel() => Read(Parser.Parse);

    public Task SaveModel(MsbtFile file) => Write(stream =>
    {
        var compiler = new MsbtFileCompiler
        {
            FixedLabelGroups = gameConfigService.CurrentConfig.Msbt.FixedLabelGroups,
            Newline = gameConfigService.CurrentConfig.Msbt.Newline
        };
        compiler.Compile(file, stream);
    });
    #endregion
}