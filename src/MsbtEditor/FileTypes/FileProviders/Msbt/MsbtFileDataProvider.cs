﻿using NintendoTools.FileFormats.Msbt;

namespace MsbtEditor;

internal sealed class MsbtFileDataProvider : IFileDataProvider
{
    #region private members
    private static readonly IconValue Icon = new("""<path d="M13 14l7 -0"/><path d="M16.5 14l0 8"/><path d="M7 22l-3 -0l0 -8l3 -0c1.104 -0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 -0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M7 18l-3 -0"/><path d="M15.5 8c0 1.104 1.12 2 2.5 2c1.38 0 2.5 -0.896 2.5 -2c0 -1.104 -0.831 -1.657 -2.5 -2c-1.927 -0.397 -2.5 -0.896 -2.5 -2c0 -1.104 1.12 -2 2.5 -2c1.38 0 2.5 0.896 2.5 2"/><path d="M3.5 10l0 -8l4 4l4 -4l0 8"/>""");
    private readonly GameConfigService _gameConfigService;
    #endregion

    #region constructor
    public MsbtFileDataProvider(GameConfigService gameConfigService, FileIconProvider iconProvider, TextEditorProvider textEditorProvider)
    {
        _gameConfigService = gameConfigService;

        iconProvider.Register<MsbtFileData>(Icon, "#599dd4");
        textEditorProvider.Register(new MsbtEditorContentHandler(gameConfigService));
    }
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream) => MsbtFileParser.CanParseStatic(stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new MsbtFileData(provider, _gameConfigService, parent);
    #endregion

    #region helper classes
    private class MsbtEditorContentHandler(GameConfigService gameConfigService) : ITextEditorContentHandler<MsbtFileData>
    {
        public string Language => "msbt";

        public string DisplayName => "MSBT";

        public int TabSize => 2;

        public async Task<string> Load(MsbtFileData file)
        {
            var model = await file.LoadModel();
            return TextConverter.SerializeMsbt(model, gameConfigService.CurrentConfig.Msbt.TagMap);
        }

        public async Task Save(MsbtFileData file, string content)
        {
            var model = TextConverter.DeserializeMsbt(content, gameConfigService.CurrentConfig.Msbt.TagMap);
            await file.SaveModel(model);
        }
    }
    #endregion
}