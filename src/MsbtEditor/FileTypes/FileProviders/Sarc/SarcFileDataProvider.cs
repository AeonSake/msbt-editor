﻿using NintendoTools.FileFormats.Sarc;

namespace MsbtEditor;

internal sealed class SarcFileDataProvider : IFileDataProvider
{
    #region private members
    private static readonly IconValue Icon = new("""<path d="M19.5 21.464c-0.588 0.341 -1.272 0.536 -2 0.536c-2.208 -0 -4 -1.792 -4 -4c-0 -2.208 1.792 -4 4 -4c0.728 -0 1.412 0.195 2 0.536"/><path d="M4.5 14l0 8"/><path d="M4.5 14l3 0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c0 -0 2 4 2 4"/><path d="M7.5 18l-3 -0"/><path d="M13 10l3.5 -8l3.5 8"/><path d="M19 8l-5 0"/><path d="M4 8c0 1.104 1.12 2 2.5 2c1.38 0 2.5 -0.896 2.5 -2c0 -1.104 -0.831 -1.657 -2.5 -2c-1.927 -0.397 -2.5 -0.896 -2.5 -2c0 -1.104 1.12 -2 2.5 -2c1.38 0 2.5 0.896 2.5 2"/>""");
    private readonly GameConfigService _gameConfigService;
    #endregion

    #region constructor
    public SarcFileDataProvider(GameConfigService gameConfigService, FileIconProvider iconProvider)
    {
        _gameConfigService = gameConfigService;

        iconProvider.Register<SarcFileData>(Icon, "#54c8b0");
    }
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream) => SarcFileParser.CanParseStatic(stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new SarcFileData(provider, fileFactory, _gameConfigService, parent);
    #endregion
}