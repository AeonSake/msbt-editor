using NintendoTools.FileFormats;
using NintendoTools.FileFormats.Sarc;

namespace MsbtEditor;

internal sealed class SarcFileData(StorageProvider storageProvider, FileDataFactory fileFactory, GameConfigService gameConfigService, ContainerFileData? parent = null) : ContainerFileData(storageProvider, parent), IAlignedContainerFileData
{
    #region private members
    private static readonly SarcFileParser Parser = new();
    private SarcFile? _fileData;
    #endregion

    #region public methods
    public async Task<List<AlignmentInfo>> CheckAlignment()
    {
        var config = gameConfigService.CurrentConfig.Sarc.FindMatch(GetFilePath());

        await using var readStream = GetReadStream();
        return SarcFileParser.CheckAlignmentStatic(readStream, config.Alignment);
    }
    #endregion

    #region protected methods
    protected override async Task<List<FileData>> LoadChildren()
    {
        await using var readStream = GetReadStream();
        var model = Parser.Parse(readStream);
        var tempDir = GetTempDir();

        foreach (var content in model.Files)
        {
            var directory = Path.GetDirectoryName(content.Name);
            if (content.Name.Equals(directory)) continue;
            Directory.CreateDirectory(Path.Combine(tempDir, directory!));
        }

        var files = new FileData[model.Files.Count];
        await Parallel.ForAsync(0, files.Length, async (i, _) =>
        {
            var content = model.Files[i];
            var provider = new FileStorageProvider(Path.Combine(tempDir, content.Name), true);
            await using var writeStream = provider.GetWriteStream();
            writeStream.Write(content.Data);
            writeStream.Close();

            files[i] = fileFactory.Create(provider);
        });

        _fileData = model;
        _fileData.Files = []; //release memory

        return FolderBuilder.Wrap(files, tempDir);
    }

    protected override async Task InternalSave()
    {
        if (_fileData is null) return;

        var model = new SarcFile
        {
            BigEndian = _fileData.BigEndian,
            Version = _fileData.Version,
            HashKey = _fileData.HashKey,
            HasFileNames = _fileData.HasFileNames
        };

        var internalFiles = FolderBuilder.BuildMap(Children).Values.ToArray();
        var files = new SarcContent[internalFiles.Length];
        await Parallel.ForAsync(0, files.Length, async (i, token) =>
        {
            var file = internalFiles[i];
            await file.Save();
            files[i] = new SarcContent
            {
                Name = file.GetFilePath()[(InternalFilePath.Length + 1)..],
                Data = await File.ReadAllBytesAsync(file.InternalFilePath, token)
            };
        });
        model.Files = [..files];

        var config = gameConfigService.CurrentConfig.Sarc.FindMatch(GetFilePath());

        if (config.CheckAlignment)
        {
            await using var readStream = GetReadStream();
            foreach (var info in SarcFileParser.CheckAlignmentStatic(readStream, config.Alignment))
            {
                if (!info.IsValid) throw new FileFormatException("One or more file alignments don't match with the provided alignment table.");
            }
        }

        var compiler = new SarcFileCompiler {Alignment = config.Alignment};
        await using var writeStream = GetWriteStream();
        compiler.Compile(model, writeStream);
    }
    #endregion
}