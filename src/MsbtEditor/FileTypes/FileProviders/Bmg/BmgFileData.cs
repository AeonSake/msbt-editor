﻿using NintendoTools.FileFormats.Bmg;

namespace MsbtEditor;

internal sealed class BmgFileData(StorageProvider storageProvider, GameConfigService gameConfigService, ContainerFileData? parent = null) : FileData(storageProvider, parent)
{
    #region private members
    private static readonly BmgFileParser Parser = new();
    #endregion

    #region public methods
    public Task<BmgFile> LoadModel() => Read(Parser.Parse);

    public Task SaveModel(BmgFile file) => Write(stream =>
    {
        var compiler = new BmgFileCompiler {Newline = gameConfigService.CurrentConfig.Msbt.Newline};
        compiler.Compile(file, stream);
    });
    #endregion
}