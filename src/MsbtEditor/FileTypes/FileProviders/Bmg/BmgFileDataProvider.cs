﻿using NintendoTools.FileFormats.Bmg;

namespace MsbtEditor;

internal sealed class BmgFileDataProvider : IFileDataProvider
{
    #region private members
    private static readonly IconValue Icon = new("""<path d="M20.5 13l2.5 0l0 3"/><path d="M22.905 13c-0.333 1.724 -1.508 3 -2.905 3c-1.656 0 -3 -1.792 -3 -4c-0 -2.208 1.344 -4 3 -4c0.947 -0 1.791 0.586 2.341 1.5"/><path d="M9 16l0 -8l2.5 4l2.5 -4l-0 8"/><path d="M4 16l-3 0l0 -8l3 -0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M4 12l-3 0"/>""");
    private readonly GameConfigService _gameConfigService;
    #endregion

    #region constructor
    public BmgFileDataProvider(GameConfigService gameConfigService, FileIconProvider iconProvider, TextEditorProvider textEditorProvider)
    {
        _gameConfigService = gameConfigService;

        iconProvider.Register<BmgFileData>(Icon, "#10a34e");
        textEditorProvider.Register(new BmgEditorContentHandler(gameConfigService));
    }
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream) => BmgFileParser.CanParseStatic(stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new BmgFileData(provider, _gameConfigService, parent);
    #endregion

    #region helper classes
    private class BmgEditorContentHandler(GameConfigService gameConfigService) : ITextEditorContentHandler<BmgFileData>
    {
        public string Language => "msbt";

        public string DisplayName => "BMG";

        public int TabSize => 2;

        public async Task<string> Load(BmgFileData file)
        {
            var model = await file.LoadModel();
            return TextConverter.SerializeBmg(model, gameConfigService.CurrentConfig.Bmg.TagMap);
        }

        public async Task Save(BmgFileData file, string content)
        {
            var model = TextConverter.DeserializeBmg(content, gameConfigService.CurrentConfig.Bmg.TagMap);
            await file.SaveModel(model);
        }
    }
    #endregion
}