using System.IO.Compression;

namespace MsbtEditor;

internal sealed class ZipFileData(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) : ContainerFileData(provider, parent)
{
    #region protected methods
    protected override Task<List<FileData>> LoadChildren() => Task.Run(() =>
    {
        using var readStream = GetReadStream();
        var tempDir = GetTempDir();
        ZipFile.ExtractToDirectory(readStream, tempDir);

        var directories = Directory.GetDirectories(tempDir);
        var files = Directory.GetFiles(tempDir);
        var children = new FileData[directories.Length + files.Length];

        Parallel.For(0, directories.Length, i => children[i] = new FolderFileData(directories[i], fileFactory, true, this));
        Parallel.For(0, files.Length, i =>
        {
            var provider = new FileStorageProvider(new FileInfo(files[i]), true);
            children[directories.Length + i] = fileFactory.Create(provider, this);
        });

        return children.ToList();
    });

    protected override async Task InternalSave()
    {
        await Parallel.ForEachAsync(Children, (file, _) => new ValueTask(file.Save()));

        await using var writeStream = GetWriteStream();
        ZipFile.CreateFromDirectory(GetTempDir(), writeStream);
    }
    #endregion
}