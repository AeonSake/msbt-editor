﻿namespace MsbtEditor;

internal sealed class ZipFileDataProvider : IFileDataProvider
{
    #region constructor
    public ZipFileDataProvider(FileIconProvider iconProvider) => iconProvider.Register<ZipFileData>(GlobalIcons.Archive, "#0cc9c9");
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream)
    {
        stream.Seek(0, SeekOrigin.Begin);

        Span<byte> buffer = stackalloc byte[4];
        _ = stream.Read(buffer);
        return buffer is [0x50, 0x4B, 0x03, 0x04];
    }

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new ZipFileData(provider, fileFactory, parent);
    #endregion
}