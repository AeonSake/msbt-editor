﻿using NintendoTools.FileFormats.Rarc;

namespace MsbtEditor;

internal sealed class RarcFileDataProvider : IFileDataProvider
{
    #region private members
    private static readonly IconValue Icon = new("""<path d="M19.5 21.464c-0.588 0.341 -1.272 0.536 -2 0.536c-2.208 -0 -4 -1.792 -4 -4c-0 -2.208 1.792 -4 4 -4c0.728 -0 1.412 0.195 2 0.536"/><path d="M4.5 14l0 8"/><path d="M4.5 14l3 0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c0 -0 2 4 2 4"/><path d="M7.5 18l-3 -0"/><path d="M13 10l3.5 -8l3.5 8"/><path d="M19 8l-5 0"/><path d="M4.5 2l0 8"/><path d="M4.5 2l3 0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c0 -0 2 4 2 4"/><path d="M7.5 6l-3 -0"/>""");
    #endregion

    #region constructor
    public RarcFileDataProvider(FileIconProvider iconProvider) => iconProvider.Register<RarcFileData>(Icon, "#4fe8ad");
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream) => RarcFileParser.CanParseStatic(stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new RarcFileData(provider, fileFactory, parent);
    #endregion
}