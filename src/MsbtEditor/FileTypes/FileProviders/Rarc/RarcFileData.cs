﻿using System.Collections.Concurrent;
using NintendoTools.FileFormats.Rarc;

namespace MsbtEditor;

internal sealed class RarcFileData(StorageProvider storageProvider, FileDataFactory fileFactory, ContainerFileData? parent = null) : ContainerFileData(storageProvider, parent)
{
    #region private members
    private static readonly RarcFileParser Parser = new();
    private static readonly RarcFileCompiler Compiler = new();
    private RarcFile? _fileData;
    private IDictionary<FileData, PreloadType> _metadata = new Dictionary<FileData, PreloadType>();
    #endregion

    #region protected methods
    protected override async Task<List<FileData>> LoadChildren()
    {
        await using var readStream = GetReadStream();
        var model = Parser.Parse(readStream);

        var metadata = new ConcurrentDictionary<FileData, PreloadType>();
        var children = await BuildFiles([model.RootNode], GetTempDir());

        _fileData = model;
        _fileData.RootNode.Children = []; //release memory
        _metadata = metadata;

        return children;

        async Task<List<FileData>> BuildFiles(IList<IRarcNode> nodes, string path)
        {
            var files = new FileData[nodes.Count];

            await Parallel.ForAsync(0, files.Length, async (i, _) =>
            {
                var node = nodes[i];
                var filePath = Path.Combine(path, node.Name);

                if (node is RarcFileNode fileNode)
                {
                    var provider = new FileStorageProvider(filePath, true);
                    await using var writeStream = provider.GetWriteStream();
                    writeStream.Write(fileNode.Data);
                    writeStream.Close();

                    var file = fileFactory.Create(provider);
                    files[i] = file;
                    metadata.TryAdd(file, fileNode.Preload);
                }
                else if (node is RarcDirectoryNode dirNode)
                {
                    Directory.CreateDirectory(filePath);
                    var childFiles = await BuildFiles(dirNode.Children, filePath);

                    files[i] = new FolderFileData(filePath, childFiles, true);
                }
            });

            return [..files];
        }
    }

    protected override async Task InternalSave()
    {
        if (_fileData is null) return;

        var rootNodes = await BuildNodes(Children);
        if (rootNodes.Count == 0) throw new FileFormatException("Missing root node.");
        if (rootNodes.Count > 1) throw new FileFormatException("RARC archive may contain only one root node");
        if (rootNodes[0] is not RarcDirectoryNode rootNode) throw new FileFormatException("RARC root node must be a directory.");

        var model = new RarcFile
        {
            SyncedIds = _fileData.SyncedIds,
            RootNode = rootNode
        };

        await using var writeStream = GetWriteStream();
        Compiler.Compile(model, writeStream);
        return;

        async Task<List<IRarcNode>> BuildNodes(IReadOnlyList<FileData> files)
        {
            var nodes = new IRarcNode[files.Count];

            await Parallel.ForAsync(0, files.Count, async (i, token) =>
            {
                var file = files[i];

                if (file is FolderFileData folder)
                {
                    nodes[i] = new RarcDirectoryNode
                    {
                        Name = file.Name,
                        Children = await BuildNodes(folder.Children)
                    };
                }
                else
                {
                    await file.Save();
                    nodes[i] = new RarcFileNode
                    {
                        Name = file.Name,
                        Preload = _metadata.TryGetValue(file, out var preloadType) ? preloadType : throw new InvalidDataException($"Missing meta data for {file.Name}."),
                        Data = await File.ReadAllBytesAsync(file.InternalFilePath, token)
                    };
                }
            });

            return [..nodes];
        }
    }
    #endregion
}