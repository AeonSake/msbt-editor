﻿using Newtonsoft.Json;
using YamlConverter;

namespace MsbtEditor;

public sealed class GameConfigFileData(StorageProvider storageProvider, ContainerFileData? parent = null) : FileData(storageProvider, parent)
{
    #region private members
    internal static readonly JsonSerializerSettings JsonSettings = new()
    {
        ContractResolver = ContractResolver.Instance,
        Converters =
        [
            new NumberConverter<byte>(),
            new NumberConverter<sbyte>(),
            new NumberConverter<short>(),
            new NumberConverter<ushort>(),
            new NumberConverter<int>(),
            new NumberConverter<uint>(),
            new NumberConverter<long>(),
            new NumberConverter<ulong>()
        ]
    };
    #endregion

    #region public methods
    public Task<GameConfig> LoadModel() => Read(async stream =>
    {
        using var reader = new StreamReader(stream);
        var content = await reader.ReadToEndAsync();
        var config = YamlConvert.DeserializeObject<GameConfig>(content, JsonSettings) ?? new GameConfig();
        config.Validate();
        return config;
    });

    public Task SaveModel(GameConfig config) => Write(async stream =>
    {
        config.Validate();

        var content = YamlConvert.SerializeObject(config, JsonSettings);
        await using var writer = new StreamWriter(stream);
        await writer.WriteAsync(content);
    });

    public Task<string> LoadContent() => Read(async stream =>
    {
        using var reader = new StreamReader(stream);
        return await reader.ReadToEndAsync();
    });

    public Task SaveContent(string content) => Write(async stream =>
    {
        //validate content before writing
        var config = YamlConvert.DeserializeObject<GameConfig>(content, JsonSettings);
        config?.Validate();

        await using var writer = new StreamWriter(stream);
        await writer.WriteAsync(content);
    });
    #endregion
}