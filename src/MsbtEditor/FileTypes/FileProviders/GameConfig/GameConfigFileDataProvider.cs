﻿namespace MsbtEditor;

internal sealed class GameConfigFileDataProvider : IFileDataProvider
{
    #region constructor
    public GameConfigFileDataProvider(FileIconProvider iconProvider, TextEditorProvider textEditorProvider)
    {
        iconProvider.Register<GameConfigFileData>(GlobalIcons.ConfigFile, "#6b5bb3");
        textEditorProvider.Register(new GameConfigEditorContentHandler());
    }
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream)
    {
        //file is not read by the factory but by the GameConfigService
        return false;
    }

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new GameConfigFileData(provider, parent);
    #endregion

    #region helper classes
    private class GameConfigEditorContentHandler : ITextEditorContentHandler<GameConfigFileData>
    {
        public string Language => "yaml";

        public string DisplayName => "YAML";

        public int TabSize => 2;

        public Task<string> Load(GameConfigFileData file) => file.LoadContent();

        public Task Save(GameConfigFileData file, string content) => file.SaveContent(content);
    }
    #endregion
}