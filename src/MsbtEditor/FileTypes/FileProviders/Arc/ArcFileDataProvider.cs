﻿using NintendoTools.FileFormats.Arc;

namespace MsbtEditor;

internal sealed class ArcFileDataProvider : IFileDataProvider
{
    #region private members
    private static readonly IconValue Icon = new("""<path d="M23 15.464c-0.49 0.341 -1.06 0.536 -1.667 0.536c-1.839 -0 -3.333 -1.792 -3.333 -4c0 -2.208 1.494 -4 3.333 -4c0.607 -0 1.177 0.195 1.667 0.536"/><path d="M10 8l0 8"/><path d="M10 8l3 0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c0 0 2 4 2 4"/><path d="M13 12l-3 0"/><path d="M1 16l3 -8l3 8"/><path d="M6 14l-4 0"/>""");
    #endregion

    #region constructor
    public ArcFileDataProvider(FileIconProvider iconProvider) => iconProvider.Register<ArcFileData>(Icon, "#a87ed5");
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream) => ArcFileParser.CanParseStatic(stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new ArcFileData(provider, fileFactory, parent);
    #endregion
}