﻿using NintendoTools.FileFormats.Arc;

namespace MsbtEditor;

internal sealed class ArcFileData(StorageProvider storageProvider, FileDataFactory fileFactory, ContainerFileData? parent = null) : ContainerFileData(storageProvider, parent)
{
    #region private members
    private static readonly ArcFileParser Parser = new();
    private static readonly ArcFileCompiler Compiler = new();
    #endregion

    #region protected methods
    protected override async Task<List<FileData>> LoadChildren()
    {
        await using var readStream = GetReadStream();
        var model = Parser.Parse(readStream);

        return await BuildFiles(model.Content, GetTempDir());

        async Task<List<FileData>> BuildFiles(IList<IArcNode> nodes, string path)
        {
            var files = new FileData[nodes.Count];

            await Parallel.ForAsync(0, files.Length, async (i, _) =>
            {
                var node = nodes[i];
                var filePath = Path.Combine(path, node.Name);

                if (node is ArcFileNode fileNode)
                {
                    var provider = new FileStorageProvider(filePath, true);
                    await using var writeStream = provider.GetWriteStream();
                    writeStream.Write(fileNode.Data);
                    writeStream.Close();

                    files[i] = fileFactory.Create(provider);
                }
                else if (node is ArcDirectoryNode dirNode)
                {
                    Directory.CreateDirectory(filePath);
                    var childFiles = await BuildFiles(dirNode.Children, filePath);

                    files[i] = new FolderFileData(filePath, childFiles, true);
                }
            });

            return [..files];
        }
    }

    protected override async Task InternalSave()
    {
        var model = new ArcFile {Content = await BuildNodes(Children)};

        await using var writeStream = GetWriteStream();
        Compiler.Compile(model, writeStream);
        return;

        async Task<List<IArcNode>> BuildNodes(IReadOnlyList<FileData> files)
        {
            var nodes = new IArcNode[files.Count];

            await Parallel.ForAsync(0, files.Count, async (i, token) =>
            {
                var file = files[i];

                if (file is FolderFileData folder)
                {
                    nodes[i] = new ArcDirectoryNode
                    {
                        Name = file.Name,
                        Children = await BuildNodes(folder.Children)
                    };
                }
                else
                {
                    await file.Save();
                    nodes[i] = new ArcFileNode
                    {
                        Name = file.Name,
                        Data = await File.ReadAllBytesAsync(file.InternalFilePath, token)
                    };
                }
            });

            return [..nodes];
        }
    }
    #endregion
}