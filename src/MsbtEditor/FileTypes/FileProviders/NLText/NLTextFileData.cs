﻿using System.Text;
using NintendoTools.Utils;

namespace MsbtEditor;

internal sealed class NLTextFileData(StorageProvider storageProvider, ContainerFileData? parent = null) : FileData(storageProvider, parent)
{
    #region public methods
    public Task<string[]> LoadModel() => Read(stream =>
    {
        using var reader = new FileReader(stream);
        if (reader.ReadStringAt(0, 4) != "TEXT") throw new InvalidDataException("File is not a Neverland Text file.");

        var count = reader.ReadUInt32();
        var content = new string[count];
        for (var i = 0; i < count; ++i)
        {
            reader.JumpTo(8 + i * 8);
            var length = reader.ReadInt32();
            var offset = reader.ReadUInt32();
            content[i] = reader.ReadStringAt(offset, length, Encoding.UTF8);
        }

        return content;
    });

    public Task SaveModel(string[] content) => Write(stream =>
    {
        //header
        using var writer = new FileWriter(stream);
        writer.Write("TEXT", Encoding.ASCII);
        writer.Write(content.Length);

        //string size table
        var tableOffset = 8 + content.Length * 8;
        var spacing = new byte[InternalFilePath.EndsWith(".jpn") ? 2 : 1]; //no idea why it varies
        foreach (var stringValue in content)
        {
            var length = Encoding.UTF8.GetByteCount(stringValue);
            writer.Write(length);
            writer.Write(tableOffset);
            tableOffset += length + spacing.Length;
        }

        //string table
        foreach (var stringValue in content)
        {
            writer.Write(stringValue, Encoding.UTF8);
            writer.Write(spacing);
        }
    });
    #endregion
}