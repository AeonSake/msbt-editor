﻿using System.Text;
using System.Text.RegularExpressions;
using NintendoTools.Utils;

namespace MsbtEditor;

internal sealed partial class NLTextFileDataProvider : IFileDataProvider
{
    #region constructor
    public NLTextFileDataProvider(FileIconProvider iconProvider, TextEditorProvider textEditorProvider)
    {
        iconProvider.Register<NLTextFileData>(GlobalIcons.TextFile, "#cccccc");
        textEditorProvider.Register(new NLTextEditorContentHandler());
    }
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream)
    {
        var reader = new FileReader(stream);
        return reader.BaseStream.Length > 4 && reader.ReadStringAt(0, 4, Encoding.ASCII) == "TEXT";
    }

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new NLTextFileData(provider, parent);
    #endregion

    #region helper classes
    private partial class NLTextEditorContentHandler : ITextEditorContentHandler<NLTextFileData>
    {
        public string Language => "nltxt";

        public string DisplayName => "Neverland Text";

        public async Task<string> Load(NLTextFileData file)
        {
            var model = await file.LoadModel();

            var sb = new StringBuilder();
            var labelFormat = $"--- {{0:D{model.Length.ToString().Length}}} ---";
            for (var i = 0; i < model.Length; ++i)
            {
                sb.AppendLine(string.Format(labelFormat, i));
                sb.AppendLine(TemplateValueReadRegex().Replace(model[i], m => "{{" + m.Groups[1].Value + "}}"));
            }

            return sb.ToString();
        }

        public async Task Save(NLTextFileData file, string content)
        {
            //parse editor content
            var strings = new List<string>();
            var sb = new StringBuilder();
            var hasSeparator = false;
            var firstLine = true;
            using var reader = new StringReader(content);
            while (true)
            {
                var line = await reader.ReadLineAsync();
                if (line is null) break;

                if (LineSeparatorRegex().IsMatch(line))
                {
                    if (hasSeparator) strings.Add(sb.ToString());
                    sb.Clear();
                    hasSeparator = true;
                    firstLine = true;
                }
                else
                {
                    if (!firstLine) sb.Append('\n');
                    sb.Append(line);
                    firstLine = false;
                }
            }
            if (hasSeparator)
            {
                var str = sb.ToString();
                strings.Add(str.EndsWith('\n') ? str[..^1] : str);
            }

            //replace template transformation
            for (var i = 0; i < strings.Count; ++i)
            {
                strings[i] = TemplateValueWriteRegex().Replace(strings[i], m => $@"\uFF20{m.Groups[1].Value}\uFF20");
            }

            await file.SaveModel(strings.ToArray());
        }

        [GeneratedRegex(@"^---\s+\d+\s+---$")]
        private partial Regex LineSeparatorRegex();

        [GeneratedRegex(@"\uFF20([^\uFF20]*)\uFF20")]
        private partial Regex TemplateValueReadRegex();

        [GeneratedRegex(@"\{\{([^\{\}]*)\}\}")]
        private partial Regex TemplateValueWriteRegex();
    }
    #endregion
}