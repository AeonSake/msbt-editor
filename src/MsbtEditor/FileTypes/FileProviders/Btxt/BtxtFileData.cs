﻿using System.Text;
using NintendoTools.Utils;

namespace MsbtEditor;

internal sealed class BtxtFileData(StorageProvider storageProvider, ContainerFileData? parent = null) : FileData(storageProvider, parent)
{
    #region public methods
    public Task<BtxtFile> LoadModel() => Read(stream =>
    {
        using var reader = new FileReader(stream);
        if (reader.ReadStringAt(0, 4) != "BTXT") throw new InvalidDataException("File is not a Binary Text file.");

        var file = new BtxtFile
        {
            VersionMajor = reader.ReadUInt16(),
            VersionMinor = reader.ReadUInt16()
        };

        while (reader.Position < reader.BaseStream.Length)
        {
            file.Entries.Add(new TextEntry
            {
                Label = reader.ReadTerminatedString(Encoding.ASCII),
                Content = reader.ReadTerminatedString(Encoding.Unicode).Replace('|', '\n')
            });
        }

        return file;
    });

    public Task SaveModel(BtxtFile file) => Write(stream =>
    {
        using var writer = new FileWriter(stream);
        writer.Write("BTXT", Encoding.ASCII);
        writer.Write(file.VersionMajor);
        writer.Write(file.VersionMinor);

        foreach (var entry in file.Entries)
        {
            writer.WriteTerminated(entry.Label, Encoding.ASCII);
            writer.WriteTerminated(entry.Content.Replace('\n', '|'), Encoding.Unicode);
        }
    });
    #endregion

    #region helper classes
    public class BtxtFile
    {
        public ushort VersionMajor { get; set; } = 1;

        public ushort VersionMinor { get; set; }

        public List<TextEntry> Entries { get; set; } = [];
    }

    public class TextEntry
    {
        public required string Label { get; set; }

        public required string Content { get; set; }
    }
    #endregion
}