﻿using System.Text;
using System.Text.RegularExpressions;
using NintendoTools.Utils;

namespace MsbtEditor;

internal sealed partial class BtxtFileDataProvider : IFileDataProvider
{
    #region constructor
    public BtxtFileDataProvider(FileIconProvider iconProvider, TextEditorProvider textEditorProvider)
    {
        iconProvider.Register<BtxtFileData>(GlobalIcons.TextFile, "#cccccc");
        textEditorProvider.Register(new BtxtEditorContentHandler());
    }
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream)
    {
        var reader = new FileReader(stream);
        return reader.BaseStream.Length > 4 && reader.ReadStringAt(0, 4, Encoding.ASCII) == "BTXT";
    }

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new BtxtFileData(provider, parent);
    #endregion

    #region helper classes
    private partial class BtxtEditorContentHandler : ITextEditorContentHandler<BtxtFileData>
    {
        public string Language => "btxt";

        public string DisplayName => "BTXT";

        public async Task<string> Load(BtxtFileData file)
        {
            var model = await file.LoadModel();

            var sb = new StringBuilder();
            sb.Append("version: ").Append(model.VersionMajor).Append('.').Append(model.VersionMinor).AppendLine().AppendLine();

            foreach (var entry in model.Entries)
            {
                sb.Append("--- ").Append(entry.Label).AppendLine(" ---");
                sb.AppendLine(entry.Content);
            }

            return sb.ToString();
        }

        public async Task Save(BtxtFileData file, string content)
        {
            var model = new BtxtFileData.BtxtFile();

            var versionFound = false;
            string? currentLabel = null;
            var currentText = new StringBuilder();
            var firstLine = true;
            using var reader = new StringReader(content);
            while (true)
            {
                var line = await reader.ReadLineAsync();
                if (line is null) break;

                if (!versionFound)
                {
                    var match = VersionRegex().Match(line);
                    if (!match.Success) continue;

                    model.VersionMajor = ushort.Parse(match.Groups[1].Value);
                    model.VersionMinor = ushort.Parse(match.Groups[2].Value);
                    versionFound = true;
                }
                else
                {
                    var match = MessageSeparatorRegex().Match(line);
                    if (match.Success)
                    {
                        if (currentLabel is not null)
                        {
                            model.Entries.Add(new BtxtFileData.TextEntry
                            {
                                Label = currentLabel,
                                Content = currentText.ToString()
                            });
                        }

                        currentLabel = match.Groups[1].Value;
                        currentText.Clear();
                        firstLine = true;
                    }
                    else
                    {
                        if (!firstLine) currentText.Append('\n');
                        currentText.Append(line);
                        firstLine = false;
                    }
                }
            }
            if (!versionFound) throw new InvalidDataException("Missing version field.");
            if (currentLabel is not null)
            {
                var str = currentText.ToString();
                model.Entries.Add(new BtxtFileData.TextEntry
                {
                    Label = currentLabel,
                    Content = str.EndsWith('\n') ? str[..^1] : str
                });
            }

            await file.SaveModel(model);
        }

        [GeneratedRegex(@"^version: (\d+)\.(\d+)$")]
        private partial Regex VersionRegex();

        [GeneratedRegex(@"^---\s+([!-~]+)\s+---$")]
        private partial Regex MessageSeparatorRegex();
    }
    #endregion
}