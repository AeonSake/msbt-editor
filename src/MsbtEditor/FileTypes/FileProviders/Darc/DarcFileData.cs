﻿using NintendoTools.FileFormats;
using NintendoTools.FileFormats.Darc;

namespace MsbtEditor;

internal sealed class DarcFileData : ContainerFileData, IAlignedContainerFileData
{
    #region private members
    private static readonly DarcFileParser Parser = new();
    private readonly FileDataFactory _fileFactory;
    private readonly GameConfigService _gameConfigService;
    private DarcFile? _fileData;
    #endregion

    #region constructor
    internal DarcFileData(StorageProvider storageProvider, FileDataFactory fileFactory, GameConfigService gameConfigService, ContainerFileData? parent = null) : base(storageProvider, parent)
    {
        _fileFactory = fileFactory;
        _gameConfigService = gameConfigService;
    }
    #endregion

    #region public methods
    public async Task<List<AlignmentInfo>> CheckAlignment()
    {
        var config = _gameConfigService.CurrentConfig.Darc.FindMatch(GetFilePath());

        await using var readStream = GetReadStream();
        return DarcFileParser.CheckAlignmentStatic(readStream, config.Alignment);
    }
    #endregion

    #region protected methods
    protected override async Task<List<FileData>> LoadChildren()
    {
        await using var readStream = GetReadStream();
        var model = Parser.Parse(readStream);

        var children = await BuildFiles(model.Content, GetTempDir());

        _fileData = model;
        _fileData.Content = []; //release memory

        return children;

        async Task<List<FileData>> BuildFiles(IList<IDarcNode> nodes, string path)
        {
            var files = new FileData[nodes.Count];

            await Parallel.ForAsync(0, files.Length, async (i, _) =>
            {
                var node = nodes[i];
                var filePath = Path.Combine(path, node.Name);

                if (node is DarcFileNode fileNode)
                {
                    var provider = new FileStorageProvider(filePath, true);
                    await using var writeStream = provider.GetWriteStream();
                    writeStream.Write(fileNode.Data);
                    writeStream.Close();

                    files[i] = _fileFactory.Create(provider);
                }
                else if (node is DarcDirectoryNode dirNode)
                {
                    Directory.CreateDirectory(filePath);
                    var childFiles = await BuildFiles(dirNode.Children, filePath);

                    files[i] = new FolderFileData(filePath, childFiles, true);
                }
            });

            return [..files];
        }
    }

    protected override async Task InternalSave()
    {
        if (_fileData is null) return;

        var model = new DarcFile
        {
            BigEndian = _fileData.BigEndian,
            Version = _fileData.Version,
            Content = await BuildNodes(Children)
        };

        var config = _gameConfigService.CurrentConfig.Darc.FindMatch(GetFilePath());

        if (config.CheckAlignment)
        {
            await using var readStream = GetReadStream();
            foreach (var info in DarcFileParser.CheckAlignmentStatic(readStream, config.Alignment))
            {
                if (!info.IsValid) throw new FileFormatException("One or more file alignments don't match with the provided alignment table.");
            }
        }

        var compiler = new DarcFileCompiler {Alignment = config.Alignment};
        await using var writeStream = GetWriteStream();
        compiler.Compile(model, writeStream);
        return;

        async Task<List<IDarcNode>> BuildNodes(IReadOnlyList<FileData> files)
        {
            var nodes = new IDarcNode[files.Count];

            await Parallel.ForAsync(0, files.Count, async (i, token) =>
            {
                var file = files[i];

                if (file is FolderFileData folder)
                {
                    nodes[i] = new DarcDirectoryNode
                    {
                        Name = file.Name,
                        Children = await BuildNodes(folder.Children)
                    };
                }
                else
                {
                    await file.Save();
                    nodes[i] = new DarcFileNode
                    {
                        Name = file.Name,
                        Data = await File.ReadAllBytesAsync(file.InternalFilePath, token)
                    };
                }
            });

            return [..nodes];
        }
    }
    #endregion
}