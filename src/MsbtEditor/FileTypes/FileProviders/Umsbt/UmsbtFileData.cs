﻿using NintendoTools.FileFormats.Msbt;
using NintendoTools.FileFormats.Umsbt;

namespace MsbtEditor;

internal sealed class UmsbtFileData(StorageProvider storageProvider, FileDataFactory fileFactory, ContainerFileData? parent = null) : ContainerFileData(storageProvider, parent)
{
    #region private members
    private static readonly UmsbtFileParser Parser = new();
    private static readonly UmsbtFileCompiler Compiler = new() {Compiler = {FixedLabelGroups = true}}; //always serialize with the exact label groups
    #endregion

    #region public methods
    public override bool CanAdd(FileData file) => file is MsbtFileData;
    #endregion

    #region protected methods
    protected override Task<List<FileData>> LoadChildren() => Task.Run(() =>
    {
        using var readStream = GetReadStream();
        var model = Parser.Parse(readStream);
        var tempDir = GetTempDir();

        var files = new FileData[model.Count];
        Parallel.For(0, model.Count, i =>
        {
            var provider = new FileStorageProvider(Path.Combine(tempDir, $"{i}.msbt"), true);
            using var writeStream = provider.GetWriteStream();
            Compiler.Compiler.Compile(model[i], writeStream);
            writeStream.Close();

            files[i] = fileFactory.Create(provider, this);
        });

        return files.ToList();
    });

    protected override Task InternalSave() => Task.Run(async () =>
    {
        var model = new MsbtFile[Children.Count];
        await Parallel.ForAsync(0, Children.Count, async (i, _) =>
        {
            if (Children[i] is not MsbtFileData file) return;

            await file.Save();
            model[i] = await file.LoadModel();
        });

        await using var writeStream = GetWriteStream();
        Compiler.Compile(model, writeStream);
    });
    #endregion
}