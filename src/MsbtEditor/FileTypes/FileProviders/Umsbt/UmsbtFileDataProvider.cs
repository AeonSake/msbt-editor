﻿using NintendoTools.FileFormats.Umsbt;

namespace MsbtEditor;

internal sealed class UmsbtFileDataProvider : IFileDataProvider
{
    #region private members
    private static readonly IconValue Icon = new("""<path d="M13 14l7 -0"/><path d="M16.5 14l0 8"/><path d="M7 22l-3 0l0 -8l3 -0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M7 18l-3 0"/><path d="M18 8c0 1.104 1.12 2 2.5 2c1.38 0 2.5 -0.896 2.5 -2c0 -1.104 -0.831 -1.657 -2.5 -2c-1.927 -0.397 -2.5 -0.896 -2.5 -2c0 -1.104 1.12 -2 2.5 -2c1.38 0 2.5 0.896 2.5 2"/><path d="M9 10l0 -8l3 4l3 -4l0 8"/><path d="M6 2l0 5.5c0 1.38 -1.12 2.5 -2.5 2.5c-1.38 0 -2.5 -1.12 -2.5 -2.5l0 -5.5"/>""");
    #endregion

    #region constructor
    public UmsbtFileDataProvider(FileIconProvider iconProvider) => iconProvider.Register<UmsbtFileData>(Icon, "#c47b2f");
    #endregion

    #region IFileDataProvider interface
    public bool CanParse(Stream stream) => UmsbtFileParser.CanParseStatic(stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null) => new UmsbtFileData(provider, fileFactory, parent);
    #endregion
}