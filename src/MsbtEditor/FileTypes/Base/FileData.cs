﻿namespace MsbtEditor;

public abstract class FileData(StorageProvider storageProvider, ContainerFileData? parent = null) : IDisposable
{
    #region private members
    private bool _isModified;
    private bool _isLoading;
    private bool _isDisposed;
    #endregion

    #region public properties
    public string Name => storageProvider.Name;

    public string InternalFilePath => storageProvider.FilePath;

    public bool IsVirtual => storageProvider.IsVirtual;

    public bool IsCompressed => storageProvider.IsCompressed;

    public virtual bool IsReadOnly { get; protected set; }

    public bool IsModified
    {
        get => _isModified;
        set
        {
            if (value == _isModified || IsReadOnly) return;
            _isModified = value;
            Modified?.Invoke(this, EventArgs.Empty);
        }
    }

    public virtual bool IsLoading
    {
        get => _isLoading;
        protected set
        {
            if (_isLoading == value) return;
            _isLoading = value;
            Loading?.Invoke(this, EventArgs.Empty);
        }
    }

    public ContainerFileData? Parent { get; set; } = parent;
    #endregion

    #region protected members
    protected AsyncLock Lock { get; } = new();
    #endregion

    #region public events
    public event EventHandler? Modified;

    public event EventHandler? Loading;
    #endregion

    #region public methods
    public string GetFilePath()
    {
        if (!IsVirtual) return InternalFilePath;
        return Parent is null ? Name : Path.Combine(Parent.GetFilePath(), Name).Replace('\\', '/');
    }

    public Task Save()
    {
        if (!IsModified || IsReadOnly) return Task.CompletedTask;

        return Lock.LockAsync(async () =>
        {
            IsLoading = true;
            try
            {
                await InternalSave();
                IsModified = false;
            }
            finally
            {
                IsLoading = false;
            }
        });
    }

    public Task Read(Stream stream, bool rawFile = false) => Read(fileStream => fileStream.CopyToAsync(stream), rawFile);

    public Task Write(Stream stream, bool rawFile = false) => Write(stream.CopyToAsync, rawFile);

    public Task Rename(string fileName)
    {
        if (Parent?.IsReadOnly ?? false) return Task.CompletedTask;

        return Lock.LockAsync(() => Task.Run(() =>
        {
            if (Name.Equals(fileName)) return;

            storageProvider.Rename(fileName);
            MetaDataChanged();
        }));
    }

    public void Unload()
    {
        if (IsVirtual) return;

        Parent?.Remove(this);
        Dispose();
    }

    public async Task Delete()
    {
        if (Parent?.IsReadOnly ?? false) return;

        await Lock.LockAsync(async () =>
        {
            await InternalDelete();
            Parent?.Remove(this);
            storageProvider.Delete();
        });
        Dispose();
    }
    #endregion

    #region protected methods
    protected Task<T> Read<T>(Func<Stream, T> reader, bool rawFile = false) => Read(stream => Task.FromResult(reader(stream)), rawFile);

    protected Task<T> Read<T>(Func<Stream, Task<T>> reader, bool rawFile = false) => Lock.LockAsync(async () =>
    {
        IsLoading = true;
        try
        {
            await using var readStream = rawFile ? File.OpenRead(InternalFilePath) : storageProvider.GetReadStream();
            return await reader(readStream);
        }
        finally
        {
            IsLoading = false;
        }
    });

    protected Task Read(Action<Stream> reader, bool rawFile = false) => Read(stream =>
    {
        reader(stream);
        return Task.FromResult(0);
    }, rawFile);

    protected Task Read(Func<Stream, Task> reader, bool rawFile = false) => Read(async stream =>
    {
        await reader(stream);
        return 0;
    }, rawFile);

    protected Task Write(Action<Stream> writer, bool rawFile = false) => Write(stream =>
    {
        writer(stream);
        return Task.CompletedTask;
    }, rawFile);

    protected Task Write(Func<Stream, Task> writer, bool rawFile = false)
    {
        if (IsReadOnly) return Task.CompletedTask;

        return Lock.LockAsync(async () =>
        {
            IsLoading = true;
            try
            {
                await using var writeStream = rawFile ? File.Create(InternalFilePath) : storageProvider.GetWriteStream();
                await writer(writeStream);

                var notify = !IsModified;
                IsModified = false;
                if (notify) MetaDataChanged();
            }
            finally
            {
                IsLoading = false;
            }
        });
    }

    protected Stream GetReadStream() => storageProvider.GetReadStream();

    protected Stream GetWriteStream() => storageProvider.GetWriteStream();

    protected virtual Task InternalSave() => Task.CompletedTask;

    protected virtual Task InternalDelete() => Task.CompletedTask;

    protected void NotifyModified() => Modified?.Invoke(this, EventArgs.Empty);

    protected virtual void MetaDataChanged() => Parent?.MetaDataChanged();
    #endregion

    #region IDisposable interface
    public void Dispose()
    {
        if (_isDisposed) return;

        Dispose(true);
        GC.SuppressFinalize(this);
        _isDisposed = true;
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposing) return;

        storageProvider.Dispose();
        Lock.Dispose();
    }
    #endregion
}