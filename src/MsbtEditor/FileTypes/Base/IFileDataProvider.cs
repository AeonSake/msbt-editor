﻿namespace MsbtEditor;

internal interface IFileDataProvider
{
    public bool CanParse(Stream stream);

    public FileData Parse(StorageProvider provider, FileDataFactory fileFactory, ContainerFileData? parent = null);
}