﻿using NintendoTools.FileFormats;

namespace MsbtEditor;

public interface IAlignedContainerFileData
{
    public Task<List<AlignmentInfo>> CheckAlignment();
}