﻿using System.Collections;

namespace MsbtEditor;

public abstract class ContainerFileData : FileData, IEnumerable<FileData>
{
    #region private members
    private DirectoryInfo? _tempDir;
    private List<FileData> _children = [];
    #endregion

    #region constructors
    protected ContainerFileData(StorageProvider storageProvider, ContainerFileData? parent = null) : base(storageProvider, parent)
    { }

    protected ContainerFileData(StorageProvider storageProvider, IEnumerable<FileData> children, ContainerFileData? parent = null) : base(storageProvider, parent)
    {
        foreach (var file in children)
        {
            _children.Add(file);

            file.Parent?.Remove(file);
            file.Parent = this;
            file.Modified += OnFileModified;
        }

        IsInitialized = true;
    }
    #endregion

    #region public properties
    public bool IsInitialized { get; protected set; }

    public IReadOnlyList<FileData> Children => _children;
    #endregion

    #region protected properties
    protected virtual bool DisposeChildrenOnReload => true;

    protected virtual bool AutoResetModifiedState => false;
    #endregion

    #region public methods
    public Task Init() => Lock.LockAsync(async () =>
    {
        if (IsInitialized) return;

        IsLoading = true;
        try
        {
            DeleteTempDir();

            var children = await LoadChildren();
            foreach (var file in children)
            {
                file.Parent = this;
                file.Modified += OnFileModified;
            }
            _children = children;
        }
        finally
        {
            IsLoading = false;
        }

        IsInitialized = true;
        NotifyModified();
    });

    public async Task InitAll()
    {
        await Init();

        foreach (var childFile in Children)
        {
            if (childFile is ContainerFileData container) await container.InitAll();
        }
    }

    public Task Reload() => Lock.LockAsync(async () =>
    {
        if (!IsInitialized) return;

        IsLoading = true;
        try
        {
            DeleteTempDir();

            var children = await LoadChildren();
            foreach (var file in _children)
            {
                file.Parent = null;
                file.Modified -= OnFileModified;
                if (DisposeChildrenOnReload) file.Dispose();
            }
            foreach (var file in children)
            {
                file.Parent = this;
                file.Modified += OnFileModified;
            }
            _children = children;
        }
        finally
        {
            IsLoading = false;
        }

        IsModified = false;
        NotifyModified();
    });

    public void Add(FileData file)
    {
        if (!IsInitialized || IsReadOnly || !CanAdd(file)) return;

        _children.Add(file);

        file.Parent?.Remove(file);
        file.Parent = this;
        file.Modified += OnFileModified;

        MetaDataChanged();
    }

    public void AddRange(IEnumerable<FileData> files)
    {
        if (!IsInitialized || IsReadOnly) return;

        var count = 0;
        foreach (var file in files)
        {
            if (!CanAdd(file)) continue;

            _children.Add(file);

            file.Parent?.Remove(file);
            file.Parent = this;
            file.Modified += OnFileModified;

            ++count;
        }

        if (count > 0) MetaDataChanged();
    }

    public void Insert(FileData file, int index)
    {
        if (index < 0 || index > _children.Count) throw new ArgumentOutOfRangeException(nameof(index));
        if (!IsInitialized || IsReadOnly || !CanAdd(file)) return;

        _children.Insert(index, file);

        file.Parent?.Remove(file);
        file.Parent = this;
        file.Modified += OnFileModified;

        MetaDataChanged();
    }

    public void Remove(FileData file)
    {
        if (!IsInitialized || IsReadOnly) return;

        if (_children.Remove(file))
        {
            file.Parent = null;
            file.Modified -= OnFileModified;
        }

        MetaDataChanged();
    }

    public void Clear(bool disposeFiles = false)
    {
        if (!IsInitialized || IsReadOnly) return;

        foreach (var file in _children)
        {
            file.Parent = null;
            file.Modified -= OnFileModified;
            if (disposeFiles) file.Dispose();
        }

        MetaDataChanged();
    }

    public bool Contains(FileData file) => _children.Contains(file);

    public bool Contains(Func<FileData, bool> predicate) => Find<FileData>(predicate) is not null;

    public bool Contains<T>(Func<T, bool> predicate) where T : FileData => Find(predicate) is not null;

    public FileData? Find(Func<FileData, bool> predicate) => Find<FileData>(predicate);

    public T? Find<T>(Func<T, bool> predicate) where T : FileData
    {
        foreach (var file in Children)
        {
            if (file is T typedFile && predicate(typedFile)) return typedFile;
            if (file is ContainerFileData container && container.Find(predicate) is { } foundFile) return foundFile;
        }
        return null;
    }

    public IEnumerable<FileData> FindAll() => FindAll<FileData>();

    public IEnumerable<T> FindAll<T>() where T : FileData
    {
        var files = new List<T>();
        FindAllChildren(this);
        return files;

        void FindAllChildren(ContainerFileData file)
        {
            foreach (var childFile in file.Children)
            {
                if (childFile is T typedFile) files.Add(typedFile);
                if (childFile is ContainerFileData container) FindAllChildren(container);
            }
        }
    }

    public IEnumerable<FileData> FindAll(Func<FileData, bool> predicate) => FindAll<FileData>(predicate);

    public IEnumerable<T> FindAll<T>(Func<T, bool> predicate) where T : FileData
    {
        var files = new List<T>();
        FindAllChildren(this);
        return files;

        void FindAllChildren(ContainerFileData file)
        {
            foreach (var childFile in file.Children)
            {
                if (childFile is T typedFile && predicate(typedFile)) files.Add(typedFile);
                if (childFile is ContainerFileData container) FindAllChildren(container);
            }
        }
    }

    public virtual bool CanAdd(FileData file) => true;

    public string GetTempDir()
    {
        _tempDir ??= CommonPaths.CreateTempDir();
        return _tempDir.FullName;
    }
    #endregion

    #region protected methods
    protected virtual Task<List<FileData>> LoadChildren() => Task.FromResult((List<FileData>) []);

    protected sealed override void MetaDataChanged()
    {
        if (!IsInitialized) return;
        if (AutoResetModifiedState) Parent?.MetaDataChanged();
        else IsModified = true;
    }
    #endregion

    #region private methods
    private void OnFileModified(object? sender, EventArgs args)
    {
        if (sender is not FileData file) return;

        if (file.IsModified)
        {
            IsModified = true;
        }
        else if (AutoResetModifiedState && !Contains(f => f.IsModified))
        {
            IsModified = false;
        }
    }

    private void DeleteTempDir()
    {
        var tempDir = _tempDir;
        if (tempDir is null) return;

        _tempDir = null;
        try
        {
            tempDir.Delete(true);
        }
        catch
        {
            //ignored
        }
    }
    #endregion

    #region IEnumerable interface
    public IEnumerator<FileData> GetEnumerator() => _children.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    #endregion

    #region IDisposable interface
    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            Parallel.ForEach(_children, file =>
            {
                file.Parent = null;
                file.Modified -= OnFileModified;
                file.Dispose();
            });

            DeleteTempDir();
        }

        base.Dispose(disposing);
    }
    #endregion
}