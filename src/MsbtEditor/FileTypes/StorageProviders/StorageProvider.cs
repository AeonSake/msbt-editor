﻿namespace MsbtEditor;

public abstract class StorageProvider : IDisposable
{
    #region private members
    private bool _isDisposed;
    #endregion

    #region public properties
    public abstract string Name { get; }

    public abstract string FilePath { get; }

    public abstract bool IsVirtual { get; }

    public abstract bool IsCompressed { get; }
    #endregion

    #region public methods
    public abstract Stream GetReadStream();

    public abstract Stream GetWriteStream();

    public abstract void Rename(string fileName);

    public abstract void Move(string filePath);

    public abstract void Delete();
    #endregion

    #region IDisposable interface
    public void Dispose()
    {
        if (_isDisposed) return;

        Dispose(true);
        GC.SuppressFinalize(this);
        _isDisposed = true;
    }

    protected virtual void Dispose(bool disposing)
    { }
    #endregion
}