﻿namespace MsbtEditor;

internal sealed class FileStorageProvider(FileInfo fileInfo, bool isVirtual) : StorageProvider
{
    #region private members
    private readonly SemaphoreSlim _lock = new(1, 1);
    private FileInfo _fileInfo = fileInfo;
    #endregion

    #region constructor
    public FileStorageProvider(string filePath, bool isVirtual) : this(new FileInfo(filePath), isVirtual)
    { }
    #endregion

    #region public properties
    public override string Name => _fileInfo.Name;

    public override string FilePath => _fileInfo.FullName.Replace('\\', '/');

    public override bool IsVirtual => isVirtual;

    public override bool IsCompressed => false;
    #endregion

    #region public methods
    public override Stream GetReadStream()
    {
        try
        {
            _lock.Wait();
            return new WrappedStream<FileStream>(_fileInfo.OpenRead(), _ => _lock.Release());
        }
        catch
        {
            _lock.Release();
            throw;
        }
    }

    public override Stream GetWriteStream()
    {
        _lock.Wait();
        return new WrappedStream<MemoryStream>(new MemoryStream(), stream =>
        {
            try
            {
                if (stream.Length <= 0) return;

                using var fileStream = _fileInfo.OpenWrite();
                stream.Seek(0, SeekOrigin.Begin);
                stream.CopyTo(fileStream);
                fileStream.SetLength(stream.Length);
            }
            finally
            {
                _lock.Release();
            }
        });
    }

    public override void Rename(string fileName) => Move(_fileInfo.Directory is null ? fileName : Path.Combine(_fileInfo.Directory.FullName, fileName));

    public override void Move(string filePath)
    {
        try
        {
            _lock.Wait();
            if (File.Exists(filePath)) throw new IOException("File already exists.");

            filePath = Path.GetFullPath(filePath);
            var currentFile = _fileInfo.FullName;

            if (_fileInfo.Name.Equals(Path.GetFileName(filePath), StringComparison.OrdinalIgnoreCase))
            {
                var tempFile = currentFile + ".TEMP";
                File.Move(currentFile, tempFile);
                currentFile = tempFile;
            }

            Directory.Move(currentFile, filePath);
            _fileInfo = new FileInfo(filePath);
        }
        finally
        {
            _lock.Release();
        }
    }

    public override void Delete()
    {
        try
        {
            _lock.Wait();
            if (!File.Exists(_fileInfo.FullName)) return;
            _fileInfo.Delete();
        }
        finally
        {
            _lock.Release();
        }
    }
    #endregion

    #region protected methods
    protected override void Dispose(bool disposing)
    {
        if (disposing) _lock.Dispose();
    }
    #endregion
}