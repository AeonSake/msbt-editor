﻿namespace MsbtEditor;

internal abstract class CompressionWrapper(StorageProvider provider) : StorageProvider
{
    #region private members
    private bool _unwrapped;
    #endregion

    #region public properties
    public override string Name => provider.Name;

    public override string FilePath => provider.FilePath;

    public override bool IsVirtual => provider.IsVirtual;

    public sealed override bool IsCompressed => true;
    #endregion

    #region protected properties
    protected StorageProvider Provider => provider;
    #endregion

    #region public methods
    public override void Rename(string fileName) => provider.Rename(fileName);

    public override void Move(string filePath) => provider.Move(filePath);

    public override void Delete() => provider.Delete();

    public StorageProvider Unwrap()
    {
        _unwrapped = true;
        return provider;
    }
    #endregion

    #region protected methods
    protected override void Dispose(bool disposing)
    {
        if (disposing && !_unwrapped) provider.Dispose();
    }
    #endregion
}