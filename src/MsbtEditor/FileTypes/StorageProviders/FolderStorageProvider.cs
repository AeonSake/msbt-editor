﻿namespace MsbtEditor;

internal sealed class FolderStorageProvider(DirectoryInfo directoryInfo, bool isVirtual) : StorageProvider
{
    #region private members
    private readonly SemaphoreSlim _lock = new(1, 1);
    private DirectoryInfo _directoryInfo = directoryInfo;
    #endregion

    #region constructor
    public FolderStorageProvider(string directoryPath, bool isVirtual) : this(new DirectoryInfo(directoryPath), isVirtual)
    { }
    #endregion

    #region public properties
    public override string Name => _directoryInfo.Name;

    public override string FilePath => _directoryInfo.FullName.Replace('\\', '/');

    public override bool IsVirtual => isVirtual;

    public override bool IsCompressed => false;
    #endregion

    #region public methods
    public override Stream GetReadStream() => new MemoryStream();

    public override Stream GetWriteStream() => new MemoryStream();

    public override void Rename(string fileName) => Move(_directoryInfo.Parent is null ? fileName : Path.Combine(_directoryInfo.Parent.FullName, fileName));

    public override void Move(string filePath)
    {
        try
        {
            _lock.Wait();
            if (Directory.Exists(filePath)) throw new IOException("Target directory already exists.");

            filePath = Path.GetFullPath(filePath);
            var currentDir = _directoryInfo.FullName;

            if (_directoryInfo.Name.Equals(Path.GetFileName(filePath), StringComparison.OrdinalIgnoreCase))
            {
                var tempDir = currentDir + "___TEMP";
                Directory.Move(currentDir, tempDir);
                currentDir = tempDir;
            }

            Directory.Move(currentDir, filePath);
            _directoryInfo = new DirectoryInfo(filePath);
        }
        finally
        {
            _lock.Release();
        }
    }

    public override void Delete()
    {
        try
        {
            _lock.Wait();
            if (!Directory.Exists(_directoryInfo.FullName)) return;
            _directoryInfo.Delete(true);
        }
        finally
        {
            _lock.Release();
        }
    }
    #endregion

    #region protected methods
    protected override void Dispose(bool disposing)
    {
        if (disposing) _lock.Dispose();
    }
    #endregion
}