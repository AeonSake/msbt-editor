# MSBT Editor Syntax
To allow editing MSBT messages in an editor, the MSBT file structure is serialized into a special text format. This always contains general file metadata at the top of the text file, followed by the individual message entries. Special caution is advised when editing header/message metadata, especially attribute data (ATR1). This data is used by the game to map certain additional flags or arguments.

The text editor provides syntax highlighting for the MSBT serialization format to make editing as easy as possible.

## Header Format
The file header is delimited by three `%` symbols, containing metadata about the MSBT file. Do not edit this data unless you know what you are doing.

```ls
%%%
bigEndian: false
version: 3
encoding: utf-16
hasNLI1: false
hasLBL1: true
hasATR1: true
hasAttributeText: false
hasATO1: false
hasTSY1: false
%%%
```

## Message Format
The header of a message is delimited by three `-` symbols, containing metadata about the message itself. It always contains a label value and additionally some attribute data. The actual text of the message, including the encoded tag calls, follows directly after the message header. Tags and tag arguments are parsed and mapped using [game configs](/doc/GameConfigs.md).

```hbs
---
label: Talk_0015
---
{{resetAnim arg="[0,0]"}}{{anim type="LaughS_00"}}Hudson Construction's whole business is
built around {{color id="0"}}supporting{{color id="-1"}} our customers as
they {{color id="0"}}assemble their dream homes{{color id="-1"}}.
```

In cases where a line of the message body consists of only the message header delimiter (`---`), the text is escaped as `{{---}}`. This is automatically removed in the final output.

A maximum of 2 newlines are trimmed at the end of a message body. Any more newlines will also be in the final text. This means the following message entries (newlines symbolized as `\n`):
```bash
---
label: Message1
---
Example Text 1.\n
\n
---
label: Message2
---
Example Text 2.\n
\n
\n
---
```
Will be converted into the following output:
```bash
[Message1] Example Text 1.
[Message2] Example Text 2.\n
```