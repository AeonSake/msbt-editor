# Game Configs
Game configs allow specifying game-specific settings such as tag maps and alignments. Pre-defined examples for some select games can be found [here](/configs). The following documentation lists all available sections and fields.


## Base Config
```yaml
game: "Game name"
```

### `game`: `string` (default: `""`)
An optional name for the game this config applies to. This name is shown in the editor when selecting a game config to use.


## `zstd` Section
The `zstd` section specifies `ZSTD`-specific settings. It can be either a single entry or a list of entries.
```yaml
zstd:
  compressionLevel: 19
  dict: path/to/zs.dict
  filter: "*.example.zs"
```
```yaml
zstd:
- compressionLevel: 19
  dict: path/to/zs.dict
  filter: "*.example.zs"
- compressionLevel: 19
  dict: path/to/zs.dict
  filter: "*.example.zs"
```

### `compressionLevel`: `int` (default: `19`)
The compression level to use when compressing files.

### `dict`: `string` (default: `null`)
A path to a [compression dictionary](/doc/ZstdCompression.md) to use for decompression. The path is relative to the config file, unless an absolute path is used.

### `filter`: `string|string[]` (default: `[]`)
An optional file filter to check before applying the config section. See [file filters](#file-filters) for more information.


## `darc` Section
The `darc` section specifies `DARC`-specific settings such as custom file alignments. It can be either a single entry or a list of entries.
```yaml
darc:
  alignment:
    default: 0x08
    extensions:
      ".msbt": 0x10
  checkAlignment: false
  filter: "*.example.darc"
```
```yaml
darc:
- alignment:
    default: 0x08
    extensions:
      ".msbt": 0x10
  checkAlignment: false
  filter: "*.example.darc"
- alignment:
    default: 0x08
    extensions:
      ".msbt": 0x10
  checkAlignment: false
  filter: "*.example.darc"
```

### `alignment.default`: `int` (default: `8`)
The default alignment value to use for all unspecified file extensions.

### `alignment.extensions`: `dict<string,int>` (default: `{}`)
A map of file extensions and the alignment values to use. `DARC` archives use extension-specific alignments during compilation which are unknown without the original compilation tools. To get bit-perfect alignments, a map like this has to be used.

### `checkAlignment`: `bool` (default: `false`)
Whether to check file alignments before writing changes to a `DARC` archive. If enabled and a file is not aligned according to the given alignment table, an error is shown and the writing process will be aborted.

### `filter`: `string|string[]` (default: `[]`)
An optional file filter to check before applying the config section. See [file filters](#file-filters) for more information.


## `sarc` Section
The `sarc` section specifies `SARC`-specific settings such as custom file alignments. It can be either a single entry or a list of entries.
```yaml
sarc:
  alignment:
    default: 0x08
    extensions:
      ".msbt": 0x10
  checkAlignment: false
  filter: "*.example.sarc"
```
```yaml
sarc:
- alignment:
    default: 0x08
    extensions:
      ".msbt": 0x10
  checkAlignment: false
  filter: "*.example.sarc"
- alignment:
    default: 0x08
    extensions:
      ".msbt": 0x10
  checkAlignment: false
  filter: "*.example.sarc"
```

### `alignment.default`: `int` (default: `8`)
The default alignment value to use for all unspecified file extensions.

### `alignment.extensions`: `dict<string,int>` (default: `{}`)
A map of file extensions and the alignment values to use. `SARC` archives use extension-specific alignments during compilation which are unknown without the original compilation tools. To get bit-perfect alignments, a map like this has to be used.

### `checkAlignment`: `bool` (default: `false`)
Whether to check file alignments before writing changes to a `SARC` archive. If enabled and a file is not aligned according to the given alignment table, an error is shown and the writing process will be aborted.

### `filter`: `string|string[]` (default: `[]`)
An optional file filter to check before applying the config section. See [file filters](#file-filters) for more information.

## `bmg` Section
The `bmg` section specifies `BMG`-specific settings such as tag maps.
```yaml
bmg:
  newline: lf
  tags:
  - name: tag1
    description: tag1 description
    group: 0
    type: 1
    arguments:
    - name: arg1
      description: arg1 description
      dataType: u16
      arrayLength: 2
    - name: arg2
      dataType: u8
      valueMap:
        1: val1
        2:
          name: val1
          description: val1 description
  - name: tag2
    group: 1
    types: [1,2,3]
  - name: tag3
    group: 2
    typeMap:
      1: type1
      2:
        name: type2
        description: type2 description
  - name: tag4
    group: 3
    discard: true
```

### `newline`: `enum` (default: `lf`)
Specifies the newline characters to use when writing changes to the file. All newlines are normalized to this. Possible values are: `lf`, `crlf`, `cr`

### `tags`: `tag[]` (default: `[]`)
A list of tag definitions to format `BMG` text in the editor.

### `tag.name`: `string` (default: `""`)
A name for the tag. This is the name rendered in the editor as `{{tagName}}`.

### `tag.description`: `string` (default: `""`)
An optional description for the tag. This description is shown in the Tag Glossary.

### `tag.group`: `byte` (required)
The group ID for the tag.

### `tag.type`: `ushort` (default: `null`)
The type ID for the tag. Either this, `tag.types`, `tag.typeMap`, or `tag.discard` has to be specified to form a valid tag definition. The type values can also be combined. Named tags without a type ID will match against this definition. No duplicate type ID definition is allowed.

### `tag.types`: `ushort[]` (default: `[]`)
A list of type IDs for the tag. Tags will be rendered as `{{tagName:tagTypeId}}`. Either this, `tag.type`, `tag.typeMap`, or `tag.discard` has to be specified to form a valid tag definition. The type values can also be combined. No duplicate type ID definition is allowed.

### `tag.typeMap`: `dict<ushort,string|valueInfo>` (default: `{}`)
A map of named type IDs for the tag. Tags will be rendered as `{{tagName:tagTypeName}}`. Values in this map can optionally include a description to render in the Tag Glossary. Either this, `tag.type`, `tag.types`, or `tag.discard` has to be specified to form a valid tag definition. The type values can also be combined. No duplicate type ID definition is allowed.

### `tag.discard`: `bool` (default: `false`)
Specifies that this tag acts as a discard. Discard tags will catch all unmatched type IDs of the group. Type IDs specified in `tag.type`, `tag.types`, and `tag.typeMap` are checked before matching against the discard. Either this, `tag.type`, `tag.types`, or `tag.typeMap` has to be specified to form a valid tag definition. The type values can also be combined.

### `tag.arguments`: `arg[]` (default: `[]`)
A list of arguments for the tag. Each entry (except padding and discards) will be rendered as `{{tagName argName="argValue"}}` in the `BMG` text in the editor. If the game file contains additional data not matched by the argument definitions, the additional data will be included as an additional `otherArg` argument (datatype: `hex`).

### `arg.name`: `string` (default: `arg{index+1}`)
A name for the tag argument. This is the name rendered in the editor as `{{tagName argName="argValue"}}`.

### `arg.description`: `string` (default: `""`)
An optional description for the tag argument. This description is shown in the Tag Glossary.

### `arg.dataType`: `dataType` (required)
The data type of the argument. See the list of available [data types](#argument-data-types) for more information.

### `arg.valueMap`: `dict<T,string|valueInfo>` (default: `{}`)
A map of named argument values (in the given argument data type). Arguments will be rendered as `argName="argValueName"`. Values in this map can optionally include a description to render in the Tag Glossary.

### `arg.arrayLength`: `int` (default: `0`)
Specifies the length of the array, if the argument data should be read as an array. The argument value will be serialized as `argName="[argValue1,argValue2]"`.

### `valueInfo.name`: `string` (required)
The name for the value.

### `valueInfo.description`: `string` (default: `""`)
An optional description for the value. This description is shown in the Tag Glossary.


## `msbt` Section
The `msbt` section specifies `MSBT`-specific settings such as tag maps.
```yaml
msbt:
  fixedLabelGroups: false
  newline: lf
  tags:
  - name: tag1
    description: tag1 description
    group: 0
    type: 1
    arguments:
    - name: arg1
      description: arg1 description
      dataType: u16
      arrayLength: 2
    - name: arg2
      dataType: u8
      valueMap:
        1: val1
        2:
          name: val1
          description: val1 description
  - name: tag2
    group: 1
    types: [1,2,3]
  - name: tag3
    group: 2
    typeMap:
      1: type1
      2:
        name: type2
        description: type2 description
  - name: tag4
    group: 3
    discard: true
```

### `fixedLabelGroups`: `bool` (default: `false`)
Whether to use fixed number of label groups. Label groups are bins used in a hash map for faster lookup of message labels in the game engine. Some older games (mostly before the Nintendo Switch) use a fixed number of label groups (usually `101`), regardless of how many labels are in a file. Using different numbers of label groups _usually_ doesn't cause any issues in the game engine but the files will not be bit-perfect for those kind of games. If enabled, the `labelGroups` property is used to determine the number of label groups instead of recalculating them when saving the file. If that value is set to `0`, the number of label groups is always recalculated. Consider only enabling this feature if you encounter issues when loading a modified file into the game engine (will require setting the value manually on an already edited file).

### `newline`: `enum` (default: `lf`)
Specifies the newline characters to use when writing changes to the file. All newlines are normalized to this. Possible values are: `lf`, `crlf`, `cr`

### `tags`: `tag[]` (default: `[]`)
A list of tag definitions to format `MSBT` text in the editor.

### `tag.name`: `string` (default: `""`)
A name for the tag. This is the name rendered in the editor as `{{tagName}}`.

### `tag.description`: `string` (default: `""`)
An optional description for the tag. This description is shown in the Tag Glossary.

### `tag.group`: `ushort` (required)
The group ID for the tag.

### `tag.type`: `ushort` (default: `null`)
The type ID for the tag. Either this, `tag.types`, `tag.typeMap`, or `tag.discard` has to be specified to form a valid tag definition. The type values can also be combined. Named tags without a type ID will match against this definition. No duplicate type ID definition is allowed.

### `tag.types`: `ushort[]` (default: `[]`)
A list of type IDs for the tag. Tags will be rendered as `{{tagName:tagTypeId}}`. Either this, `tag.type`, `tag.typeMap`, or `tag.discard` has to be specified to form a valid tag definition. The type values can also be combined. No duplicate type ID definition is allowed.

### `tag.typeMap`: `dict<ushort,string|valueInfo>` (default: `{}`)
A map of named type IDs for the tag. Tags will be rendered as `{{tagName:tagTypeName}}`. Values in this map can optionally include a description to render in the Tag Glossary. Either this, `tag.type`, `tag.types`, or `tag.discard` has to be specified to form a valid tag definition. The type values can also be combined. No duplicate type ID definition is allowed.

### `tag.discard`: `bool` (default: `false`)
Specifies that this tag acts as a discard. Discard tags will catch all unmatched type IDs of the group. Type IDs specified in `tag.type`, `tag.types`, and `tag.typeMap` are checked before matching against the discard. Either this, `tag.type`, `tag.types`, or `tag.typeMap` has to be specified to form a valid tag definition. The type values can also be combined.

### `tag.arguments`: `arg[]` (default: `[]`)
A list of arguments for the tag. Each entry (except padding and discards) will be rendered as `{{tagName argName="argValue"}}` in the `MSBT` text in the editor. If the game file contains additional data not matched by the argument definitions, the additional data will be included as an additional `otherArg` argument (datatype: `hex`).

### `arg.name`: `string` (default: `arg{index+1}`)
A name for the tag argument. This is the name rendered in the editor as `{{tagName argName="argValue"}}`.

### `arg.description`: `string` (default: `""`)
An optional description for the tag argument. This description is shown in the Tag Glossary.

### `arg.dataType`: `dataType` (required)
The data type of the argument. See the list of available [data types](#argument-data-types) for more information.

### `arg.valueMap`: `dict<T,string|valueInfo>` (default: `{}`)
A map of named argument values (in the given argument data type). Arguments will be rendered as `argName="argValueName"`. Values in this map can optionally include a description to render in the Tag Glossary.

### `arg.arrayLength`: `int` (default: `0`)
Specifies the length of the array, if the argument data should be read as an array. The argument value will be serialized as `argName="[argValue1,argValue2]"`.

### `valueInfo.name`: `string` (required)
The name for the value.

### `valueInfo.description`: `string` (default: `""`)
An optional description for the value. This description is shown in the Tag Glossary.

### Argument Data Types

| Type      | Alias                                       | Bytes | Description                   |
|-----------|---------------------------------------------|-------|-------------------------------|
| `bool`    | `boolean`                                   | 1     | Boolean                       |
| `u8`      | `uint8`, `byte`                             | 1     | Unsigned 8bit integer         |
| `s8`      | `i8`, `int8`, `sbyte`                       | 1     | Signed 8bit integer           |
| `u16`     | `uint16`, `ushort`                          | 2     | Unsigned 16bit integer        |
| `s16`     | `i16`, `int16`, `short`                     | 2     | Signed 16bit integer          |
| `u32`     | `uint32`, `uint`                            | 4     | Unsigned 32bit integer        |
| `s32`     | `i32`, `int32`, `int`                       | 4     | Signed 32bit integer          |
| `u64`     | `uint64`, `ulong`                           | 8     | Unsigned 64bit integer        |
| `s64`     | `i64`, `int64`, `long`                      | 8     | Signed 64bit integer          |
| `f32`     | `float32`, `single`, `float`                | 4     | 32bit float                   |
| `f64`     | `float64`, `double`                         | 8     | 64bit float                   |
| `str`     | `string`                                    | 2+n   | Text encoded in the format of the file; the first 2 bytes of the argument data are the length of the string |
| `nstr`    | `0str`, `nullStr`, `nstring`, `0string`, `nullString` | n+1   | Null-terminated text encoded in the format of the file; the last byte of the data is always `0x00` |
| `hex`     | `hexStr`, `hexString`                       | n     | Raw bytes as hex-encoded text |
| `<hex_value>` |                                         | n     | Data padding with the specified hex bytes |

Serialized/deserialized argument values depend on the endianness of the file and are automatically converted (see file header). Data type declarations are case-insensitive.


## `bcsv` Section
The `bcsv` section specifies `BCSV`-specific settings.
```yaml
bcsv:
  headers:
  - hash: 0x1234abcd
    name: label1
    type: u8
  - hash: 0x1234abcd
    name: label2
    type: str
  fieldHashes:
    crc32: ['label1', 'label2', 'label3'],
    mmh3: ['label1', 'label2', 'label3']
```

### `headers`: `header[]` (default: `[]`)
A list of `BCSV` header definitions to parse and format column data.

### `header.hash`: `string` (default: `""`)
The hash of the header label to match against. This or `header.name` must be specified.

### `header.name`: `string` (default: `""`)
The plain-text value of the header label to match against. Must be the exact string the game uses because the label string will be hashed as CRC32 for matching. This or `header.hash` must be specified.

### `header.type`: `dataType` (default: best-match)
The data type of the values in this column. If not specified, a data type that best fits the column data width will be selected.

### `fieldHashes.crc32`: `string[]` (default: `[]`)
A list of labels that are used to replace CRC32-hashed cell values in the `BCSV` file.

### `fieldHashes.mmh3`: `string[]` (default: `[]`)
A list of labels that are used to replace MMH3-hashed cell values in the `BCSV` file.

### Header Data Types

| Type            | Bytes | Description                                 |
|-----------------|-------|---------------------------------------------|
| `u8`            | 1     | Unsigned 8bit integer                       |
| `s8`            | 1     | Signed 8bit integer                         |
| `u16`           | 2     | Unsigned 16bit integer                      |
| `s16`           | 2     | Signed 16bit integer                        |
| `u32`           | 4     | Unsigned 32bit integer                      |
| `s32`           | 4     | Signed 32bit integer                        |
| `u64`           | 8     | Unsigned 64bit integer                      |
| `s64`           | 8     | Signed 64bit integer                        |
| `f32`           | 4     | 32bit float                                 |
| `f64`           | 8     | 64bit float                                 |
| `str`           | n     | Text encoded in the format of the file; the length of the string depends on the header size |
| `crc32`         | 4     | CRC32-hashed value                          |
| `hshCstringRef` | 4     | CRC32-hashed value (string table reference) |
| `mmh3`          | 4     | MMH3-hashed value                           |
| `u8[]`          | n     | Raw data as array of unsigned 8bit integers; the length of the array depends on the header size |
| `s8[]`          | n     | Raw data as array of signed 8bit integers; the length of the array depends on the header size |


## `byml` Section
The `byml` section specifies `BYML`-specific settings.
```yaml
byml:
  hashes: ['label1', 'label2', 'label3']
```

### `hashes`: `string[]` (default: `[]`)
A list of labels that are used to replace CRC32-hashed dictionary keys in the `BYML` file.


## Field Values
Game configs generally support most basic data types defined in the YAML specification. Numeric values can also be specified as hex values with the `0x` prefix.


## File Filters
Some sections support file filters to restrict the settings of an entry to specific files. A file filter can be either a single entry or a list of entries. Files are matched against wildcards `*`, otherwise the entire file path must match the filter value.
```yaml
filter: "*.example"
```
```yaml
filter:
- "*.example"
- "*.example"
```
Internally, filter values are converted to Regular Expressions, replacing the `*` wildcard with `.*` and escaping all other symbols. The start/end of the filter value is matched against the start/end of the file path.
```js
"*.example" -> /^.*\.example$/
".example*" -> /^\.example.*$/
".ex*mple"  -> /^\.ex.*mple$/
```