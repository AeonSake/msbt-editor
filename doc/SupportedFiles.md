# Supported File Types
The following list contains all supported file types.

| File Type      | Read | Write |
|----------------|------|-------|
| BMG            | Yes  | Yes   |
| MSBT           | Yes  | Yes   |
| MSBP           | Yes  | No    |
| BTXT           | Yes  | Yes   |
| Neverland Text | Yes  | Yes   |
| BCSV           | Yes  | No    |
| BYML           | Yes  | No    |
| ARC/U8         | Yes  | Yes   |
| DARC           | Yes  | Yes   |
| NARC           | Yes  | Yes   |
| RARC           | Yes  | Yes   |
| SARC           | Yes  | Yes   |
| UMSBT          | Yes  | Yes   |
| Zip            | Yes  | Yes   |

# Supported Compression Algorithms
The following list contains all supported compression algorithms.

| Compression Type    | Decompress | Compress |
|---------------------|------------|----------|
| LZ10                | Yes        | Yes      |
| LZ11                | Yes        | Yes      |
| LZ77                | Yes        | Yes      |
| Yay0                | Yes        | Yes      |
| Yaz0                | Yes        | Yes      |
| ZLib                | Yes        | Yes      |
| ZSTD                | Yes        | Yes      |
| [ZSTD + Dictionaries](/doc/ZstdCompression.md) | Yes        | Yes      |