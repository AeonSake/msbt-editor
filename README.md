# MSBT Editor
The MSBT Editor is an open-source C# project providing a modern text editor for MSBT and BMG files. Style and functionality is heavily inspired by [Visual Studio Code](https://code.visualstudio.com/) and uses [Monaco Editor](https://microsoft.github.io/monaco-editor/) as the base text editor.

## Features
- Opening, editing, and saving of MSBT and BMG files
- Full support for all known MSBT/BMG sections, big and little endian, and all encoding formats
- Editor syntax highlighting for MSBT and BMG files
- Opening and saving modified content of ARC, DARC, NARC, RARC, SARC, UMSBT and ZIP archives
- Opening and saving of compressed LZ10, LZ11, LZ77, Yay0, Yaz0, ZLib, and ZSTD files
- Support for ZSTD compression dictionaries
- Use of custom game configs

A list of all supported file types can be found [here](/doc/SupportedFiles.md).

## Requirements
- Windows 10 or later
- .NET Runtime 8.0 or later (might already be included with your Windows installation/update)

## Getting Started
Download the [latest release](https://gitlab.com/AeonSake/msbt-editor/-/releases) or build the project from source.

By default, the MSBT Editor doesn't come with any [game configs](/doc/GameConfigs.md). Game configs provide various game-specific settings such as readable syntax for special code instructions that are encoded into MSBT/BMG files. A set of pre-defined game configs can be found [here](/configs) which can be copied into the `configs` folder (next to the executable).

Load files or folders via `File > Open File...` and `File > Open Folder...`. Loaded files/folders are displayed on the left in the file browser. The editor can open any folder or file but only supports reading/editing certain file formats. Archives (e.g., SARC) can be browsed and edited to some extend. It is also possible to export the content of a folder or archive. Files listed in the file browser can be unloaded (not deleted) at any point by pressing the `X` button while hovering on the file/folder or via `File > Unload`. If the file is part of an archive (e.g., SARC), it cannot be unloaded (only the parent archive).

Clicking on a MSBT or BMG file in the file browser opens the text editor for that file as preview. Double-clicking the file will pin the editor tab. The MSBT Editor uses a special [text syntax](/doc/EditorSyntax.md) for displaying the content of a MSBT or BMG file. Each message entry consists of a header (delimited with `---`), followed by the actual message content. All tags in the text are mapped to the currently selected game config and are rendered as `{{example arg="value"}}`. The current game config can be changed via `Data > Change Game Config`.

The Tag Glossary (`View > Toggle Tag Glossary`) lists all defined tags and their argument types of the currently selected game config.

Some games use certain compression formats to reduce file sizes. The MSBT Editor currently supports a variety of different [compression algorithms](/doc/SupportedFiles.md). In some cases, games may use ZStandard (ZSTD) in combination with [compression dictionaries](/doc/ZszdCompression.md). Without these, the file cannot be uncompressed and therefore not be read/edited. Custom compression dictionaries can be loaded when specified as part of a [game config](/doc/GameConfigs.md#dict-string-default-null). When changing a game config, any already opened ZSTD file that has failed to load must be reloaded.

When saving changes made to MSBT/BMG files, make sure to also save the parent container. If the file is part of an archive, changes to the archive are only applied when the archive is saved as well.

## Building from Source
Building this project requires `.NET 8.0 SDK` or newer.  
Some packages are loaded from external sources (nuget.org and [NintendoTools](https://gitlab.com/AeonSake/nintendo-tools)) and are required for compilation. Make sure to run `nuget restore` (or `Restore NuGet packages` from inside Visual Studio) before building.

## Credits and Licenses
The following list contains all external tools and libraries the MSBT Editor uses to make editing MSBT/BMG files possible. The project itself is licensed under [GPL-3.0](LICENSE).
- [NintendoTools](https://gitlab.com/AeonSake/nintendo-tools) ([GPL-3.0](https://gitlab.com/AeonSake/nintendo-tools/-/blob/master/LICENSE))
  - [AuroraLib.Compression](https://github.com/Venomalia/AuroraLib.Compression) ([MIT](https://github.com/Venomalia/AuroraLib.Compression/blob/main/LICENSE))
  - [Crc32.NET](https://github.com/force-net/Crc32.NET) ([MIT](https://github.com/force-net/Crc32.NET/blob/develop/LICENSE))
  - [ZstdSharp](https://github.com/oleg-st/ZstdSharp) ([MIT](https://github.com/oleg-st/ZstdSharp/blob/master/LICENSE))
- [BlazorMonaco](https://github.com/serdarciplak/BlazorMonaco) ([MIT](https://github.com/serdarciplak/BlazorMonaco/blob/master/LICENSE))
  - [Monaco Editor](https://github.com/microsoft/monaco-editor) ([MIT](https://github.com/microsoft/monaco-editor/blob/main/LICENSE.txt))
- A modified version of [Split.js](https://github.com/nathancahill/split) ([MIT](https://github.com/nathancahill/split/blob/master/LICENSE))
- [Popper](https://github.com/floating-ui/floating-ui) ([MIT](https://github.com/floating-ui/floating-ui/blob/master/LICENSE))
- [Lucide Icons](https://github.com/lucide-icons/lucide) ([ISC](https://github.com/lucide-icons/lucide/blob/main/LICENSE))
- [Json.NET](https://github.com/JamesNK/Newtonsoft.Json) ([MIT](https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md))
- [YamlConvert](https://github.com/tomlm/yamlconvert) ([MIT](https://github.com/tomlm/YamlConvert/blob/main/LICENSE))
- [Serilog](https://github.com/serilog/serilog) ([Apache-2.0](https://github.com/serilog/serilog/blob/dev/LICENSE))
- [ASP.NET Core / Blazor](https://github.com/dotnet/aspnetcore) ([MIT](https://github.com/dotnet/aspnetcore/blob/main/LICENSE.txt))